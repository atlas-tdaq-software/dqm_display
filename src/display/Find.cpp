/*! \file Find.cpp Implements the Find class.
 * \author Anyes Taffard, Kevin Slagle, Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/Find.h>
#include <dqm_display/gui/Status.h>

#include <dqm_display/core/TreeDataModel.h>
#include <dqm_display/core/Parameter.h>

#include <QCoreApplication>
#include <QAbstractItemModel>
#include <QtConcurrent/QtConcurrentRun>

namespace {
    uint fromType(bool is_leaf) {
        return (1 << is_leaf);
    }

    uint fromStatus(dqm_display::core::Result::Status status) {
        return 1 << uint(int(status) + 1);
    }
}

using namespace dqm_display::gui;

FindResultsModel::FindResultsModel(const DQDataModel & model, QObject *parent) :
    QAbstractItemModel(parent),
    m_data_model(model),
    m_region_icon(":/images/region.png"),
    m_parameter_icon(":/images/parameter.png"),
    m_percent(m_data_model.model()->getRootRegion()->totalChildCount() / 100.),
    m_type(-1),
    m_status(-1),
    m_counter(0)
{
    connect(this, SIGNAL(finished()), this, SLOT(when_findFinished()), Qt::QueuedConnection);
    connect(&model, SIGNAL(modelReset()), this, SLOT(when_mainModelUpdated()));
}

void FindResultsModel::when_mainModelUpdated() {
    beginResetModel();
    m_result.clear();
    m_percent = m_data_model.model() ? m_data_model.model()->getRootRegion()->totalChildCount() / 100. : 0.;
    endResetModel();
}

int FindResultsModel::columnCount(const QModelIndex&) const {
    return 4;
}

QVariant FindResultsModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    const core::TreeNode *node = m_result[index.row()];

    if (role == Qt::DecorationRole) {
        if (index.column() == 0)
            return node->isLeaf() ? m_parameter_icon : m_region_icon;
        if (index.column() == 2)
            return Status::icon(node->getResult()->status());
    } else if (role == Qt::DisplayRole) {
        if (index.column() == 1)
            return node->getName();
        if (index.column() == 3)
            return node->getPath();
    } else if (role == FindResultsModel::SortRole) {
        if (index.column() == 0)
            return node->isLeaf();
        if (index.column() == 1)
            return node->getName();
        if (index.column() == 2)
            return QVariant::fromValue(node->getResult()->status());
        if (index.column() == 3)
            return node->getPath();
    }

    return QVariant();
}

Qt::ItemFlags FindResultsModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemFlags();

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant FindResultsModel::headerData(int section, Qt::Orientation orientation, int role) const {
    static const char *const headers[] = { "Type", "Name", "Status", "Location" };
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < 4)
        return headers[section];

    return QVariant();
}

QModelIndex FindResultsModel::index(int row, int column, const QModelIndex &parent) const {
    return !parent.isValid() ? createIndex(row, column, (void*) m_result[row]) : QModelIndex();
}

bool FindResultsModel::hasChildren(const QModelIndex &parent) const {
    return !parent.isValid();
}

QModelIndex FindResultsModel::parent(const QModelIndex&) const {
    return QModelIndex();
}

int FindResultsModel::rowCount(const QModelIndex &parent) const {
    return parent.isValid() ? 0 : m_result.size();
}

void FindResultsModel::reset(const QRegExp &name, const QRegExp &parent_name, uint type,
        uint status) {
    m_name = name;
    m_parent_name = parent_name;
    m_type = type;
    m_status = status;
    m_counter = 0;

    beginResetModel();
    m_result.clear();

    auto r = QtConcurrent::run(this, &FindResultsModel::run);
}

void FindResultsModel::when_findFinished() {
    endResetModel();
}

void FindResultsModel::run() {
    if (m_data_model.model()) {
        find(*m_data_model.model()->getRootRegion());
    }
    emit finished();
}

void FindResultsModel::watchProgress() {
    unsigned int old = (double) m_counter++ / m_percent;
    unsigned int cur = (double) m_counter / m_percent;

    if (cur != old) {
        emit advanced();
    }
}

void FindResultsModel::find(const core::TreeNode &node) {
    int i = 0;
    while (const core::TreeNode *child = node.getChild(i++)) {
        watchProgress();
        if (m_type & fromType(child->isLeaf())) {
            if (m_status & fromStatus(child->getResult()->status())) {
                if (m_name.exactMatch(child->getName())
                        && m_parent_name.exactMatch(node.getName())) {
                    m_result.append(child);
                }
            }
        }

        find(*child);
    }
}

using namespace dqm_display::gui;

Find::Find(const DQDataModel & model, QWidget *parent) :
        QWidget(parent), m_find_result_model(model, this) {
    setupUi(this);

    m_sorting_model.setSourceModel(&m_find_result_model);
    m_sorting_model.setSortRole(FindResultsModel::SortRole);
    treeView_find->setModel(&m_sorting_model);

    treeView_find->header()->resizeSection(0, 50);
    treeView_find->header()->resizeSection(2, 50);

    connect(&m_find_result_model, SIGNAL(finished()), this, SLOT(findFinished()),
            Qt::QueuedConnection);
    connect(&m_find_result_model, SIGNAL(advanced()), this, SLOT(findAdvanced()),
            Qt::QueuedConnection);
}

void Find::on_pushButton_find_clicked() {
    progressBar_find->setFormat("%p%");
    progressBar_find->setMaximum(100);
    progressBar_find->reset();

    QString name = lineEdit_name->text();
    QString parentName = lineEdit_parentName->text();

    using S = core::Result::Status;
    m_find_result_model.reset(
            QRegExp(name,
                    checkBox_caseName->checkState() != Qt::Unchecked ?
                            Qt::CaseSensitive : Qt::CaseInsensitive),
            QRegExp(parentName,
                    checkBox_caseParentName->checkState() != Qt::Unchecked ?
                            Qt::CaseSensitive : Qt::CaseInsensitive),

            (fromType(false) * pushButton_findRegions->isChecked())
                    | (fromType(true) * pushButton_findParameters->isChecked()),

            (fromStatus(S::DISABLED) * pushButton_black->isChecked())
                    | (fromStatus(S::UNDEFINED) * pushButton_grey->isChecked())
                    | (fromStatus(S::ERROR) * pushButton_red->isChecked())
                    | (fromStatus(S::WARNING) * pushButton_orange->isChecked())
                    | (fromStatus(S::GOOD) * pushButton_green->isChecked())
                    | (fromStatus(S::LOW_STAT) * pushButton_lowStats->isChecked()));
}

void Find::on_treeView_find_doubleClicked(const QModelIndex &index) {
    const core::TreeNode *node = static_cast<const core::TreeNode*>(
            m_sorting_model.mapToSource(index).internalPointer());
    if (node)
        emit nodeActivated(node);
}

void Find::findAdvanced() {
    progressBar_find->setValue(progressBar_find->value() + 1);
}

void Find::findFinished() {
    uint items = m_find_result_model.rowCount();

    progressBar_find->setFormat("%v items found");
    progressBar_find->setMaximum(items ? items : 100);
    progressBar_find->setValue(items);
}

/*! \file AlarmsMenu.cpp Implements the dqm_display::gui::AlarmsMenu class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/Issue.h>

#include <dqm_display/gui/AlarmsMenu.h>
#include <dqm_display/core/TreeNode.h>

#include <QDebug>
#include <QMenu>
#include <QMessageBox>
#include <QMouseEvent>

using namespace dqm_display::gui;

AlarmsMenu::AlarmsMenu(QWidget * parent)
  : QMenu(parent)
{
    setDefaultAction(addAction("&Show", parent, SLOT(showCurrent())));
    addSeparator();
    addAction("Remove Current", parent, SLOT(removeCurrent()));
    addAction("Remove Acknowledged", parent, SLOT(removeAcknowledged()));
    addAction("Remove All", parent, SLOT(removeAll()));
}


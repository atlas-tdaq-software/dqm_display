/*! \file LogWidget.cpp Implements the dqm_display::gui::LogWidget class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/LogWidget.h>
#include <dqm_display/gui/Status.h>

#include <QBrush>
#include <QDebug>
#include <QTableWidget>

using namespace dqm_display::gui;
using namespace dqm_display::core;

const int LogWidget::s_maxEntries = 100;

class TableWidgetStateItem : public QTableWidgetItem {
  public:
    TableWidgetStateItem()
    {
	setCheckState(Qt::Unchecked);
    }

    bool operator< (const QTableWidgetItem & other) const
    {
	return checkState() < other.checkState();
    }
};

class TableWidgetStatusItem : public QTableWidgetItem {
  public:
    TableWidgetStatusItem(Result::Status status)
      : m_status(status)
    {
	setIcon(Status::icon(m_status));
    }

    bool operator< (const QTableWidgetItem & other) const
    {
	return m_status < static_cast<const TableWidgetStatusItem&>(other).m_status;
    }

  private:
    const Result::Status m_status;
};

class TableWidgetSeverityItem : public QTableWidgetItem {
  public:
    TableWidgetSeverityItem(int s)
      : m_severity(s)
    {
	if (m_severity == ers::Warning) {
	    setIcon(QIcon(":/images/warning.png"));
	}
	else if (m_severity == ers::Error) {
            setIcon(QIcon(":/images/error.png"));
        }
	else if (m_severity == ers::Fatal) {
	    setIcon(QIcon(":/images/disabled.png"));
	}
	else {
	    setIcon(QIcon(":/images/undefined.png"));
	}
    }

    bool operator< (const QTableWidgetItem & other) const
    {
	return m_severity < static_cast<const TableWidgetSeverityItem&>(other).m_severity;
    }

  private:
    const int m_severity;
};


LogWidget::LogWidget(QWidget * parent)
  : QWidget(parent),
    m_alarms_menu(this),
    m_logs_menu(this)
{
    setupUi(this);
    
    tableWidget_Alarms->setStyleSheet(
	    "QTableView::indicator:unchecked {image:url(:/images/alarm_bell.png);}"
	    "QTableView::indicator:checked {image:url(:/images/tick.png);}");

    tableWidget_Alarms->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(tableWidget_Alarms, SIGNAL(customContextMenuRequested(const QPoint &)),
	    this, SLOT(showAlarmsMenu(const QPoint &)));

    tableWidget_Logs->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(tableWidget_Logs, SIGNAL(customContextMenuRequested(const QPoint &)),
	    this, SLOT(showLogsMenu(const QPoint &)));

    connect( tableWidget_Alarms, SIGNAL(itemDoubleClicked(QTableWidgetItem*)),
			   this, SLOT(showCurrent()) );

    connect( this, SIGNAL(messageReceived(int , const QString & , const QString & , const QString & )),
	     this, SLOT(when_messageReceived(int , const QString & , const QString & , const QString & )),
	     Qt::QueuedConnection);
}

LogWidget::~LogWidget()
{
    try {
	ers::StreamManager::instance().remove_receiver( this );
    }
    catch( ers::Issue & ex ) {
	ers::error(ex);
    }
}

void
LogWidget::subscribe(const std::string & partition_name)
{
    try {
        m_handler.reset(ers::set_issue_catcher(
                std::bind(&LogWidget::receive, this, std::placeholders::_1)));
    }
    catch(ers::IssueCatcherAlreadySet & ex) {
        ers::error(ex);
    }

    try {
	ers::StreamManager::instance().add_receiver(
		"mts", {partition_name, "qual=dqm_core or qual=dqmf_config or qual=dqmf"}, this);
    }
    catch(ers::Issue & ex) {
	ers::error(ex);
    }
}

void
LogWidget::showAlarmsMenu(const QPoint & pos)
{
    if (tableWidget_Alarms->currentRow() < 0)
	return;

    m_alarms_menu.move(tableWidget_Alarms->mapToGlobal(pos));
    m_alarms_menu.show();
}

void
LogWidget::showLogsMenu(const QPoint & pos)
{
    if (tableWidget_Logs->currentRow() < 0)
	return;

    m_logs_menu.move(tableWidget_Logs->mapToGlobal(pos));
    m_logs_menu.show();
}

void 
LogWidget::when_alarmProduced(const TreeNode * node)
{
    tableWidget_Alarms->insertRow(0);

    QTableWidgetItem * item = new TableWidgetStateItem;
    item->setData( Qt::UserRole, QVariant::fromValue( (void*)node ) );
    tableWidget_Alarms->setItem( 0, 0, item );

    Result::Status status = node->getStatus();
    item = new TableWidgetStatusItem(status);
    tableWidget_Alarms->setItem( 0, 1, item );

    item = new QTableWidgetItem(node->getResult()->timeString());
    tableWidget_Alarms->setItem( 0, 2, item );
        
    item = new QTableWidgetItem(node->getFullName());
    tableWidget_Alarms->setItem( 0, 3, item );
    
    while ( s_maxEntries < tableWidget_Alarms->rowCount() )
    	tableWidget_Alarms->removeRow(tableWidget_Alarms->rowCount()-1);

    tabWidget->setTabIcon(0, QIcon(":/images/alarm_bell.png"));
}

void 
LogWidget::showCurrent()
{
    int row = tableWidget_Alarms->currentRow();

    QTableWidgetItem * head = tableWidget_Alarms->item(row, 0);
    
    const core::TreeNode * node = 
    	static_cast<const TreeNode*>(head->data(Qt::UserRole).value<void*>());
    if ( node ) {
	head->setCheckState(Qt::Checked);
	for (int i = 0; i < 4; ++i)
	    tableWidget_Alarms->item(row, i)->setBackground(QBrush(Qt::gray));
	emit alarmAcknowledged(node);
    }
}

void
LogWidget::removeCurrent()
{
    int row = tableWidget_Alarms->currentRow();
    tableWidget_Alarms->removeRow(row);

    if (!tableWidget_Alarms->rowCount())
	tabWidget->setTabIcon(0, QIcon());
}

void
LogWidget::removeAcknowledged()
{
    int rows = tableWidget_Alarms->rowCount();
    for (int i = rows - 1; i >= 0; --i)
    {
	if ( tableWidget_Alarms->item(i, 0)->checkState() == Qt::Checked )
	    tableWidget_Alarms->removeRow(i);
    }

    if (!tableWidget_Alarms->rowCount())
	tabWidget->setTabIcon(0, QIcon());
}

void
LogWidget::removeAll()
{
    tableWidget_Alarms->setRowCount(0);
    tabWidget->setTabIcon(0, QIcon());
}

void
LogWidget::receive(const ers::Issue & issue)
{
    emit messageReceived(
	    issue.severity(), QString::fromStdString(issue.time()),
	    QString(issue.context().application_name()), QString::fromStdString(issue.message()));
}

void
LogWidget::when_messageReceived(
	int severity, const QString & time, const QString & application, const QString & message)
{
    tableWidget_Logs->insertRow(0);

    QTableWidgetItem * item = new TableWidgetSeverityItem(severity);
    tableWidget_Logs->setItem( 0, 0, item );

    item = new QTableWidgetItem(time);
    tableWidget_Logs->setItem( 0, 1, item );

    item = new QTableWidgetItem(application);
    tableWidget_Logs->setItem( 0, 2, item );

    item = new QTableWidgetItem(message);
    tableWidget_Logs->setItem( 0, 3, item );

    tabWidget->setTabIcon(1, QIcon(":/images/alarm_bell.png"));
}

void
LogWidget::removeCurrentMessage()
{
    int row = tableWidget_Logs->currentRow();
    tableWidget_Logs->removeRow(row);

    if (!tableWidget_Logs->rowCount())
	tabWidget->setTabIcon(1, QIcon());

}

void
LogWidget::removeAllMessages()
{
    tableWidget_Logs->setRowCount(0);
    tabWidget->setTabIcon(1, QIcon());
}

/*! \file TreeNodeMenu.cpp Implements the dqm_display::gui::TreeNodeMenu class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/Issue.h>

#include <dqm_display/gui/TreeNodeMenu.h>
#include <dqm_display/gui/Elog.h>

#include <QDebug>
#include <QMenu>
#include <QMessageBox>
#include <QMouseEvent>

using namespace dqm_display::gui;

TreeNodeMenu::TreeNodeMenu(QWidget * parent)
  : QMenu(parent),
    m_node(0)
{
    setDefaultAction(addAction("&Explore", this, SLOT(exploreNode())));
    addSeparator();
    addAction("Enable", this, SLOT(enableNode()));
    addAction("Disable", this, SLOT(disableNode()));
    addSeparator();
    addAction("Modify Algorithm Parameters", this, SLOT(updateParameters()));
    addSeparator();
    addAction("Attach to Elog Message", this, SLOT(addImageToElog()));

    setNode(m_node);
}

void
TreeNodeMenu::exploreNode()
{
    if (m_node)
	emit nodeActivated(m_node);
}

void
TreeNodeMenu::updateParameters()
{
    m_parameters_dialog.setParameter(static_cast<const core::Parameter*>(m_node));
    m_parameters_dialog.show();
}

void
TreeNodeMenu::addImageToElog()
{
    Elog::instance().addAttachment(m_node);
}

void
TreeNodeMenu::setNode(const core::TreeNode * node)
{
    m_node = node;
    QList<QAction *> a(actions());
    a[0]->setEnabled(m_node);

//      TODO: Must be in the running state
    a[2]->setEnabled(m_node && m_node->getResult()->status() == core::Result::Status::DISABLED);
    a[3]->setEnabled(m_node && m_node->getResult()->status() != core::Result::Status::DISABLED);

    a[5]->setEnabled(m_node && m_node->isLeaf());
    a[7]->setEnabled(m_node && m_node->isLeaf() && static_cast<const core::Parameter*>(m_node)->histogramExists());
}

void
TreeNodeMenu::sendCommand(bool enable)
{
    if ( !enable && QMessageBox::warning(this, "Warning!",
	"Disabling this node will permanently take it out of DQ assessment.\n"
        "All DQ results above this one will be affected.\n"
        "All results below this one will be ignored. Do you want to proceed?",
	QMessageBox::Yes|QMessageBox::No) != QMessageBox::Yes)
    {
	return ;
    }

    try {
	m_node->changeState(enable);
    }
    catch (ers::Issue &e) {
        QMessageBox::warning(this, 
        	QString("%1 failed").arg(enable ? "Enabling" : "Disabling" ), 
                e.what(), QMessageBox::Ok);
    }
}


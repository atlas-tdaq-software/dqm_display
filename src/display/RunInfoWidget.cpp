/*! \file RunInfoWidget.cpp Implements the dqm_display::gui::RunInfoWidget class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/Result.h>
#include <dqm_display/gui/RunInfoWidget.h>

#include <QDebug>

using namespace dqm_display::gui;

RunInfoWidget::RunInfoWidget(const QString & partition_name)
  : m_partition_name(partition_name),
    m_run_number(0),
    m_stable_beam(false),
    m_ready_4_physics(false),
    m_beam_energy(0)
{
    m_receivers
	<< QSharedPointer<RunInfoReceiver>(
		new RunInfoReceiver(partition_name, "RunParams.RunParams",
			std::bind(&RunInfoWidget::watchRunParams, this, std::placeholders::_1)))
	<< QSharedPointer<RunInfoReceiver>(
		new RunInfoReceiver(partition_name, "RunCtrl.LastStartTime",
			std::bind(&RunInfoWidget::watchRunStart, this, std::placeholders::_1)))
	<< QSharedPointer<RunInfoReceiver>(
		new RunInfoReceiver(partition_name, "RunParams.RunInfo",
			std::bind(&RunInfoWidget::watchRunInfo, this, std::placeholders::_1)))
	<< QSharedPointer<RunInfoReceiver>(
		new RunInfoReceiver(partition_name, "RunParams.Ready4Physics",
			std::bind(&RunInfoWidget::watchReady4Physics, this, std::placeholders::_1)))
	<< QSharedPointer<RunInfoReceiver>(
		new RunInfoReceiver(partition_name, "RunCtrl.RootController",
			std::bind(&RunInfoWidget::watchRootController, this, std::placeholders::_1)));
}

void
RunInfoWidget::watchRunParams(const ISInfoDynAny & info)
{
    try {
	m_run_number = info.getAttributeValue<uint>("run_number");
	m_run_type = QString::fromStdString(info.getAttributeValue<std::string>("run_type"));
	m_stable_beam = info.getAttributeValue<uint>("beam_type");
	m_beam_energy = info.getAttributeValue<int>("beam_energy");
	update();
    }
    catch(daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void
RunInfoWidget::watchReady4Physics(const ISInfoDynAny & info)
{
    try {
	m_ready_4_physics = info.getAttributeValue<bool>("ready4physics");
	update();
    }
    catch(daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void
RunInfoWidget::watchRunStart(const ISInfoDynAny & info)
{
    try {
	uint64_t msecs = info.getAttributeValue<uint64_t>("value");
	m_start_time = QDateTime::fromMSecsSinceEpoch(msecs);
	update();
    }
    catch(daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void
RunInfoWidget::watchRunInfo(const ISInfoDynAny & info)
{
    try {
	uint tt = info.getAttributeValue<uint>("activeTime");
	m_total_time = QTime(tt/3600, tt%3600/60, tt%3600%60);
	update();
    }
    catch(daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void
RunInfoWidget::watchRootController(const ISInfoDynAny & info)
{
    try {
	QString new_state  = QString::fromStdString(info.getAttributeValue<std::string>("state"));
	QString transition = QString::fromStdString(info.getAttributeValue<std::string>("lastTransitionName"));

	if (m_run_state == new_state)
	    return;

	if (!m_run_state.isEmpty() && (transition == "INIT_FSM" || transition == "CONNECT")) {
	    emit databaseRestarted();
	}

	m_run_state = new_state;
	update();
    }
    catch(daq::is::Exception & ex) {
	;
    }
}

void
RunInfoWidget::update()
{
    if (m_run_state == "RUNNING") {
	core::Result::runStartedAt(m_start_time.toTime_t());
        emit runStarted();
    }

    QString s;

    if (m_run_state != "RUNNING")
	s = QString("<font color=darkblue>Partition '%1' is not running (RC state = %2)</font>")
	    .arg(m_partition_name)
	    .arg(m_run_state);
    else
	s = QString("<font color=darkgreen>Partition '%1' is running for %3 in %4 mode. Run Number is %5</font>")
	    .arg(m_partition_name)
	    .arg(m_total_time.toString("hh'h' mm'm' ss's'"))
	    .arg(m_ready_4_physics ? "Physics" : "StandBy")
	    .arg(m_run_number);
    setText(s);
}


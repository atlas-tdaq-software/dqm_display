/*! \file LogsMenu.cpp Implements the dqm_display::gui::LogsMenu class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/Issue.h>

#include <dqm_display/gui/LogsMenu.h>
#include <dqm_display/core/TreeNode.h>

#include <QDebug>
#include <QMenu>
#include <QMessageBox>
#include <QMouseEvent>

using namespace dqm_display::gui;

LogsMenu::LogsMenu(QWidget * parent)
  : QMenu(parent)
{
    addAction("Remove Current", parent, SLOT(removeCurrentMessage()));
    addAction("Remove All", parent, SLOT(removeAllMessages()));
}


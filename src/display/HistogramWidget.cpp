#include <dqm_display/gui/Display.h>
#include <dqm_display/gui/HistogramWidget.h>
#include <dqm_display/gui/Status.h>
#include <dqm_display/core/Parameter.h>

#include <HistogramStyles/HistogramStyles.h>

#include <TAttLine.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH1.h>
#include <TH2.h>
#include <TImage.h>
#include <TObject.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TColor.h>
#include <TPaveStats.h>
#include <TROOT.h>
#include <TText.h>
#include <TPaveText.h>

#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QPainter>
#include <QResizeEvent>

using namespace dqm_display::gui;

HistogramWidget::HistogramWidget(QWidget* parent)
: CanvasWidget(parent),
  m_histogram(m_canvas)
{ }

void HistogramWidget::clear() {
    m_histogram.clear();
    refresh();
}

void
HistogramWidget::setNode(const core::TreeNode * node)
{
    if (not node || not node->isLeaf()) {
        return;
    }

    m_histogram.setNode(static_cast<const core::Parameter*>(node));

    refresh();
}


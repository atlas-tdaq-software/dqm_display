/*! \file GuiSettings.cpp Implements the dqm_display::gui GUI settings.
 * \author Anyes Taffard, Kevin Slagle, Hideki Okawa
 * \version 1.0
 */

#include <dqm_display/gui/Status.h>

#include <map>

using namespace dqm_display::gui;

using dqm_display::core::Result;

Color_t
Status::color_t(core::Result::Status s)
{
    static std::map<core::Result::Status, Color_t> colors = {
	    {Result::Status::DISABLED, kBlack},
	    {Result::Status::UNDEFINED, kGray},
	    {Result::Status::ERROR, kPink},
	    {Result::Status::WARNING, kOrange},
	    {Result::Status::GOOD, kSpring},
	    {Result::Status::LOW_STAT, kCyan+2}
    };

    return colors[s];
}

const QColor &
Status::color(core::Result::Status s)
{
    static std::map<core::Result::Status, QColor> colors = {
	    {Result::Status::DISABLED, QColor(32, 32, 32)},
	    {Result::Status::UNDEFINED, Qt::gray},
	    {Result::Status::ERROR, QColor(232, 0, 0)},
	    {Result::Status::WARNING, QColor(255, 224, 0)},
	    {Result::Status::GOOD, QColor(0, 192, 0)},
	    {Result::Status::LOW_STAT, QColor(40, 155, 155)}
    };

    return colors[s];
}

const QString &
Status::htmlColor(core::Result::Status s)
{
    static std::map<core::Result::Status, QString> colors = {
	    {Result::Status::DISABLED, "black"},
	    {Result::Status::UNDEFINED, "gray"},
	    {Result::Status::ERROR, "red"},
	    {Result::Status::WARNING, "yellow"},
	    {Result::Status::GOOD, "green"},
	    {Result::Status::LOW_STAT, "darkcyan"}
    };

    return colors[s];
}

const char *
Status::iconName(core::Result::Status s)
{
    static std::map<core::Result::Status, const char * const> icons = {
	    {Result::Status::DISABLED, ":/images/disabled.png"},
	    {Result::Status::UNDEFINED, ":/images/undefined.png"},
	    {Result::Status::ERROR, ":/images/error.png"},
	    {Result::Status::WARNING, ":/images/warning.png"},
	    {Result::Status::GOOD, ":/images/ok.png"},
	    {Result::Status::LOW_STAT, ":/images/low_stat.png"}
    };

    return icons[s];
}

QIcon
Status::icon(core::Result::Status s)
{
    static std::map<core::Result::Status, QIcon> icons = {
	    {Result::Status::DISABLED, QIcon(iconName(Result::Status::DISABLED))},
	    {Result::Status::UNDEFINED, QIcon(iconName(Result::Status::UNDEFINED))},
	    {Result::Status::ERROR, QIcon(iconName(Result::Status::ERROR))},
	    {Result::Status::WARNING, QIcon(iconName(Result::Status::WARNING))},
	    {Result::Status::GOOD, QIcon(iconName(Result::Status::GOOD))},
	    {Result::Status::LOW_STAT, QIcon(iconName(Result::Status::LOW_STAT))}
    };

    return icons[s];
}

QImage
Status::regionImage(core::Result::Status s)
{
    static QImage qp(":/images/region.png");

    return qp;
}


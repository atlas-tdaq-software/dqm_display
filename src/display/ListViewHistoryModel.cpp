/*! \file ListViewHistoryModel.cpp Implements the ListViewHistoryModel class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <is/infodictionary.h>

#include <dqm_display/gui/ListViewHistoryModel.h>

#include <dqm_display/core/TreeNode.h>

#include <QBitmap>
#include <QDebug>
#include <QMainWindow>
#include <QPainter>

#include <TVirtualX.h>

using namespace dqm_display::gui;

ListViewHistoryModel::ListViewHistoryModel(QWidget * parent, const QString & partition_name)
  : QAbstractListModel(parent),
    m_partition(partition_name.toStdString()),
    m_node(0)
{
    m_first_font.setBold(true);
    m_others_font.setItalic(true);
}

void
ListViewHistoryModel::clear()
{
    beginResetModel();
    m_graphs.clear();
    m_node = 0;
    endResetModel();
}

void
ListViewHistoryModel::setNode(const core::TreeNode* node)
{
    m_node = node;
    update();
}

void
ListViewHistoryModel::update()
{
    beginResetModel();
    m_graphs.clear();
    
    std::shared_ptr<core::Result> result = m_node->getResult();

    if (result) {
        ISInfoDictionary dictionary(m_partition);
        std::string nodeName(m_node->getName().toStdString());
        std::string resultName = "DQM." + nodeName;
        std::vector<dqmf::is::Result> results;
        try {
            dictionary.getValues(resultName, results, -1);
            m_graphs.push_back(
                QSharedPointer<ResultHistoryGraph>(
                        new ResultHistoryGraph(nodeName, results)));

            typedef std::vector<dqmf::is::Tag>::const_iterator Iter;
            const Iter end = result->tags().end();
            for (Iter it = result->tags().begin(); it != end; it++) {
                m_graphs.push_back(
                        QSharedPointer<ResultHistoryGraph>(
                            new ResultHistoryGraph(nodeName, results, it->name)));
            }
        } catch (daq::is::Exception& ex) {
            ers::warning(ex);
        }
    }
    
    endResetModel();
}

QModelIndex 
ListViewHistoryModel::index(int row, int column, const QModelIndex & parent) const
{ 
    if (parent.isValid())
	return QModelIndex();
    
    return createIndex(row, column, (void*)m_node);
}

QVariant
ListViewHistoryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
	return QVariant();
    
    if (role == Qt::FontRole)
    	return index.row() ? m_others_font : m_first_font;
    
    if (role == Qt::DecorationRole) 
        return QVariant::fromValue(m_graphs[index.row()].data());
    
    if (role == Qt::DisplayRole)
	return QString(m_graphs[index.row()]->getTitle());
    
    if (role == Qt::UserRole)
	return QVariant::fromValue((void*)m_node);

    return QVariant();
}

int
ListViewHistoryModel::rowCount(const QModelIndex & ) const
{
    return m_graphs.size();
}


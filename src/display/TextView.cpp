/*! \file TextView.cpp Implements the dqm_display::gui::TextView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/TextView.h>

#include <QDebug>

using namespace dqm_display::gui;

TextView::TextView(QWidget *parent)
  : QTextEdit(parent),
    m_document(this),
    m_font_size(18)
{
    setDocument(&m_document);
}

void TextView::when_fontSizeChanged(int size) {
    m_font_size = size;
    m_document.setDefaultStyleSheet(QString("table {font-size: %1px;}").arg(m_font_size));
    m_document.setHtml(m_html);
}

void TextView::clear() {
    m_document.setHtml(m_html = "");
}

void TextView::update(const core::TreeNode & node)
{
    m_document.setDefaultStyleSheet(QString("table {font-size: %1px;}").arg(m_font_size));

    m_html = QString("<table><tr><td colspan=\"2\"><b>%1</b></td></tr>").arg(node.getName());
    QVector<QPair<QString, QString>> c = node.getAllAttributes();
    for (auto it = c.begin(); it != c.end(); ++it) {
        m_html += QString("<tr><td><font color=darkslategray>%1:</font></td><td>%2</td></tr>")
                .arg(it->first, it->second);
    }
    m_html += "</table>";

    m_document.setHtml(m_html);
}


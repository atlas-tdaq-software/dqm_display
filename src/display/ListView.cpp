/*! \file ListView.cpp Implements the dqm_display::gui::ListView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/ListView.h>
#include <QDebug>

using namespace dqm_display::gui;

ListView::ListView(QWidget *parent)
  : ViewBase<QListView>(parent)
{
    setUniformItemSizes(true);
}

void ListView::setInteractive()
{
    m_interactive = true;

    connect(this, SIGNAL(clicked(const QModelIndex &)),
	this, SLOT(when_clicked(const QModelIndex &)));

    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
	this, SLOT(when_menu_requested(const QPoint &)));

    connect(&m_menu, SIGNAL(nodeActivated(const core::TreeNode *)),
	this, SIGNAL(nodeActivated(const core::TreeNode *)));
}

void ListView::resizeEvent(QResizeEvent * event)
{
    QListView::resizeEvent(event);

    if (m_interactive) {
        return;
    }

    if (event->oldSize().height() != event->size().height()) {
        emit viewHeightChanged(event->size().height(), model()->index(0,0));
    }
}

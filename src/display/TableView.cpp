/*! \file TableView.cpp Implements the dqm_display::gui::TableView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/TableView.h>

#include <QDebug>

using namespace dqm_display::gui;

TableView::TableView(QWidget *parent)
  : ViewBase<QTableView>(parent)
{
}

void
TableView::setProxyModel(QAbstractProxyModel * model)
{
    delete m_proxy_model;
    m_proxy_model = model;
    QTableView::setModel(m_proxy_model);
}

void
TableView::updateNode(const QModelIndex & index)
{
    if (isVisible()) {
	QModelIndex i = m_proxy_model->mapFromSource(index);
	update(i);
	update(i.sibling(i.row(), 1));
    }
}


/*! \file TreeView.cpp Implements the dqm_display::gui::TreeView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/TreeView.h>

#include <QDebug>

using namespace dqm_display::gui;

TreeView::TreeView(QWidget *parent)
  : ViewBase<QTreeView>(parent)
{
    connect(this, SIGNAL(clicked(const QModelIndex &)),
	this, SLOT(when_clicked(const QModelIndex &)));

    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)),
	this, SLOT(when_menu_requested(const QPoint &)));

    connect(&m_menu, SIGNAL(nodeActivated(const core::TreeNode *)),
	this, SIGNAL(nodeActivated(const core::TreeNode *)));
}

void
TreeView::setProxyModel(QAbstractProxyModel * model)
{
    delete m_proxy_model;
    m_proxy_model = model;
    QTreeView::setModel(m_proxy_model);
}

void
TreeView::resizeEvent(QResizeEvent * event)
{
    QTreeView::resizeEvent(event);

    if (model()->rowCount() != 1)
	return;

    if (event->oldSize().height() != event->size().height())
    {
	emit viewHeightChanged(event->size().height(), model()->index(0,0));
    }
}

void
TreeView::updateNode(const QModelIndex & index)
{
    if (isVisible()) {
	QModelIndex i = m_proxy_model->mapFromSource(index);
	update(i);
	update(i.sibling(i.row(), 1));
    }
}


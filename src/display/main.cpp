#include <unistd.h>
#include <sys/types.h>
#include <sys/file.h>
#include <pwd.h>

#include <QApplication>
#include <QSplashScreen>
#include <QMessageBox>

#include <cmdl/cmdargs.h>

#include <TApplication.h>
#include <TROOT.h>

#include <HistogramStyles/HistogramStyles.h>

#include <dqm_display/gui/About.h>
#include <dqm_display/gui/Display.h>
#include <dqm_display/gui/DQDataModel.h>

namespace daq {
    ERS_DECLARE_ISSUE(dqm_display, UnhandledException,
            "Unhandled '" << name << "' exception was thrown",
            ((const char *)name))
}

struct SafeApplication: public TApplication, public QApplication {
    SafeApplication(int &ac, char **av) :
            TApplication("DQM Display", &ac, av), QApplication(ac, av) {
        ;
    }

    bool notify(QObject *receiver, QEvent *event) {
        try {
            return QApplication::notify(receiver, event);
        } catch (ers::Issue &ex) {
            ers::error(ex);
        } catch (std::exception &ex) {
            ers::error(daq::dqm_display::UnhandledException(ERS_HERE, "std::exception", ex));
        }
        return false;
    }
};

struct SingleInstance {
    static std::string alreadyRunning(const std::string &partition_name) {
        struct passwd *pw = ::getpwuid(getuid());

        if (!pw || !pw->pw_dir) {
            ERS_DEBUG(1, "Users home directory can not be retrieved: " << strerror(errno));
            return "";
        }

        std::string lock_file_name(pw->pw_dir);
        lock_file_name += "/.dqm_display." + partition_name + ".lock";

        int fd = ::open(lock_file_name.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

        if (fd == -1) {
            ERS_DEBUG(1,
                    "Lock file '" << lock_file_name << "' can not be created: " << strerror(errno));
            return "";
        }

        int result = ::flock(fd, LOCK_EX | LOCK_NB);

        if (result != 0 && errno == EWOULDBLOCK) {
            char tmp[1024];
            int bytes = read(fd, tmp, 1023);
            if (bytes >= 0) {
                tmp[bytes] = 0;
            }
            ::close(fd);
            ERS_DEBUG(1, "Lock file '" << lock_file_name << "' already exists: " << tmp);
            return tmp;
        }

        char hostname[1024];
        if (!::gethostname(hostname, sizeof(hostname))) {
            ::ftruncate(fd, 0);
            ::write(fd, hostname, ::strlen(hostname));
            ::fsync(fd);
            ERS_DEBUG(1, "Lock file '" << lock_file_name << "' is created");
        }

        return "";
    }
};

int main(int argc, char *argv[]) {
    ::setenv("TDAQ_APPLICATION_NAME", "self", 1);

    IPCCore::init(argc, argv);

    CmdArgStr partition_name('p', "partition", "partition-name", "TDAQ partition", CmdArg::isREQ);
    CmdArgStr database('d', "database", "database-connect-string", "DQM Configuration file to use");
    CmdArgInt palette('c', "palette", "ROOT-palette-number", "ID of the ROOT palette used to draw histograms (default = 1)");
    CmdArgBool use_data_flow('f', "data-flow", "Show only the Data Flow subsystem.");

    // Declare command object and its argument-iterator
    CmdLine cmd(*argv, &database, &partition_name, &use_data_flow, &palette, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    // Parse arguments
    cmd.parse(arg_iter);

    std::string partition((const char*) partition_name);
    std::string db_connect_string =
            (database.flags() && CmdArg::GIVEN) ?
                    std::string((const char*) database) : "rdbconfig:" + partition + "::RDB";

    Q_INIT_RESOURCE(Display);
    qRegisterMetaType<dqm_display::gui::ResultHistoryGraph*>(
            "dqm_display::gui::ResultHistoryGraph*");

    int numArgs = 1;
    SafeApplication app(numArgs, argv); //(argc, argv);
    app.setQuitOnLastWindowClosed(true);

    HistogramStyles::rootUtils = new HistogramStyles();
    gROOT->SetStyle("ATLAS");
    TStyle *s = gROOT->GetStyle("ATLAS");
    s->SetStatStyle(1001);
    s->SetTitleStyle(1001);
    s->SetTitleFillColor(0);
    s->SetLineWidth(1);
    if (palette.flags() && CmdArg::GIVEN) {
        s->SetPalette((int)palette);
    }

    int color = 19;
    s->SetFrameFillColor(color);
    s->SetCanvasColor(color);
    s->SetTitleFillColor(color);
    s->SetStatColor(color - 1);

    std::string host = SingleInstance::alreadyRunning(partition);
    if (!host.empty()) {
        QMessageBox::critical(0, QString("Critical Error"),
                QString(
                        "Another instance of DQM Display application for '"
                                + QString::fromStdString(partition) + "' partition "
                                        "is already running under your account on the '"
                                + QString::fromStdString(host) + "' computer.\n"
                                        "Please stop it before starting another one."),
                QMessageBox::Ok);
        return 1;
    }

    dqm_display::gui::DQDataModel data_model(partition, db_connect_string);

    dqm_display::gui::About about;
    about.show(false);
    if (!data_model.loadData()) {
        return 1;
    }
    about.hide();

    dqm_display::gui::Display mainPanel(&data_model, &about);

    mainPanel.show();

    return app.exec();
}

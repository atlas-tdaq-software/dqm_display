/*! \file UpdateParameters.cpp Implements the dqm_display::gui::UpdateParameters class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <ers/Issue.h>
#include <is/infodictionary.h>

#include <dqm_config/is/DQParameter.h>
#include <dqm_display/core/Parameter.h>
#include <dqm_display/core/Region.h>
#include <dqm_display/core/TreeDataModel.h>
#include <dqm_display/gui/UpdateParameters.h>

#include <QDebug>
#include <QTableWidget>
#include <QDoubleValidator>
#include <QLineEdit>
#include <QStyledItemDelegate>
#include <QSharedPointer>

using namespace dqm_display::gui;

class DoubleValidate : public QStyledItemDelegate
{
public:
    DoubleValidate(QObject * parent) : QStyledItemDelegate(parent) { ; }

    QWidget * createEditor(
	QWidget * parent, const QStyleOptionViewItem & option, const QModelIndex & index) const
    {
         QWidget* editor = QStyledItemDelegate::createEditor(parent, option, index);
         QLineEdit* lineEditEditor = qobject_cast<QLineEdit*>(editor);
         if( lineEditEditor ) {
             lineEditEditor->setValidator(new QDoubleValidator(parent));
         }

         return editor;
    }
};

UpdateParameters::UpdateParameters()
{
    setupUi(this);

    tableWidget_Parameters->setItemDelegateForColumn(0, new DoubleValidate(this));
    tableWidget_Thresholds->setItemDelegateForColumn(0, new DoubleValidate(this));
    tableWidget_Thresholds->setItemDelegateForColumn(1, new DoubleValidate(this));
}

void
UpdateParameters::setParameter(const core::Parameter * param)
{
    m_parameter = param;
    setWindowTitle(QString("Update Parameters for %1").arg(param->getName()));

    std::shared_ptr<const dqm_core::AlgorithmConfig> config = m_parameter->getAlgorithmConfig();

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Display Algorithm Parameters
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    const std::map<std::string, double> & params = config->getParameters();

    tableWidget_Parameters->setRowCount(0);
    tableWidget_Parameters->setRowCount(params.size());
    int row = 0;
    for(std::map<std::string, double>::const_iterator it = params.begin();
	it != params.end(); ++it, ++row)
    {
	QTableWidgetItem * item = new QTableWidgetItem(QString::fromStdString(it->first));
	item->setFlags(Qt::NoItemFlags);
	tableWidget_Parameters->setItem(row, 1, item);

	item = new QTableWidgetItem(QString::number(it->second));
	tableWidget_Parameters->setItem(row, 0, item);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Display Algorithm Thresholds
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::map<std::string, double> greenTh = config->getGreenThresholds();
    std::map<std::string, double> redTh = config->getRedThresholds();

    tableWidget_Thresholds->setRowCount(0);
    tableWidget_Thresholds->setRowCount(greenTh.size() + redTh.size());

    row = 0;
    std::map<std::string, double>::const_iterator green = greenTh.begin();
    for (; green != greenTh.end(); ++green) {
	QTableWidgetItem * item = new QTableWidgetItem(QString::fromStdString(green->first));
	item->setFlags(Qt::NoItemFlags);
	tableWidget_Thresholds->setItem(row, 2, item);

	item = new QTableWidgetItem(QString::number(green->second));
	tableWidget_Thresholds->setItem(row, 1, item);

	std::map<std::string, double>::const_iterator red = redTh.find(green->first);
	if (red != redTh.end()) {
	    item = new QTableWidgetItem(QString::number(red->second));
	    tableWidget_Thresholds->setItem(row, 0, item);
	}
	else {
	    item = new QTableWidgetItem("");
	    item->setFlags(Qt::NoItemFlags);
	    tableWidget_Thresholds->setItem(row, 0, item);
	}
	++row;
    }

    std::map<std::string, double>::const_iterator red = redTh.begin();
    for (; red != redTh.end(); ++red) {
	std::map<std::string, double>::const_iterator green = greenTh.find(red->first);
	if (green == greenTh.end()) {
	    QTableWidgetItem * item = new QTableWidgetItem(QString::fromStdString(red->first));
	    item->setFlags(Qt::NoItemFlags);
	    tableWidget_Thresholds->setItem(row, 2, item);

	    item = new QTableWidgetItem("");
	    item->setFlags(Qt::NoItemFlags);
	    tableWidget_Thresholds->setItem(row, 1, item);

	    item = new QTableWidgetItem(QString::number(red->second));
	    tableWidget_Thresholds->setItem(row, 0, item);
	    ++row;
	}
    }

    tableWidget_Thresholds->setRowCount(row);
}

void
UpdateParameters::accept()
{
    dqm_config::is::DQParameter param;

    int rows = tableWidget_Parameters->rowCount();
    for (int i = 0; i < rows; ++i)
    {
	QString name = tableWidget_Parameters->item(i, 1)->text();
	QString value = tableWidget_Parameters->item(i, 0)->text();
	param.Parameters.push_back((name + "=" + value).toStdString());
    }

    rows = tableWidget_Thresholds->rowCount();
    for (int i = 0; i < rows; ++i)
    {
	QString name = tableWidget_Thresholds->item(i, 2)->text();
	QString red_value = tableWidget_Thresholds->item(i, 0)->text();
	QString green_value = tableWidget_Thresholds->item(i, 1)->text();

	if (!red_value.isEmpty())
	    param.RedThresholds.push_back((name + "=" + red_value).toStdString());

	if (!green_value.isEmpty())
	    param.GreenThresholds.push_back((name + "=" + green_value).toStdString());
    }

    ISInfoDictionary dictionary(
	IPCPartition(m_parameter->getParent()->model().getPartitionName()));
    std::string name("DQMConfig." + m_parameter->getName().toStdString());

    try {
	dictionary.checkin(name, param);
    }
    catch (daq::is::Exception & ex) {
	ers::error(ex);
    }
    hide();
}


/*! \file OverlayWidget.cpp Implements the dqm_display::gui::OverlayWidget class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/OverlayWidget.h>
#include <dqm_display/gui/Display.h>

#include <QDebug>
#include <QApplication>
#include <QResizeEvent>
#include <QPainter>
#include <QGraphicsBlurEffect>

using namespace dqm_display::gui;

OverlayWidget::OverlayWidget(QWidget * parent)
    : QWidget(parent)
{
    setAttribute(Qt::WA_NoSystemBackground);
    QApplication::setOverrideCursor(Qt::WaitCursor);

    parent->installEventFilter(this);
    raise();
    show();
    resize(parent->size());

    QList<QWidget*> children = parent->findChildren<QWidget*>();
    for (auto a: children)
    {
	if (a != this)
	    a->setGraphicsEffect(new QGraphicsBlurEffect());
    }
    parent->repaint();
}

OverlayWidget::~OverlayWidget()
{
    parentWidget()->removeEventFilter(this);

    QList<QWidget*> children = parentWidget()->findChildren<QWidget*>();
    for (auto a: children)
    {
	a->setGraphicsEffect(0);
    }
    QApplication::restoreOverrideCursor();
}

bool
OverlayWidget::eventFilter(QObject * obj, QEvent * ev)
{
    if (obj == parent())
    {
	if (ev->type() == QEvent::Resize)
	{
	    QResizeEvent * rev = static_cast<QResizeEvent*>(ev);
	    resize(rev->size());
	}
    }
    return QWidget::eventFilter(obj, ev);
}

void
OverlayWidget::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.fillRect(rect(), QColor(255, 255, 255, 128));

    p.setPen(QColor(128, 128, 128, 255));
    p.setFont(QFont("arial,helvetica", 48));
    p.drawText(rect(), "Reloading configuration...", Qt::AlignHCenter | Qt::AlignVCenter);
}

/*
 * SystemFilterModel.cpp
 *
 *  Created on: Oct 9, 2013
 *      Author: kolos
 */

#include <dqm_display/gui/SystemFilterModel.h>

using namespace dqm_display::gui;

SystemFilterModel::SystemFilterModel(QObject * parent, QAbstractItemModel* source)
  : QSortFilterProxyModel(parent)
{
    setSourceModel(source);
}

bool
SystemFilterModel::filterAcceptsRow(int /*sourceRow*/,
         const QModelIndex &sourceParent) const
{
    return (!sourceParent.isValid()
	    || !sourceParent.parent().isValid()
	    || !sourceParent.parent().parent().isValid());
}



#include <dqm_display/gui/Display.h>
#include <dqm_display/gui/Histogram.h>
#include <dqm_display/gui/Status.h>
#include <dqm_display/core/Parameter.h>

#include <HistogramStyles/HistogramStyles.h>

#include <TAttLine.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH1.h>
#include <TH2.h>
#include <TImage.h>
#include <TObject.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TColor.h>
#include <TPaveStats.h>
#include <TROOT.h>
#include <TText.h>
#include <TPaveText.h>
#include <TVirtualX.h>

#include <QApplication>
#include <QDateTime>
#include <QDebug>
#include <QPainter>
#include <QResizeEvent>

namespace {
    TCanvas * canvas() {
        static QWidget w;
        static TCanvas * c = new TCanvas("histogram", 0, 0,
                gVirtualX->AddWindow(w.winId(), 2000, 2000));
        return c;
    }
}

using namespace dqm_display::gui;

Histogram::Histogram(TCanvas * c)
: m_canvas(c),
  m_image(TImage::Create()),
  m_histogram(0),
  m_time(0),
  m_reference(0),
  m_reference_pointer(0),
  m_clone_reference(0),
  m_scale_ref(true),
  m_multiGraph(0),
  m_legend(0),
  m_type(OTHER_TYPE),
  m_ref_type(OTHER_TYPE),
  m_split_canvas(false),
  m_empty(true)
{}

Histogram::Histogram(const QString & name, const std::string & drawOption,
        const std::string & canvasOption, bool scaleRef)
  : m_name(name),
    m_canvas(canvas()),
    m_image(TImage::Create()),
    m_histogram(0),
    m_time(0),
    m_reference(0),
    m_reference_pointer(0),
    m_clone_reference(0),
    m_draw_option(drawOption),
    m_canvas_option(canvasOption),
    m_scale_ref(scaleRef),
    m_multiGraph(0),
    m_legend(0),
    m_type(OTHER_TYPE),
    m_ref_type(OTHER_TYPE),
    m_split_canvas(false),
    m_empty(false)
{}

Histogram::~Histogram() {
    delete m_histogram;
    delete m_reference;
    delete m_legend;
    delete m_multiGraph;
    delete m_clone_reference;
    delete m_image;
}

void Histogram::clear() {
    update(0, 0, 0, true);
    m_empty = true;
}

void Histogram::setNode(const core::Parameter * parameter) {
    if (not parameter) {
        return ;
    }
    copyFrom(parameter->getHistogram());
    m_empty = false;
}

QImage
Histogram::getImage(const QSize & s)
{
    if (not m_histogram && not m_reference) {
        return QImage();
    }

    if (s.isValid() && !s.isEmpty() && m_size != s) {
        // The order of the following calls is important!
        m_size = s;
        m_canvas->SetCanvasSize(m_size.width(), m_size.height());
        gPad = m_canvas;
        m_canvas->ResizePad();
        reset();
    }

    if (!m_image->IsValid()) {
        paint();
        m_canvas->Modified();
        m_canvas->Update();

        m_image->FromPad(m_canvas);
    }

    uchar * arr = (uchar *)m_image->GetArgbArray();
    return QImage(arr, m_image->GetWidth(), m_image->GetHeight(),
            m_image->GetWidth() * 4, QImage::Format_ARGB32);
}

void Histogram::reset()
{
    m_image->SetImage(0);
}

void
Histogram::setPadsOptions(TPad * pad1, TPad * pad2)
{
    QStringList commands = QString::fromStdString(m_canvas_option).split(';', Qt::SkipEmptyParts);
    for (int i = 0; i < commands.size(); ++i )
    {
        QByteArray func = commands.at(i).section('(', 0, 0).trimmed().toLocal8Bit();
        QByteArray args = commands.at(i).section('(', 1).section(')', 0, 0).trimmed().toLocal8Bit();

        ERS_DEBUG(1, "Executing '" << func.data()
        	<< "(" << args.data() << ")' function");

        if (pad1) {
            int error = 0;
            pad1 -> Execute(func.data(), args.data(), &error);
            if (error) {
                ERS_LOG("Got " << error << " error while executing '"
                        << func.data() << "(" << args.data() << ")' function");
                continue;
            }
        }

        if (pad2) {
            int error = 0;
            pad2 -> Execute(func.data(), args.data(), &error);
            if (error) {
                ERS_LOG("Got " << error << " error while executing '"
                        << func.data() << "(" << args.data() << ")' function");
            }
        }
    }
}

void Histogram::copyFrom(const Histogram & h) {
    m_draw_option = h.m_draw_option;
    m_canvas_option = h.m_canvas_option;
    m_scale_ref = h.m_scale_ref;

    update(h.m_histogram ? h.m_histogram->Clone() : 0, h.m_time, h.m_reference, true);
}

void
Histogram::update(TObject* h, std::time_t time, TObject* r, bool resetCanvas)
{
    if (m_histogram == h && m_reference_pointer == r) {
        return;
    }

    delete m_histogram, m_histogram = 0;
    delete m_multiGraph, m_multiGraph = 0;
    m_type = OTHER_TYPE;

    bool modified = setHistogram(h, m_histogram, m_type);
    if (m_reference_pointer != r) {
        // This is a workaround for the bad design of the AlgorithmConfig class
        // Its getReference() function returns a pointer to a reference, which
        // may be destroyed if the reference is updated
        // TODO: Fix this in the dqm_config package
        m_reference_pointer = r;
        delete m_reference, m_reference = 0;
        if (r) {
            modified = setHistogram(r->Clone(), m_reference, m_ref_type) || modified;
        }
    }

    m_time = time;
    m_split_canvas = m_reference && ((m_type != TH1_TYPE && m_type != TGraph_TYPE) || m_type != m_ref_type);

    if (!m_split_canvas) {
        if (m_reference) {
            TAttLine * al = dynamic_cast<TAttLine*>(m_reference);
            if (al) {
                al->SetLineColor(6);
                al->SetLineStyle(12);
            }
            if (m_histogram) {
                if (m_ref_type == TGraph_TYPE && m_type == TGraph_TYPE)
                {
                    m_multiGraph = new TMultiGraph(m_histogram->GetName(), m_histogram->GetTitle());
                    m_multiGraph->Add((TGraph*) m_histogram->Clone());
                    m_multiGraph->Add((TGraph*) m_reference->Clone());
                }
            }
        }
    }

    if (resetCanvas) {
        m_canvas->Clear();
        m_canvas->ResetAttPad();
        m_canvas->UseCurrentStyle();
        if (m_split_canvas) {
            m_canvas->Divide(1, 2);
            setPadsOptions((TPad*)m_canvas->cd(1), (TPad*)m_canvas->cd(2));
        }
        else {
            setPadsOptions(m_canvas, 0);
        }
        delete m_legend;
        m_legend = 0;
    }

    reset();
}

void
Histogram::paint()
{
    m_canvas->Clear();
    m_canvas->ResetAttPad();
    m_canvas->UseCurrentStyle();

    if (m_split_canvas)
    {
        m_canvas->Divide(1, 2);
        setPadsOptions((TPad*)m_canvas->cd(1), (TPad*)m_canvas->cd(2));

	m_canvas->cd(1);
	plotHistogram(false);

	m_canvas->cd(2);
	plotReference();
    }
    else
    {
        m_canvas->cd();
        setPadsOptions(m_canvas, 0);

        if (m_multiGraph) {
            m_multiGraph->Draw(m_draw_option.c_str());
        }
        else {
            plotHistogram(m_reference && m_type == m_ref_type);
            if (m_ref_type == m_type) {
                plotReference();
            }
        }
        if (m_legend) {
            m_legend->Draw();
        }
    }
}

void
Histogram::plotTime()
{
    QByteArray time = QDateTime::fromTime_t(m_time).toString("yyyy-MMM-dd hh:mm:ss").toLocal8Bit();

    TText t;
    t.SetNDC(kTRUE);
    t.SetTextAlign(11);
    t.SetTextColor(kYellow+3);
    t.SetTextFont(22);
    t.SetTextSize(0.04);
    t.DrawText(0.01, 0.87, time.data());
}

void
Histogram::plotReference()
{
    if (!m_reference) {
	return ;
    }

    if (!m_split_canvas && !m_histogram) {
        return;
    }

    TNamed * reference = m_reference;
    reference->SetTitle("Reference");

    if (m_histogram) {
        if (m_ref_type == TH1_TYPE || m_ref_type == TH2_TYPE || m_ref_type == TH3_TYPE)
        {
            TH1 * histogram = static_cast<TH1*>(m_histogram);
            TH1 * th1_ref = static_cast<TH1*>(m_reference);

            if (m_scale_ref) {
                double r_norm = th1_ref->GetSumOfWeights();
                double h_norm = histogram->GetSumOfWeights() ? histogram->GetSumOfWeights() : 1;

                if (r_norm * h_norm != 0) {
                    th1_ref->Scale(h_norm/r_norm);
                }
                reference->SetTitle("Normalized Reference");
            }
            if (Display::isDivideByReferenceOn() && m_type == m_ref_type)
            {
                delete m_clone_reference;
                m_clone_reference = static_cast<TH1*>(m_histogram->Clone());
                if (m_clone_reference->Divide(th1_ref)) {
                    std::string title("Input divided by ");
                    title += reference->GetTitle();
                    m_clone_reference->SetTitle(title.c_str());
                    m_clone_reference->SetLineColor(6);
                    m_clone_reference->SetLineStyle(12);
                    reference = m_clone_reference;
                } else {
                    delete m_clone_reference;
                    m_clone_reference = 0;
                }
            }
        }
        if (!m_split_canvas) {
            delete m_legend;
            m_legend = new TLegend(0.01, 0.90, 0.5, 0.99);
            HistogramStyles::rootUtils->myLegendSetting(m_legend);
            m_legend->AddEntry(m_histogram, m_histogram->GetTitle(), "l");
            m_legend->AddEntry(reference, reference->GetTitle(), "l");
        }
    }

    std::string dopt = m_draw_option + (m_split_canvas ? "" : "same");
    reference->Draw(dopt.c_str());
}

void Histogram::plotHistogram(bool noTitle)
{
    if (m_empty) {
        return;
    }

    if (not m_histogram) {
        TPaveText* text = new TPaveText(0.03, 0.03, 0.97, 0.97, "blNDC");
        text->SetBit(kCanDelete);
        text->SetFillColor(19);
        text->SetLineWidth(0);
        text->SetBorderSize(1);

        TText* t = text->AddText(0.5, 0.5, "Histogram not found");
        t->SetTextSize(0.07);
        t->SetTextAlign(22);
        t->SetTextFont(62);
        t->SetTextColor(16);

        text->Draw();
	return;
    }

    m_histogram->SetBit(TH1::kNoTitle, noTitle);

    m_histogram->Draw(m_draw_option.c_str());

    plotTime();
}

bool Histogram::setHistogram(TObject *histo, TNamed*& m_h, HistoType& m_t)
{
    if (histo == m_h) {
        return false;
    }

    HistoType old_type = m_t;
    if (histo != NULL && histo->InheritsFrom("TNamed"))
    {
	m_h = static_cast<TNamed*>(histo);
	if (histo->InheritsFrom("TH3")) {
	    m_t = TH3_TYPE;
	    m_draw_option = (m_draw_option == "") ? "iso" : m_draw_option;
	}
	else if (histo->InheritsFrom("TH2")) {
	    m_t = TH2_TYPE;
	    m_draw_option = (m_draw_option == "") ? "colz" : m_draw_option;
	}
	else if (histo->InheritsFrom("TH1")) {
	    m_t = TH1_TYPE;
	}
	else if (histo->InheritsFrom("TGraph")) {
	    m_t = TGraph_TYPE;
	    m_draw_option = (m_draw_option == "") ? "alp" : m_draw_option;
	}
	else if (histo->InheritsFrom("TGraph2D")) {
	    m_t = TGraph2D_TYPE;
	    m_draw_option = (m_draw_option == "") ? "pcol" : m_draw_option;
	}
	else {
	    m_t = OTHER_TYPE;
	}
    }
    else {
	m_h = NULL;
	m_t = OTHER_TYPE;
    }
    return old_type != m_t;
}

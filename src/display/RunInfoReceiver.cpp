#include <dqm_display/gui/RunInfoReceiver.h>

#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/infodynany.h>

using namespace dqm_display::gui;

RunInfoReceiver::RunInfoReceiver(   const QString & partition_name,
				    const QString & name,
				    Callback callback )
  : m_receiver(IPCPartition(partition_name.toStdString()), true /*serialize callbacks*/),
    m_callback(callback)
{    
    connect( this, SIGNAL(runInfoUpdated(const ISInfoDynAny *)),
  	     this, SLOT(when_runInfoUpdated(const ISInfoDynAny *)), Qt::QueuedConnection );

    try {
	m_receiver.subscribe(name.toStdString(), &RunInfoReceiver::callbackReceived, this,
		{ ISInfo::Created, ISInfo::Updated, ISInfo::Subscribed });
    }
    catch (daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void 
RunInfoReceiver::callbackReceived(ISCallbackInfo * isc)
{
    ISInfoDynAny * any = new ISInfoDynAny;
    isc->value( *any );
    emit runInfoUpdated( any );
}

void
RunInfoReceiver::when_runInfoUpdated(const ISInfoDynAny * any)
{
    m_callback( *any );
    delete any;
}


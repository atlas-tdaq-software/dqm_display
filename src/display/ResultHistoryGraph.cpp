/*! \file ResultHistoryGraph Declares the dqm_display::gui::ResultHistoryGraph class.
 * \author Yurij Ilchenko, Kevin Slagle, Serguei Kolos
 */

#include <dqm_display/gui/ResultHistoryGraph.h>
#include <dqm_display/gui/Status.h>

#include <TAxis.h>
#include <TCanvas.h>
#include <TVirtualX.h>

#include <QDebug>
#include <QImage>
#include <QWidget>

namespace {
    TCanvas * canvas() {
        static QWidget w;
        static TCanvas * c = []() {
            auto c = new TCanvas("graph", 0, 0,
                    gVirtualX->AddWindow(w.winId(), 2000, 2000));
            c->SetTopMargin(0.05);
            c->SetRightMargin(0.05);
            c->SetGridy(kTRUE);
            return c;
        }();
        return c;
    }
}

using namespace dqm_display::gui;

ResultHistoryGraph::ResultHistoryGraph(
        const std::string & name,
        const std::vector<dqmf::is::Result> & results, 
        const std::string & tag_name)
  : m_main(tag_name.empty()),
    m_title(m_main ? name.c_str() : tag_name.c_str()),
    m_canvas(canvas()),
    m_image(TImage::Create())
{
    m_graph.SetMarkerStyle(1);
    m_graph.SetLineColor(13);
    m_graph.SetLineStyle(2);
    
    const size_t size = results.size();

    m_markers.reserve(5);
    for (int i = 0; i < 5; ++i) {
        m_markers.emplace_back(size);
        auto & m = *m_markers.rbegin();
        m.SetBit(kCanDelete, 0);
        m.SetMarkerColor(Status::color_t((core::Result::Status)(i - 1)));
        m.SetMarkerStyle(33);
        m.SetMarkerSize(1);
    }

    if (m_main) {
        m_graph.GetYaxis()->SetNdivisions(0, false);
	for (size_t i = 0; i < size; ++i) {
            double x = (double)results[i].time().c_time();// - time_offset;
	    int y = (int)results[i].status;
	    m_graph.SetPoint(i, x, y);
            ERS_RANGE_CHECK(-1, results[i].status, 3);
            m_markers[results[i].status + 1].SetNextPoint(x, y);
	}
    }
    else {
        int cnt=0;
	for (size_t i = 0; i < size; ++i) {
	    for (size_t j = 0; j < results[i].tags.size(); j++) {
		if (results[i].tags[j].name == tag_name) {
                    double x = (double)results[i].time().c_time();// - time_offset;
                    double y = results[i].tags[j].value;
                    m_graph.SetPoint(cnt++, x, y);
                    ERS_RANGE_CHECK(-1, results[i].status, 3);
                    m_markers[results[i].status + 1].SetNextPoint(x, y);
		    break;
		}
	    }
	}
    }
    
    m_graph.GetXaxis()->SetLabelOffset(0.02);
    m_graph.GetXaxis()->SetLabelSize(0.06);
    m_graph.GetXaxis()->SetTimeDisplay(1);
    m_graph.GetXaxis()->SetTimeFormat("%H:%M%F1970-01-01 00:00:00");
    m_graph.GetXaxis()->SetNdivisions(4, true);

    m_graph.GetYaxis()->SetLabelOffset(0.02);
    m_graph.GetYaxis()->SetLabelSize(0.06);
}

QImage
ResultHistoryGraph::getImage(const QSize & s)
{
    if (m_size != s) {
        m_size = s;
        m_canvas->SetCanvasSize(s.width(), s.height());
        gPad = m_canvas;
        m_canvas->ResizePad();
        m_image->SetImage(0);
    }

    if (not m_image->IsValid()) {
        m_canvas->SetGridx(not m_main);
        m_canvas->SetLeftMargin(m_main ? 0.05 : 0.15);

        m_canvas->cd();
	m_graph.Draw("ALP");

	// Reversed order is important
	for (auto it = m_markers.rbegin(); it != m_markers.rend(); ++it) {
	    if (it->GetN()) {
	        it->Draw();
	    }
	}

        m_canvas->Modified();
        m_canvas->Update();

        m_image->FromPad(m_canvas);
    }

    uchar * arr = (uchar *)m_image->GetArgbArray();
    return QImage(arr, m_image->GetWidth(), m_image->GetHeight(),
            m_image->GetWidth() * 4, QImage::Format_ARGB32);
}

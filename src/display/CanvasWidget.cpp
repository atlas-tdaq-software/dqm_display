#include <dqm_display/gui/CanvasWidget.h>

#include <TCanvas.h>
#include <TImage.h>
#include <TROOT.h>
#include <TVirtualX.h>

#include <QApplication>
#include <QDebug>
#include <QPainter>
#include <QMouseEvent>
#include <QResizeEvent>

using namespace dqm_display::gui;

CanvasWidget::CanvasWidget(QWidget* parent, int w, int h)
: QWidget(parent),
  m_canvas(nullptr)
{
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setUpdatesEnabled(false);
    setMouseTracking(true);

    int wid = gVirtualX->AddWindow((ULong_t)winId(), w, h);
    m_canvas = new TCanvas("Root Canvas", width(), height(), wid);
}

CanvasWidget::~CanvasWidget() {
    delete m_canvas;
}

void CanvasWidget::refresh()
{
    if (m_canvas) {
        paintCanvas();
        m_canvas->Modified();
        update();
    }
}

void CanvasWidget::update()
{
    if (m_canvas) {
        m_canvas->Resize();
        m_canvas->Update();
    }
}

void CanvasWidget::mouseMoveEvent(QMouseEvent *e)
{
    if (e->buttons() & Qt::LeftButton) {
        m_canvas->HandleInput(kButton1Motion, e->x(), e->y());
    }
    else if (e->buttons() & Qt::MiddleButton) {
        m_canvas->HandleInput(kButton2Motion, e->x(), e->y());
    }
    else if (e->buttons() & Qt::RightButton) {
        m_canvas->HandleInput(kButton3Motion, e->x(), e->y());
    }
    else {
        m_canvas->HandleInput(kMouseMotion, e->x(), e->y());
    }
}

void CanvasWidget::mousePressEvent(QMouseEvent *e)
{
    switch (e->button()) {
        case Qt::LeftButton:
            m_canvas->HandleInput(kButton1Down, e->x(), e->y());
            break;
        case Qt::MiddleButton:
            m_canvas->HandleInput(kButton2Down, e->x(), e->y());
            break;
        case Qt::RightButton:
            m_canvas->HandleInput(kButton3Down, e->x(), e->y());
            break;
        default:
            break;
    }
}

void CanvasWidget::mouseReleaseEvent(QMouseEvent *e)
{
    switch (e->button()) {
        case Qt::LeftButton:
            m_canvas->HandleInput(kButton1Up, e->x(), e->y());
            break;
        case Qt::MiddleButton:
            m_canvas->HandleInput(kButton2Up, e->x(), e->y());
            break;
        case Qt::RightButton:
            m_canvas->HandleInput(kButton3Up, e->x(), e->y());
            break;
        default:
            break;
    }
}

void CanvasWidget::paintEvent(QPaintEvent * e)
{
    update();
}

void CanvasWidget::resizeEvent(QResizeEvent * e)
{
    if (e->size() != e->oldSize()) {
        QApplication::postEvent(this, new QPaintEvent(rect()));
    }
}

void CanvasWidget::showEvent(QShowEvent * e)
{
    update();
}

bool CanvasWidget::event(QEvent* e) {
    if (e->type() == QEvent::WindowActivate) {
        update();
    }
    return QWidget::event(e);
}

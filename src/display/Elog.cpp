/*! \file Elog.cpp Implements the dqm_display::gui::Elog class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <unistd.h>
#include <fstream>

#include <set>

#include <ers/Issue.h>

#include "elisa_client_api/c++/elisa_client_api.h"

#include <dqm_display/core/Parameter.h>
#include <dqm_display/gui/Elog.h>
#include <dqm_display/gui/Histogram.h>

#include <QDebug>
#include <QMenu>
#include <QGroupBox>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QPushButton>

using namespace dqm_display::gui;

Elog::Elog()
{
    setupUi(this);

    QLayout *layout = groupBoxSystems->layout();
    if (layout) {
	 for (int i = 0; i < layout->count(); ++i) {
	     QLayoutItem * item = layout->itemAt(i);
	     if (!item)
		 continue;

	     QAbstractButton * cbox = dynamic_cast<QAbstractButton*>(item->widget());

	     if (!cbox)
		 continue;

	     m_systems.addButton(cbox);
	 }
    }
    m_systems.setExclusive(false);

    connect( lineEditFullName, SIGNAL(textEdited(const QString &)),
	     this, SLOT(when_stateChanged()));

    connect( lineEditUserID, SIGNAL(textEdited(const QString &)),
             this, SLOT(when_stateChanged()));

    connect( lineEditPassword, SIGNAL(textEdited(const QString &)),
	     this, SLOT(when_stateChanged()));

    connect( lineEditSubject, SIGNAL(textEdited(const QString &)),
	     this, SLOT(when_stateChanged()));

    connect( plainTextEditMessage, SIGNAL(textChanged()),
	     this, SLOT(when_stateChanged()));

    connect( &m_systems, SIGNAL(buttonClicked(int )),
	     this, SLOT(when_stateChanged()));

    connect( listWidgetAttachments, SIGNAL(itemChanged(QListWidgetItem * )),
	     this, SLOT(when_attachmentStateChanged(QListWidgetItem *)));

    buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

void
Elog::when_stateChanged()
{
    bool ok_enabled = !(lineEditUserID->text().trimmed().isEmpty()
		|| lineEditFullName->text().trimmed().isEmpty()
                || lineEditPassword->text().trimmed().isEmpty()
		|| lineEditSubject->text().trimmed().isEmpty()
		|| plainTextEditMessage->toPlainText().trimmed().isEmpty()
		|| m_systems.checkedId() == -1);

    buttonBox->button(QDialogButtonBox::Ok)->setEnabled(ok_enabled);
}

bool
Elog::runningAtP1()
{
    static const char * instpath = getenv( "TDAQ_INST_PATH" );
    static const bool atP1 = []() {
        if ( !instpath ) {
            return false;
        }
        std::string filename = instpath;
        filename += "/com/ipc_reference_location";
        if ( std::ifstream( filename.c_str() ) ) {
            return true;
        }
        return false;
    }();

    return atP1;
}

void
Elog::addAttachment(const core::TreeNode * node)
{
    addAttachment(static_cast<const core::Parameter*>(node)->getHistogram());
}

void
Elog::addAttachment(Histogram & histogram)
{
    QImage image = histogram.getImage(QSize(600, 400));

    if (image.isNull())
        return;

    QPixmap pixmap = QPixmap::fromImage(image);
    QListWidgetItem * item = new QListWidgetItem(QIcon(pixmap), histogram.getName());
    item->setCheckState(Qt::Checked);
    listWidgetAttachments->addItem(item);
    m_images.insert(item, pixmap);

    QString ss = listWidgetAttachments->styleSheet();
    if (ss.contains("url(:/images/AttachmentHint.png)")) {
        listWidgetAttachments->setStyleSheet(ss.replace("url(:/images/AttachmentHint.png)", "none"));
    }
}

void
Elog::when_attachmentStateChanged(QListWidgetItem * item)
{
    if (item->checkState() == Qt::Unchecked) {
	delete item;
    }
}

void
Elog::accept()
{
    QString systems("Monitoring, Data Quality");

    QList<QAbstractButton*> buttons = m_systems.buttons();
    for (int i = 0; i < buttons.size(); ++i) {
	if (buttons[i]->isChecked())
	    systems += "," + buttons[i]->text();
    }

    std::vector<std::string> argv(
	    {"-y", "Data Quality",
             "-p", "DQ_Type=online",
//             "-k", "TEST",
//             "-s", "https://pc-atd-elisa.cern.ch",
//             "-o", "/afs/cern.ch/user/k/kolos/Desktop/ssocookie",
             "-c", (lineEditUserID->text() + ":" + lineEditPassword->text()).toStdString(),
	     "-a", lineEditFullName->text().toStdString(),
	     "-e", systems.toStdString(),
	     "-j", lineEditSubject->text().toStdString(),
	     "-b", plainTextEditMessage->toPlainText().toStdString() });

    std::set<std::string> files;
    for (int row = 0; row < listWidgetAttachments->count(); ++row)
    {
        QListWidgetItem *item = listWidgetAttachments->item(row);

        auto it = m_images.find(item);
        if (it == m_images.end()) {
            continue;
        }
        QString fname("/tmp/" + QString("%1_").arg(::getpid()) + item->text() + QString("_%1.png").arg(row));
        QFile file(fname);
        file.open(QIODevice::WriteOnly);
        it.value().save(&file, "PNG");

        argv.push_back("-m");
        argv.push_back(fname.toStdString());

        files.insert(fname.toStdString());
    }

    bool success = true;
    try {
        QApplication::setOverrideCursor(Qt::WaitCursor);
	elisa::ELisA_clientAPI elisa;
        elisa.elisa_insert(argv);
        listWidgetAttachments->clear();
        m_images.clear();
        QApplication::restoreOverrideCursor();
    }
    catch(ers::Issue & ex) {
        success = false;
        QApplication::restoreOverrideCursor();
        QMessageBox::warning(this,
	         QString("Sending message failed"),
	         QString::fromStdString(ex.what()), QMessageBox::Ok);
    }

    for (std::set<std::string>::const_iterator it = files.begin();
	    it != files.end(); ++it)
    {
	::unlink(it->c_str());
    }

    if (success) {
        hide();
    }
}


/*! \file Display.cpp Implements the Display class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/Display.h>
#include <dqm_display/gui/Find.h>
#include <dqm_display/gui/ListViewHistoryModel.h>
#include <dqm_display/gui/DetailsViewDelegate.h>
#include <dqm_display/gui/IconsViewDelegate.h>
#include <dqm_display/gui/RunInfoWidget.h>
#include <dqm_display/gui/Elog.h>
#include <dqm_display/gui/Status.h>
#include <dqm_display/gui/OverlayWidget.h>

#include <dqm_display/core/Region.h>
#include <dqm_display/core/Parameter.h>
#include <dqm_display/core/TreeDataModel.h>

#include <QColor>
#include <QDebug>
#include <QUrl>
#include <QSettings>

#include <TSystem.h>

using namespace dqm_display::gui;

bool Display::m_divide_by_reference = false;

Display::Display(DQDataModel * model, About * about)
  : m_mainModel(model),
    m_systemModel(new SystemFilterModel(this, m_mainModel)),
    m_parameterModel(new ParameterFilterModel(this, m_mainModel)),
    m_findDialog(new Find(*m_mainModel)),
    m_aboutDialog(about),
    m_systemActionGroup(this),
    m_viewActionGroup(this),
    m_history_position(0),
    m_current_view(0),
    m_histograms_height(192),
    m_font_size(12),
    m_timer(this)
{  
    setupUi(this);

    m_listViewHistoryModel = new ListViewHistoryModel(listView_History,
            QString::fromStdString(m_mainModel->model()->getPartitionName()));

    widget_AlarmsAndLogs->subscribe(m_mainModel->model()->getPartitionName());

    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

    actionView_Graphics->setData((int)0);
    actionView_Icons->setData((int)1);
    actionView_Details->setData((int)2);

    m_viewActionGroup.addAction(actionView_Icons);
    m_viewActionGroup.addAction(actionView_Details);
    m_viewActionGroup.addAction(actionView_Graphics);
    connect(&m_viewActionGroup, SIGNAL(triggered(QAction*)),
            this, SLOT(when_actionViewTriggered(QAction*)));
    
    m_current_view = actionView_Graphics;

    listView_History->setModel(m_listViewHistoryModel);
    listView_History->setItemDelegate(
	    new IconsViewDelegate(QSize(100, 100), 5, listView_History));
    
    listView_Histograms->setModel(m_mainModel);
    listView_Histograms->setInteractive();
    listView_Histograms->setItemDelegate(
	    new IconsViewDelegate(
		    QSize(m_histograms_height, m_histograms_height), 10, this));

    treeView_Histograms->setModel(m_mainModel);
    treeView_Histograms->setItemDelegate(
	    new DetailsViewDelegate(m_histograms_height, m_font_size, 5,
	            m_mainModel, this));
    treeView_Histograms->header()->resizeSection(0, 300);
    treeView_Histograms->header()->resizeSection(1, 100);
    
    treeView_Main->setModel(m_mainModel);
    treeView_Main->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    for (int i = 1; i < m_mainModel->columnCount(); ++i) {
	treeView_Main->setColumnHidden(i, true);
    }

    treeView_Systems->setProxyModel(m_systemModel);
    treeView_Systems->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    for (int i = 1; i < m_mainModel->columnCount(); ++i) {
	treeView_Systems->setColumnHidden(i, true);
    }
    treeView_Systems->setRootIndex(m_mainModel->rootIndex());
    treeView_Systems->expandAll();
    dockWidget_Systems->setWindowTitle(m_mainModel->model()->getRootRegion()->getName());
    
    connect( m_findDialog, SIGNAL(nodeActivated(const core::TreeNode*)),
	     this, SLOT(when_nodeActivated(const core::TreeNode*)));
    
    connect( treeView_Systems, SIGNAL(nodeActivated(const core::TreeNode*)),
           this, SLOT(when_nodeActivated(const core::TreeNode*)));

    connect( treeView_Main, SIGNAL(nodeActivated(const core::TreeNode*)),
           this, SLOT(when_nodeActivated(const core::TreeNode*)));
           
    connect( graphicsView_Layout, SIGNAL(nodeActivated(const core::TreeNode*)),
           this, SLOT(when_nodeActivated(const core::TreeNode*)));
           
    connect( listView_Histograms, SIGNAL(nodeActivated(const core::TreeNode*)),
           this, SLOT(when_nodeActivated(const core::TreeNode*)));
           
    connect( treeView_Histograms, SIGNAL(nodeActivated(const core::TreeNode*)),
           this, SLOT(when_nodeActivated(const core::TreeNode*)));
    
    connect( actionView_Show_Tree, SIGNAL(toggled(bool)),
           treeView_Main, SLOT(setVisible(bool)));

    connect( widget_AlarmsAndLogs, SIGNAL(alarmAcknowledged(const core::TreeNode*)),
	     this, SLOT(when_nodeActivated(const core::TreeNode*)));

    connect( this, SIGNAL(fontSizeChanged(int)),
        textEdit_Parameters, SLOT(when_fontSizeChanged(int)));

    // The following connections are done via queue as corresponding
    // signals are emitted from non-Qt threads
    connect( m_mainModel, SIGNAL(resultProduced(const core::TreeNode*)),
	   this, SLOT(when_resultProduced(const core::TreeNode*)), Qt::QueuedConnection);

    connect( m_mainModel, SIGNAL(alarmProduced(const core::TreeNode*)),
	   widget_AlarmsAndLogs, SLOT(when_alarmProduced(const core::TreeNode*)), Qt::QueuedConnection);

    connect( m_mainModel, SIGNAL(configurationUpdated()),
	    this, SLOT(when_configurationUpdated()), Qt::QueuedConnection);

    connect( m_mainModel, SIGNAL(statsUpdated(const QString &, int, int)),
	    this, SLOT(when_statsUpdated(const QString &, int, int)), Qt::QueuedConnection);

    action_Elog->setEnabled(Elog::runningAtP1());
    action_AddToElog->setEnabled(Elog::runningAtP1());
    
    RunInfoWidget * ri = new RunInfoWidget(
            QString::fromStdString(m_mainModel->model()->getPartitionName()));

    connect(ri, SIGNAL(databaseRestarted()),
	    this, SLOT(when_configurationUpdated()), Qt::QueuedConnection);

    connect(ri, SIGNAL(runStarted()),
            this, SLOT(when_runStarted()), Qt::QueuedConnection);

    statusBar()->addPermanentWidget(ri, 3);

    statusBar()->addPermanentWidget(m_stat_label = new QLabel(), 2);

    statusBar()->addPermanentWidget(m_path_label = new QLabel(), 1);

    resizeDocks({dockWidget_Parameter, dockWidget_Logs}, {500, 500}, Qt::Horizontal);
    resizeDocks({dockWidget_Logs}, {140}, Qt::Vertical);
    restoreState();

    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(handleRootEvents()));
    m_timer.start(10);

    m_mainModel->subscribeForUpdates();
    setDropShadowEffect();

    actionNavigation_Home->trigger();
}

Display::~Display()
{
    delete m_findDialog;
}

void Display::handleRootEvents()
{
   //call the inner loop of ROOT
   gSystem->ProcessEvents();
}

void
Display::on_action_AddToElog_triggered()
{
    Elog::instance().addAttachment(histogram_Widget->histogram());
}

void
Display::closeEvent(QCloseEvent* event)
{
    saveState();
    QMainWindow::closeEvent(event);
}

void
Display::signalImageSizeChange()
{
    emit viewHeightChanged(m_histograms_height,
            m_systemModel->index(0, 0));
}

void
Display::signalFontSizeChange()
{
    emit fontSizeChanged(m_font_size);
}

void Display::setDropShadowEffect() {
    m_histogram_Widget_shadow = new QGraphicsDropShadowEffect(widget_HistogramHolder);
    m_histogram_Widget_shadow->setBlurRadius(10);
    m_histogram_Widget_shadow->setOffset(QPointF(4, 6));
    widget_HistogramHolder->setGraphicsEffect(m_histogram_Widget_shadow);
}

void
Display::when_runStarted()
{
    m_mainModel->runStarted();
}

void
Display::when_configurationUpdated()
{
    m_mainModel->clear();
    graphicsView_Layout->clear();
    m_listViewHistoryModel->clear();
    textEdit_Parameters->clear();
    histogram_Widget->clear();

    {
        OverlayWidget ow(this);

        if (!m_mainModel->loadData()) {
            delete this;
            return ;
        }
    }

    // This shouldn't be necessary (?) but after reload
    // all hidden columns in the TreeViews show up again.
    // Looks like a bug in Qt.
    for (int i = 1; i < m_mainModel->columnCount(); ++i) {
        treeView_Main->setColumnHidden(i, true);
        treeView_Systems->setColumnHidden(i, true);
    }

    setDropShadowEffect();
    m_mainModel->subscribeForUpdates();

    treeView_Systems->setRootIndex(m_mainModel->rootIndex());
    treeView_Systems->expandAll();
    m_history.clear();
    m_history_position = 0;
    actionNavigation_Home->trigger();
}

void
Display::when_statsUpdated(const QString & last_update_time, int total_updates, int /*update_rate*/)
{
    QString text = QString("%1 updates received, last one at %2")
	    .arg(total_updates).arg(last_update_time);
    m_stat_label->setText(text);
}

void
Display::saveState()
{
    QSettings settings("CERN", "DQMD");
    settings.setValue("/geometry", saveGeometry());
    settings.setValue("/mainWindowState", this->QMainWindow::saveState());
    settings.setValue("/parameterDockState", splitter_ParameterDock->saveState());
    settings.setValue("/histogramsHeight", m_histograms_height);
    settings.setValue("/fontSize", m_font_size);

    auto saveHeaders = [&settings](const char * name, QHeaderView * header) {
        settings.beginWriteArray(name);
        for (int i = 0; i < header->count(); ++i) {
            settings.setArrayIndex(i);
            settings.setValue("width", header->sectionSize(i));
        }
        settings.endArray();
    };

    saveHeaders("/detailsView/columns", treeView_Histograms->header());
    saveHeaders("/alarmsView/columns", widget_AlarmsAndLogs->tableWidget_Alarms->horizontalHeader());
    saveHeaders("/logsView/columns", widget_AlarmsAndLogs->tableWidget_Logs->horizontalHeader());
}

void
Display::restoreState()
{
    QSettings settings("CERN", "DQMD");
    restoreGeometry(settings.value("/geometry").toByteArray());
    this->QMainWindow::restoreState(settings.value("/mainWindowState").toByteArray());
    splitter_ParameterDock->restoreState(settings.value("/parameterDockState").toByteArray());
    m_histograms_height = settings.value("/histogramsHeight", m_histograms_height).toInt();
    m_font_size = settings.value("/fontSize", m_font_size).toInt();

    auto restoreHeaders = [&settings](const char * name, QHeaderView * header) {
        int size = settings.beginReadArray(name);
        for (int i = 0; i < size; ++i) {
            settings.setArrayIndex(i);
            int s = settings.value("width", header->sectionSize(i)).toInt();
            header->resizeSection(i, s);
        }
        settings.endArray();
    };

    restoreHeaders("/detailsView/columns", treeView_Histograms->header());
    restoreHeaders("/alarmsView/columns", widget_AlarmsAndLogs->tableWidget_Alarms->horizontalHeader());
    restoreHeaders("/logsView/columns", widget_AlarmsAndLogs->tableWidget_Logs->horizontalHeader());

    signalImageSizeChange();
    signalFontSizeChange();
}

void
Display::when_actionViewTriggered(QAction * action)
{
    stackedWidget->setCurrentIndex(action->data().toInt());

    m_current_view = action;

    bool zoomable = action == actionView_Icons || action == actionView_Graphics;
    actionView_ZoomIn->setEnabled(zoomable);
    actionView_ZoomOut->setEnabled(zoomable);
}

void
Display::activateNode(const core::TreeNode * node)
{
    QModelIndex index = m_mainModel->index(node);

    QModelIndex i = index;
    QModelIndex c = treeView_Systems->currentIndex();
    bool inplace = false;
    for( ; i.parent().isValid(); i = i.parent()) {
	if (c == i) {
	    inplace = true;
	    break;
	}
    }
    if (!inplace) {
	treeView_Systems->setCurrentIndex(i);
	treeView_Main->setRootIndex(i);
    }
    else {
	treeView_Main->setRootIndex(c);
    }

    treeView_Main->setCurrentIndex(index);
    updateViews(index);
}

void
Display::when_nodeActivated(const core::TreeNode * node)
{
    activateNode(node);
    addToHistory(node);
}


void
Display::updateHistogramView(const core::TreeNode * node, bool force) {
    if (not node) {
         return;
    }

    if (force || not histogram_Widget->isEnabled()) {
        histogram_Widget->setNode(node);
        core::Result::Status status = node->getResult()->status();
        QColor color(Status::color(status));
        color.setAlpha(128);
        m_histogram_Widget_shadow->setColor(color);
    }
    textEdit_Parameters->update(*node);
}

void 
Display::updateViews(const QModelIndex & index)
{
    if (!index.isValid()) {
    	return ;
    }
    
    m_current_index = index;

    const core::TreeNode * node = DQDataModel::treeNode(index);
    graphicsView_Layout->setNode(node);

    m_listViewHistoryModel->setNode(node);
    dockWidget_History->setWindowTitle(QString("History for \"%1\"").arg(node->getName()));

    m_path_label->setText(node->getFullName());

    if (!node->isLeaf())
    {
        QString title(node->getLabel());
        label_LayoutTitle->setText(title.replace("\n", " "));

        listView_Histograms->setRootIndex(index);
        treeView_Histograms->setRootIndex(index);

        dockWidget_Parameter->setWindowTitle("Histogram");
        splitter_ParameterDock->hide();
        textEdit_Parameters->clear();
        histogram_Widget->clear();
        m_histogram_Widget_shadow->setColor(QColor(128, 128, 128, 128));

        m_current_view->trigger();
        action_AddToElog->setEnabled(false);
    }
    else
    {
        QString title(node->getParent()->getLabel());
        label_LayoutTitle->setText(title.replace("\n", " "));
        listView_Histograms->setRootIndex(index.parent());
        listView_Histograms->setCurrentIndex(index);
        treeView_Histograms->setRootIndex(index.parent());
        treeView_Histograms->setCurrentIndex(index);

        dockWidget_Parameter->setWindowTitle(QString("%1").arg(node->getFullName()));
        splitter_ParameterDock->show();
        updateHistogramView(node);

	action_AddToElog->setEnabled(Elog::runningAtP1() && not histogram_Widget->isNull());
    }
}

void 
Display::on_actionView_ZoomIn_triggered()
{
    if (	stackedWidget->currentIndex() == 1 // icons view
	    ||	stackedWidget->currentIndex() == 2 // details view
	    ) {
	m_histograms_height = m_histograms_height * 6./5.;
	signalImageSizeChange();
    }
    else {
	graphicsView_Layout->scale(6./5.);
    }
}

void
Display::on_actionView_ZoomOut_triggered()
{
    if (	stackedWidget->currentIndex() == 1 // icons view
	    ||	stackedWidget->currentIndex() == 2 // details view
	    ) {
	m_histograms_height = m_histograms_height * 5./6.;
	signalImageSizeChange();
    }
    else {
	graphicsView_Layout->scale(5./6.);
    }
}

void Display::on_actionView_IncreaseFontSize_triggered()
{
    ++m_font_size;
    signalFontSizeChange();
}

void Display::on_actionView_DecreaseFontSize_triggered()
{
    if (m_font_size) {
        --m_font_size;
        signalFontSizeChange();
    }
}

void
Display::on_actionView_FitToView_triggered()
{
    graphicsView_Layout->fitSceneToView();
}

void
Display::on_actionView_DivideByReference_triggered() {
    m_divide_by_reference = actionView_DivideByReference->isChecked();
    histogram_Widget->refresh();
    emit iconsRepaintRequired(m_systemModel->index(0, 0));
}

void
Display::on_actionNavigation_Back_triggered()
{
    const core::TreeNode * node = m_history[--m_history_position];
    activateNode(node);
    updateNavigationButtons(node);
}

void 
Display::on_actionNavigation_Forward_triggered()
{
    const core::TreeNode * node = m_history[++m_history_position];
    activateNode(node);
    updateNavigationButtons(node);
}

void 
Display::on_actionNavigation_Home_triggered()
{
    when_nodeActivated(DQDataModel::treeNode(m_mainModel->rootIndex()));
}

void 
Display::on_actionNavigation_LevelUp_triggered()
{
    QModelIndex i = treeView_Main->currentIndex();
    if (not i.isValid()) {
        return;
    }
    if (DQDataModel::treeNode(i)->isLeaf()) {
        i = i.parent();
    }
    when_nodeActivated(DQDataModel::treeNode(m_mainModel->parent(i)));
}

void
Display::on_actionNavigation_Find_triggered()
{
    m_findDialog->show();
    m_findDialog->raise();
}

void
Display::addToHistory(const core::TreeNode * node)
{
    static const int MAX_HISTORY_SIZE = 100;

    if (!node) {
	return;
    }

    if (m_history_position < (m_history.size() - 1)) {
	m_history.remove(m_history_position + 1,
		m_history.size() - m_history_position - 1);
    }

    if (m_history_position < m_history.size()) {
        const core::TreeNode * last = m_history[m_history_position];
        if (node->isLeaf() && last->isLeaf()
                && node->getParent() == last->getParent()) {
            return;
        }
        if (node->isLeaf() && node->getParent() == last) {
            return;
        }
    }

    int tobe_removed = m_history.size() - MAX_HISTORY_SIZE;
    if (tobe_removed > 0) {
	m_history.remove(0, tobe_removed);
	m_history_position -= tobe_removed;
    }

    m_history.push_back(node);
    m_history_position = m_history.size() - 1;

    updateNavigationButtons(node);
}

void 
Display::updateNavigationButtons(const core::TreeNode * node)
{
    if (!node) {
	return;
    }

    actionNavigation_Back->setEnabled(m_history_position > 0);
    actionNavigation_Forward->setEnabled(m_history_position < (m_history.size()-1));
    actionNavigation_LevelUp->setEnabled(!node->isRoot());
}

void 
Display::when_resultProduced(const core::TreeNode * node)
{
    if (!node) {
	return;
    }

    QModelIndex index = m_mainModel->index(node);
    treeView_Systems->updateNode(index);
    treeView_Main->updateNode(index);

    if (index == m_current_index) {
        m_listViewHistoryModel->update();

        if (node->isLeaf()) {
            updateHistogramView(node, false);
            action_AddToElog->setEnabled(
                    Elog::runningAtP1() && not histogram_Widget->isNull());
        }
    }

    if (index == m_current_index
            || index.parent() == m_current_index
            || (node->isLeaf() && index.parent() == m_current_index.parent())) {
        if (stackedWidget->currentIndex() == 0) {
            graphicsView_Layout->updateNode(node);
        } else if (stackedWidget->currentIndex() == 1) {
            listView_Histograms->updateNode(index);
        } else if (stackedWidget->currentIndex() == 2) {
            treeView_Histograms->updateNode(index);
        }
    }
    else if (stackedWidget->currentIndex() == 0) {
        // Graphical view may display several levels of nested items simultaneously
        for (QModelIndex i = index.parent(); i.isValid(); i = i.parent()) {
            if (m_current_index == i) {
                graphicsView_Layout->updateNode(node);
                break;
            }
        }
    }
}

void
Display::on_actionHelp_About_triggered()
{
    m_aboutDialog->show();
}

void Display::on_action_Elog_triggered()
{
    Elog::instance().show();
}

void Display::on_pushButton_Lock_clicked(bool locked)
{
    histogram_Widget->setEnabled(locked);
    widget_HistogramBackground->setStyleSheet(locked
            ? "QWidget{background-color: black;}" : "QWidget{background-color: white;}");
    if (not locked) {
        QModelIndex i = treeView_Main->currentIndex();
        updateHistogramView(DQDataModel::treeNode(i));
    }
}

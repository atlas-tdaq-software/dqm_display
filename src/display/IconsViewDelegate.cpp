/*! \file IconsViewDelegate.cpp Implements the IconsViewDelegate class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_display/gui/IconsViewDelegate.h>
#include <dqm_display/gui/Status.h>
#include <dqm_display/gui/DQDataModel.h>

#include <QBitmap>
#include <QDebug>
#include <QLinearGradient>
#include <QMainWindow>
#include <QPainter>
#include <QPainterPath>
#include <QTextDocument>

using namespace dqm_display::gui;

IconsViewDelegate::IconsViewDelegate(const QSize & icon_size, int spacing,
        QObject *parent, bool follow_height_change)
  : QStyledItemDelegate(parent),
    m_original_spacing(spacing),
    m_margin(4),
    m_radius(5),
    m_shadow(6),
    m_spacing(m_original_spacing),
    m_size_hint(icon_size + QSize(2*m_spacing, 2*m_spacing)),
    m_selection_color(200, 200, 200)
{
    if (follow_height_change){
	connect(parent, SIGNAL(viewHeightChanged(int, const QModelIndex &)),
		this, SLOT(when_viewHeightChanged(int, const QModelIndex &)));
    }

    if (qobject_cast<QMainWindow*>(parent)) {
        connect(parent, SIGNAL(iconsRepaintRequired(const QModelIndex &)),
                this, SLOT(when_iconsRepaintRequired(const QModelIndex &)));
    }
}

void
IconsViewDelegate::when_viewHeightChanged(int height, const QModelIndex & index)
{
    m_size_hint = QSize(height, height);
    if (index.isValid())
	emit sizeHintChanged(index);
}

void 
IconsViewDelegate::paint( QPainter * painter, 
			 const QStyleOptionViewItem & option, 
                         const QModelIndex & index ) const
{
    painter->save();

    QFont font = qvariant_cast<QFont>(index.data(Qt::FontRole));
    int font_size = option.rect.height()/20;
    if (font_size < 16) {
        font_size = 16;
    }
    font.setPixelSize(font_size);
    painter->setFont(font);
    int titleHeight = painter->fontMetrics().height() + 4;

    if(option.state & QStyle::State_Selected) {
        painter->setPen(Qt::NoPen);
        painter->setBrush(QBrush(m_selection_color));
        painter->drawRoundedRect(option.rect, m_radius, m_radius, Qt::RelativeSize);
    }

    doPaint(painter, option, index, titleHeight);

    painter->restore();
}

void
IconsViewDelegate::doPaint( QPainter * painter,
			 const QStyleOptionViewItem & option,
                         const QModelIndex & index, int titleHeight ) const
{
    static QPixmap NotAvailablePixmap(QString::fromUtf8(":/images/not_found.png"));
    static QPixmap BackgroundPixmap(QString::fromUtf8(":/images/pattern.png"));

    painter->translate(option.rect.left(), option.rect.top());
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::TextAntialiasing, true);

    if ( index.column() == 0 ) {
	const core::TreeNode * node = DQDataModel::treeNode(index);

	if (!node)
	    return;

	QRect fullRect(m_spacing, m_spacing,
	        option.rect.width() - 2*m_spacing,
	        option.rect.height() - 2*m_spacing);
	QRect imageRect = fullRect.adjusted(m_margin, m_margin,
	        - m_margin - m_shadow,
		- m_margin - m_shadow - titleHeight);

	painter->setPen(Qt::NoPen);
        painter->setBrush(QBrush(Qt::NoBrush));

        QPainterPath img;
        img.addRoundedRect(imageRect, m_radius, m_radius, Qt::RelativeSize);

        // Shadow is in the main painter coordinates.
        QPainterPath shadow;
        shadow.addRoundedRect(imageRect.adjusted(m_shadow, m_shadow, m_shadow, m_shadow),
                m_radius, m_radius, Qt::RelativeSize);
        shadow = shadow.subtracted(img);

        // Mask will be drawn on the image. It is in the image local coordinates.
        QPainterPath mask;
        mask.addRect(imageRect);
        mask = mask.subtracted(img);
        mask = mask.translated(-imageRect.left(), -imageRect.top());

        bool drawShadow = true;

	if (index.data(Qt::DecorationRole).canConvert<dqm_display::gui::ResultHistoryGraph*>())
	{
	    ResultHistoryGraph * graph =
		qvariant_cast<dqm_display::gui::ResultHistoryGraph*>(index.data(Qt::DecorationRole));
	    QImage image = graph->getImage(imageRect.size());

            QPainter p(&image);
            p.setCompositionMode(QPainter::CompositionMode_Clear);
            p.fillPath(mask, QBrush(Qt::transparent));
	    painter->drawImage(imageRect, image);
	}
	else
	{
	    if (node->isLeaf()) {
		QImage image = node->getImage(imageRect.size());

		if (!image.isNull()) {
	            QPainter p(&image);
	            p.setCompositionMode(QPainter::CompositionMode_Clear);
	            p.fillPath(mask, QBrush(Qt::transparent));
		    painter->drawImage(imageRect, image);
		}
		else {
		    drawShadow = false;
		    painter->drawPixmap(imageRect, NotAvailablePixmap);
		}
	    }
	    else {
		painter->setBrush(QBrush(BackgroundPixmap));
		painter->drawRoundedRect(imageRect, m_radius, m_radius, Qt::RelativeSize);

		QTextDocument doc;
		doc.setHtml(node->getStatusHtml());
		painter->translate(imageRect.left() + m_margin, imageRect.top() + m_margin);
		doc.drawContents(painter, QRect(0, 0, imageRect.width(), imageRect.height()));
		painter->translate(-imageRect.left() - m_margin, -imageRect.top() - m_margin);
	    }
	}

	if (drawShadow) {
            core::Result::Status status = node->getResult()->status();

            QConicalGradient gradient(imageRect.left() + imageRect.width()/2,
                    imageRect.top() + imageRect.height()/2, 135);
            QColor color(Status::color(status));
            color.setAlpha(128);
            gradient.setColorAt(0, Qt::white);
            gradient.setColorAt(0.5, color);
            gradient.setColorAt(1, Qt::white);

            painter->fillPath(shadow, QBrush(gradient));

            if (status == core::Result::Status::DISABLED) {
                painter->setBrush(QColor(0, 0, 0, 96));
                painter->drawRoundedRect(fullRect, m_radius, m_radius, Qt::RelativeSize);
            }
	}

	if (titleHeight) {
            // draw title text
            QString text = qvariant_cast<QString>(index.data(Qt::DisplayRole));

            QColor textColor = option.state & QStyle::State_Selected
                    ? Qt::white : QColor(64, 64, 64, 255);
            QRect textRect(fullRect.left(), fullRect.top() + fullRect.height() - titleHeight,
                    fullRect.width(), titleHeight);
            painter->setPen(textColor);
            painter->drawText(textRect, Qt::AlignHCenter | Qt::AlignCenter, text);
	}
    }
}

QSize
IconsViewDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & ) const
{
    return QSize(m_size_hint.width(), m_size_hint.height());
}

void IconsViewDelegate::when_iconsRepaintRequired(const QModelIndex & index) {
    if (m_original_spacing != m_spacing) {
        m_spacing = m_original_spacing;
    } else {
        m_spacing += 1;
    }
    emit sizeHintChanged(index);
}

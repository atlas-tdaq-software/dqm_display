/*
 * DQDataModel.cpp
 *
 * Provides a simple tree model to show how to create and use DQM data model.
 */
#include <map>

#include <dqm_display/gui/DQDataModel.h>
#include <dqm_display/gui/LogWidget.h>
#include <dqm_display/gui/Status.h>

#include <QIcon>
#include <QVariant>
#include <QDebug>

using namespace dqm_display::gui;

DQDataModel::DQDataModel(const std::string & partition_name,
    const std::string & connect_string)
    : m_partition_name(partition_name),
      m_connect_string(connect_string),
      m_lastUpdatedNode(0),
      m_updates(0),
      m_last_updates(0),
      m_thread(1)
{
    m_parameterFont.setItalic(true);
    m_parameterFont.setPointSize(12);
    m_regionFont.setPointSize(12);

    m_timer = this->startTimer(1000);
}

void DQDataModel::clear()
{
    beginResetModel();
    m_data.reset(0);
    m_lastUpdatedNode = 0;
    endResetModel();
}

void DQDataModel::loadDataThread(bool & done)
{
    core::TreeDataModel * m = 0;

    try {
        m = new core::TreeDataModel(
                m_partition_name, m_connect_string, *this,
                std::bind(&DQDataModel::configurationReloaded, this));
    }
    catch (ers::Issue & ex) {
        ers::fatal(ex);
    }

    m_data.reset(m);
    done = true;
}

bool
DQDataModel::loadData()
{
    killTimer(m_timer);

    // process all pending events so there will be no surprises
    // when we destroy the data model
    QCoreApplication::processEvents();

    beginResetModel();

    bool done = false;
    m_thread.addJob(std::bind(&DQDataModel::loadDataThread, this, std::ref(done)));

    while (!done)
    {
	QCoreApplication::processEvents(
	        QEventLoop::ExcludeUserInputEvents | QEventLoop::ExcludeSocketNotifiers, 10);
    }

    endResetModel();

    m_timer = this->startTimer(1000);

    return m_data;
}

void DQDataModel::subscribeForUpdates() {
    if (m_data) {
        m_data->input().start();
    }
}

void
DQDataModel::nodeStatusUpdated(const core::TreeNode & node, bool /*status_changed*/,
    bool status_degraded)
{
    if (!m_data)
	return;

    m_lastUpdatedNode = &node;
    ++m_updates;

    if (status_degraded)
    {
	emit alarmProduced(&node);
    }

    emit resultProduced(&node);
}

void
DQDataModel::timerEvent(QTimerEvent *)
{
    if (m_lastUpdatedNode)
    {
	const QString time = m_lastUpdatedNode->getResult()->timeString();
	emit statsUpdated(time, m_updates, m_updates - m_last_updates);
	m_last_updates = m_updates;
    }
}

int DQDataModel::columnCount(const QModelIndex &) const
{
    return 5;
}

QVariant
DQDataModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
	return QVariant();

    const core::TreeNode * const node = getNode(index);

    switch (role)
    {
	case Qt::DecorationRole:
	    return Status::icon(node->getResult()->status());

	case Qt::DisplayRole:
	{
	    switch (index.column())
	    {
		case 0:
		    return node->getName();

		case 1:
		    return node->makeResultText();

		case 2:
		    return node->makeConfigText();

		case 3:
		    return node->getDescription();

		case 4:
		    return node->getTroubleshooting();

		default:
		    break;
	    }
	    break;
	}

	case Qt::FontRole:
	    return node->isLeaf() ? m_parameterFont : m_regionFont;

	case Qt::ToolTipRole:
	    return node->getToolTip();

	case Qt::UserRole:
	    return QVariant::fromValue((void*) node);

	default:
	    break;
    }

    return QVariant();
}

Qt::ItemFlags
DQDataModel::flags(const QModelIndex &) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QModelIndex
DQDataModel::rootIndex() const
{
    return (m_data ? index(m_data->getRootRegion()) : QModelIndex());
}

QModelIndex
DQDataModel::index(int row, int column, const QModelIndex & parent) const
{
    if (!m_data)
	return QModelIndex();

    const core::TreeNode * node =
	parent.isValid() ? getNode(parent)->getChild(row) : m_data->getRootRegion();
    return createIndex(row, column, (void*) node);
}

QModelIndex
DQDataModel::parent(const QModelIndex & ii) const
{
    if (!ii.isValid() || !m_data)
	return QModelIndex();

    const core::TreeNode * node = getNode(ii);
    if (node == m_data->getRootRegion())
	return QModelIndex();

    return index(node->getParent());
}

int
DQDataModel::rowCount(const QModelIndex & parent) const
{
    if (!parent.isValid())
	return 1;
    else
	return getNode(parent)->childCount();
}

QModelIndex
DQDataModel::index(const core::TreeNode * node) const
{
    if (!m_data)
	return QModelIndex();

    return createIndex(node == m_data->getRootRegion() ? 0 : node->getPosition(), 0,
	(void*) node);
}

QVariant
DQDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    static const QVariant headers[] = {
            QVariant("Histogram"), QVariant("Result"), QVariant("Configuration"),
                    QVariant("Description"), QVariant("Troubleshooting") };
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole
	&& section < 5) {
	return headers[section];
    }

    return QVariant();
}

/*
 * ParameterFilterModel.cpp
 *
 *  Created on: Oct 9, 2013
 *      Author: kolos
 */

#include <QDebug>
#include <QTreeView>

#include <dqm_display/gui/ParameterFilterModel.h>

using namespace dqm_display::gui;

ParameterFilterModel::ParameterFilterModel(QObject * parent, QAbstractItemModel* source)
  : QAbstractProxyModel(parent),
    m_row(-1)
{
    setSourceModel(source);
}

void
ParameterFilterModel::setVisibleItem(const QModelIndex & index)
{
    beginResetModel();
    m_parent = index.parent();
    m_row = index.row();
    qDebug() << "setting " << m_row;
    endResetModel();
}



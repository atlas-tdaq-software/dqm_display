/*! \file DetailsViewDelegate.cpp Implements the DetailsViewDelegate class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_display/gui/DetailsViewDelegate.h>
#include <dqm_display/gui/Status.h>

#include <dqm_display/core/Parameter.h>

#include <QApplication>
#include <QBitmap>
#include <QDebug>
#include <QPainter>
#include <QTextBlock>
#include <QTextDocument>
#include <QTextLayout>
#include <QTextLine>
#include <QTextOption>

using namespace dqm_display::gui;

DetailsViewDelegate::DetailsViewDelegate(int row_height, int font_size, int spacing,
        QAbstractItemModel *model, QObject *parent)
  : IconsViewDelegate(QSize(row_height, row_height), spacing, parent, false),
    m_model(model),
    m_font_size(font_size)
{
    connect(parent, SIGNAL(fontSizeChanged(int)),
        this, SLOT(when_fontSizeChanged(int)));
}

void DetailsViewDelegate::when_fontSizeChanged(int size) {
    m_font_size = size;
    emit m_model->dataChanged(m_model->index(0,0),
            m_model->index(m_model->columnCount(), m_model->rowCount()));
}

void 
DetailsViewDelegate::paint(QPainter * painter,
	const QStyleOptionViewItem & option, const QModelIndex & index) const
{
    if (index.column() == 0 && option.rect.width() != option.rect.height()) {
        const_cast<DetailsViewDelegate*>(this)->when_viewHeightChanged(
                option.rect.width(), m_model->index(0,0));
        return;
    }

    painter->save();
    painter->save();

    if(option.state & QStyle::State_Selected) {
        painter->setPen(Qt::NoPen);
        painter->setBrush(QBrush(m_selection_color));
        if (index.column() == 0) {
            // first columns
            painter->drawRoundedRect(option.rect.adjusted(1, 1, 0, -1),
                    m_radius, m_radius, Qt::RelativeSize);
            painter->drawRect(option.rect.adjusted(2 * m_radius, 1, 0, -1));
        }
        else if (index.column() == (index.model()->columnCount() - 1)) {
            // last column
            painter->drawRoundedRect(option.rect.adjusted(0, 1, -1, -1),
                    m_radius, m_radius, Qt::RelativeSize);
            painter->drawRect(option.rect.adjusted(0, 1, -2*m_radius, -1));
        }
        else {
            painter->drawRect(option.rect.adjusted(0, 1, 0, -1));
        }
    }
    else if (index.row() % 2) {
        painter->fillRect(option.rect, QBrush(QColor(250, 250, 250)));
    }
    painter->restore();

    IconsViewDelegate::doPaint(painter, option, index, 0);

    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setRenderHint(QPainter::TextAntialiasing, true);

    if (index.column() != 0) {
	QSize size(option.rect.size() - QSize(2 * m_margin, 2 * m_spacing));
	QTextDocument doc;
	doc.setDefaultStyleSheet(QString("p {font-size: %1px;}").arg(m_font_size));

	doc.setHtml("<p>" + qvariant_cast<QString>(index.data(Qt::DisplayRole)) + "</p>");

	int X = m_margin;
	int Y = m_spacing;
        for ( QTextBlock b = doc.begin(); b != doc.end(); b = b.next() )
        {        
	    QTextOption op;
	    op.setWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);

            QTextLayout * layout = b.layout();            
            layout->setTextOption(op);
	    layout->beginLayout();
	    while (1) {
		QTextLine line = layout->createLine();

		if (!line.isValid())
		    break;

		line.setLineWidth(size.width());

		int nextY = Y + layout->boundingRect().height();
		if (size.height() >= nextY) {
		    line.draw(painter, QPoint(X, Y));
                    Y = nextY;
		}
		else {
		    QString lastLine("...");
		    painter->drawText(X, Y, lastLine);
		    break;
		}
	    }
	    layout->endLayout();
	}
    }

    painter->restore();
}

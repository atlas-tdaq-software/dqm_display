/*! \file GraphicsRegion.cpp Implements the GraphicsRegion class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_config/dal/DQShape.h>

#include <dqm_display/gui/GraphicsRegion.h>
#include <dqm_display/gui/GraphicsRingLayout.h>

#include <QDebug>

using namespace dqm_display::gui;

GraphicsRegion::GraphicsRegion(
    const core::Region & region,
    TreeNodeMenu & menu,
    const dqm_config::dal::DQShape * default_shape,
    bool top_level)
  : GraphicsItem(region, menu, default_shape)
{
    try
    {
	const dqm_config::dal::DQLayout * layout = region.getLayout();
	const dqm_config::dal::DQShape * children_shape =
	    layout ? layout->get_DefaultChildrenShape() : 0;
	m_layout.reset(createLayout(layout, region.getName()));

	if (top_level)
	{
	    m_transparency = true;
	}
	else if (!layout || !m_transparency)
	{
	    m_show_label = true;
	    return;
	}

	const std::vector<core::Region*> & rr = region.getSubRegions();

	for (uint i = 0; i < rr.size(); ++i)
	{
	    GraphicsRegion * item = new GraphicsRegion(*rr[i], menu,
		children_shape);
	    if (!m_layout->insertItem(item, i))
		delete item;
	}

	const std::vector<core::Parameter*> & pp = region.getParameters();
	for (uint i = 0; i < pp.size(); ++i)
	{
	    GraphicsItem * item = new GraphicsItem(*pp[i], menu, children_shape);
	    if (!m_layout->insertItem(item, rr.size() + i))
		delete item;
	}
    }
    catch (daq::config::Generic & ex)
    {
	ers::error(ex);
    }
}

GraphicsGridLayout *
GraphicsRegion::createLayout(const dqm_config::dal::DQLayout * layout,
    const QString & name)
{
    return !layout || layout->get_Type() == dqm_config::dal::DQLayout::Type::Grid
	? new GraphicsGridLayout(this, layout, name)
	: new GraphicsRingLayout(this, layout, name);
}

QSizeF
GraphicsRegion::sizeHint(Qt::SizeHint which, const QSizeF & constraint) const
{
    if (which == Qt::PreferredSize)
    {
	if (m_transparency)
	{
	    m_size = m_layout->preferredSize();
	    if (m_size.width() < m_size.height() * m_aspect_ratio)
	    {
		m_size.setWidth(m_size.height() * m_aspect_ratio);
	    }
	    else if (m_size.height() < m_size.width() / m_aspect_ratio)
	    {
		m_size.setHeight(m_size.width() / m_aspect_ratio);
	    }
	}
	return m_size;
    }
    return constraint;
}

void
GraphicsRegion::setGeometry(const QRectF & rect)
{
    GraphicsItem::setGeometry(rect);
    m_layout->setGeometry(QRectF(QPointF(0, 0), rect.size()));
}

void
GraphicsRegion::setLayoutHint(double angle, double radius, double offset)
{
    GraphicsItem::setLayoutHint(angle, radius, offset);
    m_layout->setAngle(angle);
}

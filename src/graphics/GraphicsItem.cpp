/*! \file GraphicsItem.cpp Implements the GraphicsItem class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <map>

#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQShape.h>

#include <dqm_display/gui/GraphicsItem.h>
#include <dqm_display/gui/Status.h>

#include <QFontMetrics>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QPen>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QDebug>

using namespace dqm_display::gui;

GraphicsItem::Shape
shapeFromDB(const dqm_config::dal::DQShape * dqShape)
{
    static std::map<std::string, GraphicsItem::Shape> shapes = {
	{"Ring", GraphicsItem::Shape::Ring},
	{"Trapezoid", GraphicsItem::Shape::Trapezoid},
	{"Filler", GraphicsItem::Shape::Filler}
    };
  
    if (!dqShape)
	return GraphicsItem::Shape::Filler;

    return shapes[dqShape->get_Type()];
}

////////////////////////////////////////////////////////////////////////////

GraphicsItem::GraphicsItem(
	const core::TreeNode & node,
        TreeNodeMenu & menu,
        const dqm_config::dal::DQShape * default_shape)
  : m_node(&node),
    m_text(node.getLabel()),
    m_menu(&menu),
    m_size(500., 500.),
    m_shape(Shape::Filler),
    m_aspect_ratio(1.),
    m_length_ratio(1.),
    m_rotation(0.),
    m_layout_azimuth(360.),
    m_layout_radius(0.),
    m_layout_offset(0.),
    m_transparency(false),
    m_show_label(true),
    m_circular_layout(false),
    m_row(0),
    m_column(0),
    m_rspan(1),
    m_cspan(1),
    m_draw_shape(true),
    m_mouse_in(false),
    m_selected(false)
{
    setOwnedByLayout(true);
    setAcceptHoverEvents(true);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    try {
	const dqm_config::dal::DQShape * shape = node.getShape()
						    ? node.getShape()
						    : default_shape;
	if (shape) {
	    m_shape = shapeFromDB(shape);
	    m_aspect_ratio = shape->get_AspectRatio();
	    m_length_ratio = shape->get_LengthRatio();
	    m_transparency = shape->get_Transparency();
	    m_show_label = !m_transparency;
	    m_rotation = shape->get_Rotation();
	}
    }
    catch(daq::config::Generic & ex) {
	ers::error(ex);
    }
    
    if (m_size.width() > m_size.height()*m_aspect_ratio) {
	m_size.setWidth(m_size.height()*m_aspect_ratio);
    }
    else if (m_size.height() > m_size.width()/m_aspect_ratio) {
	m_size.setHeight(m_size.width()/m_aspect_ratio);
    }

    setToolTip(m_node->getToolTip());

    setGraphicsItem(this);
}

GraphicsItem::GraphicsItem(const char * text, int column, int row, int cspan, int rspan)
  : m_node(0),
    m_text(QString(text).replace("\\n", "\n")),
    m_menu(0),
    m_size(500., 500.),
    m_shape(Shape::Filler),
    m_aspect_ratio(1.),
    m_length_ratio(1.),
    m_rotation(0.),
    m_layout_azimuth(360.),
    m_layout_radius(0.),
    m_layout_offset(0.),
    m_transparency(false),
    m_show_label(true),
    m_circular_layout(false),
    m_row(row),
    m_column(column),
    m_rspan(rspan),
    m_cspan(cspan),
    m_draw_shape(false),
    m_mouse_in(false),
    m_selected(false)
{
    setOwnedByLayout(true);
    setAcceptHoverEvents(false);
    setAcceptedMouseButtons(Qt::NoButton);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    setBrush(QColor(255, 255, 255, 0));
    setPen(Qt::SolidLine);
    setGraphicsItem(this);
}

void
GraphicsItem::hoverEnterEvent( QGraphicsSceneHoverEvent * )
{
    if (!m_node)
	return;

    m_mouse_in = true;
    update();
}

void 
GraphicsItem::hoverLeaveEvent( QGraphicsSceneHoverEvent * )
{
    if (!m_node)
	return;

    m_mouse_in = false;
    update();
}

void 
GraphicsItem::updateStatusColor()
{
    if (!m_node)
	return;

    setToolTip(m_node->getToolTip());

    update();
}

void
GraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
    if (!m_node)
	return;

    if (event->button() == Qt::LeftButton) {
        m_selected = true;
	emit nodeActivated(m_node);
    }
    else if (event->button() == Qt::RightButton) {
	m_menu->setNode(m_node);
	disconnect(m_menu, SIGNAL(nodeActivated(const core::TreeNode *)), 0, 0);
	connect(m_menu, SIGNAL(nodeActivated(const core::TreeNode *)),
	    this, SIGNAL(nodeActivated(const core::TreeNode *)));
	m_menu->move(event->screenPos());
	m_menu->show();
    }
}

void 
GraphicsItem::setLayoutHint(double angle, double radius, double offset)
{
    m_layout_azimuth = angle;
    m_layout_radius = radius;
    m_layout_offset = offset;
    m_circular_layout = true;
}

void 
GraphicsItem::setGeometry(const QRectF & rect)
{
    if (rect.isEmpty())
    	return;

    m_size = rect.size();

    const double height = m_size.height();
    const double width  = m_size.width();

    QPainterPath path;

    switch (m_shape)
    {
      case Shape::Ring:
      {
	const double dx = width*(1.-m_length_ratio);
	const double dy = height*(1.-m_length_ratio);
	path.moveTo((width + dx)/2., height/2.);
	QRectF inner((width - dx)/2., (height-dy)/2., dx, dy);
	path.arcTo(inner, 0, 360);
	path.moveTo(width, height/2.);
	QRectF outer(0, 0, width, height);
	path.arcTo(outer, 0, 360);
	path.closeSubpath();

	const double margin_x = width/5.;
	const double margin_y = height/5.;
	m_label_rect = QRectF(margin_x, margin_y, width-2.*margin_x, height-2.*margin_y);
	break;
      }
      
      case Shape::Trapezoid:
      {
	const double dw = width*(1.-m_length_ratio)/2.;

	QRectF r(QRectF(QPointF(0.,0.), m_size));
	path.moveTo(r.left(), r.top());
	path.lineTo(r.left() + r.width(), r.top());
	path.lineTo(r.left() + r.width() - dw, r.top() + r.height());
	path.lineTo(r.left() + dw, r.top() + r.height());
	path.closeSubpath();

	const double margin_y = height/20.;
        const double margin_x = dw ? dw : width/20.;
	m_label_rect = QRectF(margin_x, margin_y, width-2.*margin_x, height-2.*margin_y);
        break;
      }

      case Shape::Filler:
      {
	if (m_circular_layout)
	{
	    // This is a special case - this code fills cell in a Circular layout
	    // m_layout_offset, beta and gamma are used to account for horizontal spacing
	    const double R = m_layout_radius + m_layout_offset;
	    const double ALPHA = m_layout_azimuth/2.;
	    const double alpha = ALPHA/180.*M_PI;

	    const double beta = alpha - asin(sin(M_PI-alpha)/R*m_layout_offset);
	    const double gamma = alpha - asin(sin(M_PI-alpha)/(R-height)*m_layout_offset);
	    const double BETA = beta*180./M_PI;
	    const double GAMMA = gamma*180./M_PI;

	    const double rr = sqrt(R*R + m_layout_offset*m_layout_offset - 2*R*m_layout_offset*cos(beta));

	    const double dx = width/2. - rr * sin(alpha);
	    const double dy = R*(1.-cos(beta));

	    path.moveTo(width - dx, dy);
	    QRectF bounding_rect(-R + width/2., 0, 2.*R, 2.*R);
	    path.arcTo(bounding_rect, 90-BETA, 2.*BETA);

	    if ( m_layout_azimuth == 360. )
		path.moveTo(dx + height*sin(alpha), dy + height*cos(alpha));
	    else
		path.lineTo(dx + height*sin(alpha), dy + height*cos(alpha));

	    bounding_rect.adjust(height, height, -height, -height);
	    path.arcTo(bounding_rect, 90 + GAMMA, -2.*GAMMA);
	    path.closeSubpath();

	    // Calculate label rectangle
	    if (alpha == M_PI && m_layout_radius == height) {
		m_label_rect = QRectF(height/3., height/3., 2*(height - height/3.), 2*(height - height/3.));
	    }
	    else {
		const double __beta__ = alpha >= M_PI/4.
		    ? M_PI/8.
		    : m_layout_radius == height ? beta/3. : beta/1.5;
		const double ww = R*sin(__beta__);
		const double margin_x = width/2. - ww;
		const double margin_y = R - R*cos(__beta__);
		const double hh = height != m_layout_radius
				? height - margin_y
				: height - margin_y - ww/tan(alpha);
		m_label_rect = QRectF(margin_x + ww/20., margin_y+hh/20., ww*2.-ww/10., hh-hh/10.);
	    }
	}
	else
	{
	    path.addRect(QRectF(QPointF(0.,0.), m_size));
	    const double margin_x = width/20.;
	    const double margin_y = height/20.;
	    m_label_rect = QRectF(margin_x, margin_y, width-2.*margin_x, height-2.*margin_y);
	}
	break;
      }

      default:
      	break;
    }
        
    if (m_rotation != 0)
    {
	setTransformOriginPoint(QPointF(width/2., height/2.));
	setRotation(m_rotation);
    }

    prepareGeometryChange();
    
    setPath(path);
            
    QPointF left_top = QPointF( rect.left() + (rect.width() - m_size.width())/2.,
				rect.top() + (rect.height() - m_size.height())/2.);
    setPos(left_top);
}
 
QSizeF 
GraphicsItem::sizeHint(Qt::SizeHint which, const QSizeF & constraint) const
{
    return (which == Qt::PreferredSize ? m_size : constraint);
}

void 
GraphicsItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    const QTransform & t(painter->deviceTransform());
    qreal scale = sqrt(t.m11()*t.m11()+t.m12()*t.m12());

    if (m_node) {
        QColor c = Status::color(m_node->getStatus());
        if (m_selected || m_mouse_in) {
            c = c.lighter(150);
        }
        if (m_transparency)
            c.setAlpha(64);
        setBrush(c);
        if (m_selected) {
            int width = 3./scale < 1 ? 1 : (3./scale + 0.5);
            setPen(QPen(QBrush(QColor(64, 64, 64)), width, Qt::DotLine));
        } else {
            int width = 1./scale;
            setPen(QPen(Qt::lightGray, width));
        }
    }

    if (m_draw_shape) {
	QGraphicsPathItem::paint(painter, option, widget);
    }

    if (!m_show_label) {
	return;
    }

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing);
    painter->setRenderHint(QPainter::TextAntialiasing);
    
    QFont font(m_font);

    QRectF rect;
    int fontSize = 16;
    for ( ; fontSize > 5; --fontSize)
    {
        int fs = fontSize/scale;
        if (fs < 2) {
            fs = 2;
        }
	font.setPointSize(fs);
	painter->setFont(font);
	rect = labelRect(painter);
	if (!rect.isEmpty()) {
	    break;
	}
    }

    if (rect.isEmpty()) {
	painter->restore();
	return;
    }

    if (m_rotation > 90 && m_rotation < 270) {
	// avoid having text upside down
	painter->translate(rect.left() + rect.width(), rect.top() + rect.height());
	painter->rotate(180);
	rect = QRectF(0., 0., rect.width(), rect.height());
    }

    if (m_selected) {
        qreal shift = 1./scale;
        painter->setPen(Qt::black);
        painter->drawText(rect.adjusted(shift, shift, shift, shift), Qt::AlignCenter, m_text);
        painter->setPen(Qt::white);
        painter->drawText(rect.adjusted(-shift, -shift, -shift, -shift), Qt::AlignCenter, m_text);
    }
    painter->setPen(QColor(64, 64, 64));
    painter->drawText(rect, Qt::AlignCenter, m_text);

    painter->restore();
}

QRectF
GraphicsItem::labelRect(QPainter * painter)
{
    QRectF rect = painter->boundingRect(m_label_rect, Qt::AlignCenter, m_text);

    if (m_label_rect.width() < rect.width()) {
        if (m_label_rect.width() > rect.height() && m_label_rect.height() > rect.width()) {
            painter->translate(m_label_rect.left(), m_label_rect.top() + m_label_rect.height());
            painter->rotate(-90);
            return QRectF(0., 0., m_label_rect.height(), m_label_rect.width());
        } else {
            return QRectF();
        }
    } else if (m_label_rect.height() < rect.height()) {
        return QRectF();
    } else {
        return m_label_rect;
    }
}

/*! \file GraphicsView.cpp Implements the GraphicsView class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_config/dal/DQShape.h>

#include <dqm_display/gui/GraphicsView.h>

#include <QGraphicsScene>
#include <QGraphicsWidget>
#include <QResizeEvent>
#include <QDebug>

using namespace dqm_display::gui;

GraphicsView::GraphicsView(QWidget *parent)
  : QGraphicsView(parent),
    m_scene_set(false),
    m_scaled(false),
    m_layout(0),
    m_selected(0),
    m_node_to_select(0),
    m_menu(this)
{
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setScene(new QGraphicsScene(this));
}

void 
GraphicsView::clear()
{
    scene()->clear();
    m_scene_set = false;
    m_region.reset(0);
    m_layout = 0;
    m_selected = 0;
    m_node_to_select = 0;
}

void GraphicsView::fitSceneToView() {
    if (not m_scene_set) {
        return;
    }

    auto r = scene()->sceneRect();
    int marginX = r.width() / 100;
    int marginY = r.height() / 100;
    fitInView(r.adjusted(-marginX, -marginY, marginX, marginY), Qt::KeepAspectRatio);
    m_scaled = false;
}

void
GraphicsView::setNode(const core::TreeNode * node)
{
    m_node_to_select = node;

    if (isVisible()) {
        selectNode();
    }
}

void
GraphicsView::setupScene()
{
    if (m_scene_set || !m_region)
	return;

    m_selected = 0;

    QGraphicsScene * scene = this->scene();
    scene->clear();

    QGraphicsWidget* gwidget = new QGraphicsWidget();
    m_layout = m_region->takeLayout();
    gwidget->setLayout(m_layout);
    scene->addItem(gwidget);

    QList<QGraphicsItem *> it = scene->items();
    for ( int i = 0; i < it.size(); ++i )
    {
	GraphicsItem * item = dynamic_cast<GraphicsItem*>(it[i]);
	if (not item) {
	    continue;
	}
	if (m_node_to_select && m_node_to_select == item->node()) {
	    item->setSelected(true);
	    m_selected = item;
	    m_node_to_select = 0;
	}
	QObject::connect(item, SIGNAL(nodeActivated(const core::TreeNode*)),
		     this, SIGNAL(nodeActivated(const core::TreeNode*)));
    }

    m_scene_set = true;

    scene->setSceneRect(QRectF(QPoint(0.,0.), m_layout->preferredSize()));

    fitSceneToView();
}

void GraphicsView::selectNode() {
    if (not isVisible() || !m_node_to_select) {
        return;
    }

    if (m_selected) {
       m_selected->setSelected(false);
       m_selected = 0;
    }

    bool setupRequired = not m_region || m_region->node() != m_node_to_select;

    if (m_node_to_select->isLeaf()) {
        QList<QGraphicsItem *> it = scene()->items();
        for (int i = 0; i < it.size(); ++i) {
            GraphicsItem * item = dynamic_cast<GraphicsItem*>(it[i]);
            if (item && item->node() == m_node_to_select) {
                setupRequired = false;
                item->setSelected(true);
                m_selected = item;
                break;
            }
        }
    }

    if (setupRequired) {
        const core::TreeNode * scene_node = m_node_to_select->isLeaf()
                ? m_node_to_select->getParent() : m_node_to_select;
        const core::Region * r = static_cast<const core::Region*>(scene_node);
        m_region.reset(new GraphicsRegion(*r, m_menu, 0, true));

        m_scene_set = false;
        setupScene();
    }
    m_node_to_select = 0;
}

void
GraphicsView::updateNode(const core::TreeNode * node)
{
    if (!isVisible() || !m_scene_set)
	return;

    QList<QGraphicsItem *> it = scene()->items();
    for ( int i = 0; i < it.size(); ++i )
    {
	GraphicsItem * item = dynamic_cast<GraphicsItem*>(it[i]);
	if (item && item->node() == node) {
	    item->updateStatusColor();
	    break;
	}
    }
}

void
GraphicsView::scale(double q)
{
    QGraphicsView::scale(q, q);
    m_scaled = true;
}

void
GraphicsView::resizeEvent(QResizeEvent * )
{    
    if (m_scene_set && not m_scaled) {
	fitSceneToView();
    }
}

void
GraphicsView::showEvent(QShowEvent * )
{
    selectNode();
}

bool
GraphicsView::viewportEvent(QEvent * event)
{
    if (event->type() == QEvent::WindowActivate || event->type() == QEvent::WindowDeactivate)
	return true;
    else
	return QGraphicsView::viewportEvent(event);
}

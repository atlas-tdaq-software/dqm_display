/*! \file GraphicsRingLayout.cpp Implements the GraphicsRingLayout class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_config/dal/DQLayout.h>

#include <dqm_display/gui/GraphicsRingLayout.h>
#include <dqm_display/gui/GraphicsItem.h>

#include <QDebug>

using namespace dqm_display::gui;

GraphicsRingLayout::GraphicsRingLayout(GraphicsRegion * parent,
        const dqm_config::dal::DQLayout * layout, const QString & name)
  : GraphicsGridLayout(parent, layout, name),
    m_sector_angle(360.),
    m_total_v_spacing(0.),
    m_rotation(0.)
{
    for (size_t i = 0; i < m_row_spacing.size(); ++i )
	m_total_v_spacing += m_row_spacing[i];

    try {
    	m_rotation = layout->get_Rotation();
    }
    catch(ers::Issue & ex) {
	ers::error(ex);
    }
}

void
GraphicsRingLayout::setAngle(double angle)
{
    m_sector_angle = angle;
}

void
GraphicsRingLayout::updateSize() const
{
    GraphicsGridLayout::updateSize();

    int col_header = !m_column_headers.empty();
    qreal d = 2*m_cell_size.height()*(m_rows + col_header + m_total_v_spacing + m_v_margins);
    m_size = QSizeF(d, d);
    m_bounding_rect = QRectF(QPointF(0,0), m_size);
}

void
GraphicsRingLayout::setGeometry(const QRectF & rect)
{    
    if (m_rows*m_columns == 0)
	return;

    if (rect.isEmpty())
	return;

    int rows = m_rows + !m_column_headers.empty();
    int columns = m_columns + !m_row_headers.empty();

    const double min = std::min(rect.size().width(), rect.size().height());
    const double width = m_sector_angle < 180 ? rect.size().width()/2. : min/2.;
    const double height = m_sector_angle < 180 ? rect.size().height()/2. : min/2.;

    const double shift_y = m_h_spacing * width/columns*4.;
    const double r0 = m_sector_angle < 180 ? width/tan(m_sector_angle/360.*M_PI) : height - shift_y;
    const double dr = m_sector_angle < 180 ? 2.*(height-shift_y)/(rows + m_total_v_spacing)
					   :    (height-shift_y)/(rows + m_total_v_spacing);

    const double alpha = m_sector_angle/columns;
    const double a0 = m_sector_angle < 180 ? -alpha/2*(columns-1) : alpha/2 + m_rotation;

    const double dX = rect.left() + rect.size().width()/2. - width;
    const double dY = m_sector_angle < 180 ? rect.top() + rect.size().height()/2.
					   : rect.top() + rect.size().height()/2. - height;

    m_bounding_rect = QRectF();
    for ( int n = 0; n < count(); ++n )
    {
	GraphicsItem* item = static_cast<GraphicsItem*>(itemAt(n));
	int row,col,rspan,cspan;
	item->getLayoutParameters(row, col, rspan, cspan);

	qreal row_spacing = 0.;
	for (int i = 0; i < row; ++i)
	    row_spacing += m_row_spacing[i];

	qreal rspan_spacing = 0.;
	for (int i = row; i < row+rspan-1; ++i)
	    if (i < (int)m_row_spacing.size())
		rspan_spacing += m_row_spacing[i];

	const double radius = r0 - dr*(row + row_spacing + m_column_shifts[col]);
	const double span_alpha = alpha*cspan;
	const double tan_alpha = span_alpha < 180 ? tan(span_alpha*M_PI/360.) : 1;
	const double left = width - radius*tan_alpha + dX;
 	const double top = height - radius + dY;
 	const double wi = 2.*radius*tan_alpha;
 	const double hi = dr*(rspan + rspan_spacing);

 	item->setLayoutHint(span_alpha, radius, shift_y);
	item->setGeometry(QRectF(left, top - shift_y, wi, hi));
	item->setTransformOriginPoint(QPointF(wi/2., radius + shift_y));
	item->setRotation(a0 + alpha*m_row_shifts[row] + alpha*col + (span_alpha - alpha)/2);

	m_bounding_rect |= item->sceneBoundingRect();
    }
}

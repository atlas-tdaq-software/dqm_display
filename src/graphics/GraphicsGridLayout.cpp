/*! \file GraphicsGridLayout.cpp Implements the GraphicsGridLayout class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_config/dal/DQLayout.h>

#include <dqm_display/gui/GraphicsGridLayout.h>
#include <dqm_display/gui/GraphicsItem.h>
#include <dqm_display/gui/GraphicsRegion.h>

#include <QDebug>

using namespace dqm_display::gui;

GraphicsGridLayout::GraphicsGridLayout(GraphicsRegion * parent,
        const dqm_config::dal::DQLayout * layout, const QString & name)
  : m_parent(parent),
    m_name(name),
    m_size(500, 500),
    m_rows(1),
    m_columns(1),
    m_children_grid("1"),
    m_h_spacing(0),
    m_v_spacing(0),
    m_h_margins(0),
    m_v_margins(0),
    m_row_spacing(1, 0.),
    m_column_spacing(1, 0.),
    m_row_shifts(1, 0.),
    m_column_shifts(1, 0.),
    m_children_span(1, std::make_pair(1U,1U)),
    m_layout_horizontal(true)
{  
    setOwnedByLayout(false);
    
    if (!layout) {
        uint cn = parent->node()->childCount();
	if (cn) {
            m_rows = m_columns = sqrt(cn);
            if (m_columns * m_rows < cn) {
                m_columns++;
                if (m_columns * m_rows < cn) {
                   m_rows++;
                }
            }
	}
    }

    typedef std::map<
            std::pair<std::pair<unsigned int, unsigned int>,
            std::pair<unsigned int, unsigned int>>,
        std::string> Labels;
    Labels labels;

    try {
        std::map<unsigned int, double> row_s;
        std::map<unsigned int, double> col_s;
        std::map<unsigned int, double> row_sh;
        std::map<unsigned int, double> col_sh;
        std::map<unsigned int, std::pair<unsigned int, unsigned int>> span;

        if (layout) {
            m_rows    = layout->get_Rows();
            m_columns = layout->get_Columns();
            m_children_grid = layout->get_children_grid();
            m_v_spacing = layout->get_VSpacing();
            m_h_spacing = layout->get_HSpacing();
            m_v_margins = layout->get_VMargins();
            m_h_margins = layout->get_HMargins();
            m_row_headers = layout->get_row_headers();
            m_column_headers = layout->get_column_headers();
            m_layout_horizontal = layout->get_Orientation() == "Horizontal";

            row_s = layout->get_row_spacing();
            col_s = layout->get_column_spacing();
            row_sh = layout->get_row_shifts();
            col_sh = layout->get_column_shifts();
            span = layout->get_children_span();
            labels = layout->get_labels();
        }

	m_row_spacing = Spacing(m_rows - m_column_headers.empty(), m_v_spacing);
	for (std::map<unsigned int, double>::iterator it = row_s.begin(); it != row_s.end(); ++it )
	{
	    if (it->first < m_row_spacing.size())
		m_row_spacing[it->first] = it->second;
	}

	m_column_spacing = Spacing(m_columns - m_row_headers.empty(), m_h_spacing);
	for (auto it = col_s.begin(); it != col_s.end(); ++it )
	{
	    if (it->first < m_column_spacing.size())
		m_column_spacing[it->first] = it->second;
	}

	m_row_shifts = Spacing(m_rows + !m_column_headers.empty(), 0);
	for (auto it = row_sh.begin(); it != row_sh.end(); ++it )
	{
	    if (it->first < m_row_shifts.size())
		m_row_shifts[it->first] = it->second;
	}

	m_column_shifts = Spacing(m_columns + !m_row_headers.empty(), 0);
	for (auto it = col_sh.begin(); it != col_sh.end(); ++it )
	{
	    if (it->first < m_column_shifts.size())
		m_column_shifts[it->first] = it->second;
	}

	m_children_span.resize(m_columns*m_rows, std::make_pair(1U,1U));
	for (auto it = span.begin(); it != span.end(); ++it )
	{
	    if (it->first < m_children_span.size())
		m_children_span[it->first] = it->second;
	}
    }
    catch(ers::Issue & ex) {
	ers::error(ex);
    }

    if (!m_layout_horizontal) {
	while (m_children_grid.size() < m_rows*m_columns)
	    m_children_grid += m_children_grid;

	Mask tmp(m_children_grid);
	for (size_t i = 0; i < m_children_grid.size(); ++i)
	    tmp[i] = m_children_grid[(i%m_columns)*m_columns + i/m_columns];

	m_children_grid = tmp;
    }

    int shift = !m_column_headers.empty();
    for (Headers::const_iterator it = m_row_headers.begin();
            it != m_row_headers.end(); ++it) {
	GraphicsItem * item = new GraphicsItem(it->second.c_str(), 0,
	        it->first.first + shift, 1, it->first.second);
	addItem(item, it->first.first + shift, 0);
	item->setParentItem(m_parent);
    }

    shift = !m_row_headers.empty();
    for (Headers::const_iterator it = m_column_headers.begin();
            it != m_column_headers.end(); ++it) {
	GraphicsItem * item = new GraphicsItem(it->second.c_str(),
	        it->first.first + shift, 0, it->first.second, 0);
	addItem(item, 0, it->first.first + shift);
	item->setParentItem(m_parent);
    }

    for (Labels::const_iterator it = labels.begin(); it != labels.end(); ++it)
    {
        GraphicsItem * item = new GraphicsItem(it->second.c_str(),
            it->first.first.first, it->first.first.second,
            it->first.second.first, it->first.second.second);
        insertItem(item);
    }
}

bool
GraphicsGridLayout::calculateItemPosition(
    uint n, int & row, int & column, int & row_span, int & column_span) const
{
    if (m_children_grid.empty() || m_rows*m_columns <= n)
	return false;

    uint pos = 0;
    for (uint i = 0; i <= n; i += (m_children_grid[pos++ % m_children_grid.size()] != '0'))
	;

    if (m_rows*m_columns >= pos)
    {
	if (m_layout_horizontal) {
	    row = (pos-1)/m_columns;
	    column = (pos-1)%m_columns;
	}
	else {
	    column = (pos-1)/m_columns;
	    row = (pos-1)%m_columns;
	}
	column_span = m_children_span[n].first;
	row_span = m_children_span[n].second;
	return true;
    }

    return false;
}

bool
GraphicsGridLayout::insertItem(GraphicsItem * item, int n)
{
    int r,c,rs,cs;
    if ( calculateItemPosition(n, r, c, rs, cs) )
    {
	if (!m_row_headers.empty())
	    ++c;
	if (!m_column_headers.empty())
	    ++r;
	item->setLayoutParameters(r, c, rs, cs);
	addItem(item, r, c);
	item->setParentItem(m_parent);
	return true;
    }
    return false;
}

void
GraphicsGridLayout::insertItem(GraphicsItem * item)
{
    if (!m_row_headers.empty())
	++item->column();
    if (!m_column_headers.empty())
	++item->row();
    addItem(item, item->row(), item->column());
    item->setParentItem(m_parent);
}

void
GraphicsGridLayout::updateSize() const
{
    qreal height = m_cell_size.height();
    qreal width = m_cell_size.width();
    for ( int i = 0; i < count(); ++i )
    {
	QSizeF s = itemAt(i)->preferredSize();

	height = std::max(height, s.height());
	width = std::max(width, s.width());
    }

    m_cell_size = QSizeF(width, height);

    qreal row_spacing = 0.;
    for (size_t i = 0; i < m_row_spacing.size(); ++i)
	row_spacing += m_row_spacing[i];

    qreal col_spacing = 0.;
    for (size_t i = 0; i < m_column_spacing.size(); ++i)
	col_spacing += m_column_spacing[i];

    qreal row_shift = 0.;
    for (size_t i = 0; i < m_row_shifts.size(); ++i)
	row_shift = std::max(m_row_shifts[i], row_shift);

    qreal col_shift = 0.;
    for (size_t i = 0; i < m_column_shifts.size(); ++i)
	col_shift = std::max(m_column_shifts[i], col_shift);

    int row_header = !m_row_headers.empty();
    int col_header = !m_column_headers.empty();
    m_size = QSizeF(
	m_cell_size.width()*(m_columns + row_header + col_spacing + row_shift + 2*m_h_margins),
	m_cell_size.height()*(m_rows + col_header + row_spacing + col_shift + 2*m_v_margins));

    m_bounding_rect = QRectF(QPointF(0,0), m_size);
}

void
GraphicsGridLayout::setGeometry(const QRectF & rect)
{
    if (m_rows*m_columns == 0)
	return;

    if (rect.isEmpty())
	return;

    qreal dx = rect.size().width()/m_size.width();
    qreal dy = rect.size().height()/m_size.height();

    m_size *= std::min(dx, dy);
    m_cell_size *= std::min(dx, dy);

    qreal left = (rect.size().width() - m_size.width())/2. + m_cell_size.width()*m_h_margins;
    qreal top  = (rect.size().height() - m_size.height())/2. + m_cell_size.height()*m_v_margins;

    for ( int n = 0; n < count(); ++n )
    {
	GraphicsItem* item = static_cast<GraphicsItem*>(itemAt(n));

	int row,col,rspan,cspan;
	item->getLayoutParameters(row, col, rspan, cspan);

	// Calculate left-top position of the current item
	qreal row_spacing = 0.;
	for (int i = 0; i < row; ++i)
	    row_spacing += m_row_spacing[i];

	qreal col_spacing = 0.;
	for (int i = 0; i < col; ++i)
	    col_spacing += m_column_spacing[i];

	QPointF leftop(
	    left + m_cell_size.width()*(col + col_spacing + m_row_shifts[row]),
	    top  + m_cell_size.height()*(row + row_spacing + m_column_shifts[col]));

	// Calculate size of the current item
	row_spacing = 0.;
	for (int i = row; i < row+rspan-1; ++i)
	    if (i < (int)m_row_spacing.size())
		row_spacing += m_row_spacing[i];

	col_spacing = 0.;
	for (int i = col; i < col+cspan-1; ++i)
	    if (i < (int)m_column_spacing.size())
		col_spacing += m_column_spacing[i];

	QSizeF size(
	    m_cell_size.width()*cspan + m_cell_size.width()*col_spacing,
	    m_cell_size.height()*rspan + m_cell_size.height()*row_spacing);

	item->setGeometry(QRectF(leftop, size));

    }
}

QRectF
GraphicsGridLayout::boundingRect() const
{
    return m_bounding_rect;
}

QSizeF
GraphicsGridLayout::sizeHint(Qt::SizeHint which, const QSizeF & constraint) const
{
    if ( which == Qt::PreferredSize ) {
	updateSize();
	return m_size;
    }

    return constraint;
}

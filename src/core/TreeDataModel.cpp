#include <dqm_display/core/TreeDataModel.h>

#include <config/Configuration.h>
#include <dal/Partition.h>
#include <dal/OnlineSegment.h>
#include <dal/util.h>

#include <ipc/threadpool.h>

#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQSummaryMaker.h>
#include <dqm_config/dal/DQAgent.h>

#include <dqm_display/core/Region.h>

#include <algorithm>

using namespace dqm_display::core;

namespace
{
    struct SubTreeLoader
    {
	SubTreeLoader( dqm_display::core::Region & parent, 
        	const dqm_config::dal::DQRegion * root )
	  : parent_(0),
	    root_( root )
	{
	    // Do this immediately to keep the order of the regions as defined in the database
	    try {
		parent_ = parent.addRegion(*root_);
	    }
	    catch( ers::Issue & ex ) {
		ers::error( ex );
	    }
	}

	void operator()();
        
      private:
	bool visitor( const dqm_config::dal::DQRegion* region, SubTreeLoader & helper );

	dqm_display::core::Region * parent_;
	const dqm_config::dal::DQRegion * root_;
    };
    
    void
    SubTreeLoader::operator()()
    {
	ERS_DEBUG(1, "Loading '" << root_->UID() << "' subtree" );
	root_ -> visit_all_regions( std::bind(&SubTreeLoader::visitor, this, std::placeholders::_1, *this) );
	ERS_DEBUG(1, "The '" << root_->UID() << "' subtree is loaded" );
    }
            
    bool
    SubTreeLoader::visitor( const dqm_config::dal::DQRegion * region, SubTreeLoader & helper )
    {
	ERS_DEBUG(2, "Region UID: " << region->UID() << " and algo: "<< region->get_DQSummaryMaker()->UID());
        
        if (region != root_)
        {
            try {
		helper.parent_ = helper.parent_->addRegion(*region);
	    }
	    catch( ers::Issue & ex ) {
		ers::error(ex);
		return false;
	    }
	}

	std::vector<const dqm_config::dal::DQParameter *> parameters;
	region->get_all_parameters(parameters);
	for ( size_t i = 0; i < parameters.size(); i++ )
	{
	    ERS_DEBUG(3, "Adding parameter '" << parameters[i]->UID() << "'");
	    try {
		helper.parent_->addParameter(*parameters[i]);
	    }
	    catch( ers::Issue & ex ) {
		ers::error(ex);
	    }
	}

	return true;
    }
}

TreeDataModel::TreeDataModel( const std::string & partition_name, 
                              const std::string & connect_string,
                              ResultListener & listener,
                              ReloadCallback reload_callback,
                              SubsystemSelector subsystemSelector )
  : partition_name_(partition_name),
    algo_factory_(partition_name_),
    input_(partition_name, "DQM"),
    configuration_(connect_string),
    db_reloaded_callback_(reload_callback),
    result_listener_(listener),
    root_region_(*this)
{
    const daq::core::Partition * partition = configuration_.get<daq::core::Partition>(partition_name);
    if ( !partition )
	throw daq::config::Generic(ERS_HERE, "partition is not found in the database");
    configuration_.register_converter(new daq::core::SubstituteVariables(*partition));

    ERS_DEBUG(1, "Finding root regions");
    std::vector<const dqm_config::dal::DQRegion *> regions;
    dqm_config::dal::DQRegion::get_root_regions(configuration_, *partition, regions);

    if (regions.size() == 1) {
	ERS_DEBUG(1, "Configuration contains unique root region - use it as DQ tree root node.");
	root_region_.config_ = regions[0];
	root_region_.name_ = QString(root_region_.config_->UID().c_str());
	root_region_.label_ = QString(root_region_.config_->get_Label().c_str());
	if (root_region_.label_.isEmpty())
	    root_region_.label_ = root_region_.name_;
	regions.clear();
	root_region_.config_->get_all_regions(regions);
    }

    ERS_DEBUG(1, "Building up DQ Tree, which has " << regions.size() << " branches");
    for( size_t i = 0; i < regions.size(); i++ )
    {
	SubTreeLoader job(root_region_, regions[i]);
	job();
    }

    ConfigurationSubscriptionCriteria criteria;
    criteria.add("DQRegion");           //for objects of this class removed/added
    criteria.add("DQParameter");        //for objects of this class removed/added
    criteria.add("DQTemplateRegion");	//for objects of this class removed/added
    criteria.add("DQTemplateParameter");//for objects of this class removed/added
    criteria.add("DQAlgorithm");        //for objects of this class modified
    criteria.add("DQLayout");
    criteria.add("DQShape");
    criteria.add("DQReference");
    criteria.add("Partition");

    try {
       subscription_ = configuration_.subscribe(criteria, configurationCallback, this);
       ERS_DEBUG(1, "Configuration Database Subscription Criteria: " << criteria);
    }
    catch (daq::config::Generic& ex) {
       ers::error( ex );
    }

    algo_factory_.updateRunParameters();
}

TreeDataModel::~TreeDataModel()
{
    try {
        configuration_.unsubscribe(subscription_);
    }
    catch (daq::config::Generic& ex) {
	ers::error( ex );
    };
}

void
TreeDataModel::configurationCallback( const std::vector<ConfigurationChange *> & , void * parameter)
{
    ERS_DEBUG(1, "DQM configuration has been updated.");
    TreeDataModel * model = (TreeDataModel *)parameter;
    model->db_reloaded_callback_();
}


/*! \file Parameter.cpp contains the Parameter class implementation.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/core/Parameter.h>

#include <ers/ers.h>

#include <oh/OHRootReceiver.h>

#include <dqm_core/Output.h>

#include <dqm_config/dal/DQAlgorithm.h>
#include <dqm_config/dal/DQParameter.h>
#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/exceptions.h>

#include <dqm_display/core/Region.h>
#include <dqm_display/core/TreeDataModel.h>
#include <dqm_display/gui/Histogram.h>
#include <dqm_display/gui/Status.h>

#include <QScopedPointer>
#include <QDebug>

using namespace dqm_display::core;

Parameter::Parameter(Region & parent, const dqm_config::dal::DQParameter & config)
  : TreeNode(parent, config),
    m_config(config),
    m_algorithm_config(parent_.model().createAlgorithmConfig(m_config)),
    m_histogram(0)
{
    ;
}

Parameter::~Parameter()
{
    delete m_histogram;
}

const std::vector<std::string> &
Parameter::getInputDataSource( ) const
{
    return m_config.get_InputDataSource();
}

void 
Parameter::changeState(bool enabled, const QString& ) const
{ 
    parent_.changeState(enabled, getName()); 
}

int 
Parameter::getPosition() const
{
    int pos = parent_.getPos(this);
    if ( pos >= 0 )
	pos += parent_.regionCount();
    return pos;
}

QString
Parameter::getReferenceName() const
{
    return parent_.model().getReferenceNameForParameter(name_);
}

bool Parameter::histogramExists() const
{
    return (m_histogram && !m_histogram->isNull());
}

dqm_display::gui::Histogram &
Parameter::getHistogram() const
{
    if (!m_histogram) {
	m_histogram = new dqm_display::gui::Histogram(name_,
	        m_config.get_DrawOption(),
	        m_config.get_CanvasOption(),
	        m_config.get_ScaleReference());
    }

    if ((int)(result_->time_t() - m_histogram->time()) <= 1 )
    {
        // we have a histogram which is not more than 1 second older than the
        // corresponding DQ result - we assume that the histogram is still up to date
	ERS_DEBUG(1, "Histogram is still up to date, won't read it");
	return *m_histogram;
    }

    TObject * ref = 0;
    try {
        ref = m_algorithm_config->getReference();
    }
    catch(ers::Issue & ex) {
        ers::error(ex);
    }

    TObject * histo = 0;
    time_t time = 0;
    try {
	OHRootObject hd = OHRootReceiver::getRootObject(
		    parent_.model().getPartitionName(), m_config.get_InputDataSource()[0]);
	histo = hd.object.release();
	time = hd.time.c_time();
    }
    catch (const ers::Issue & ex) {
        ers::warning(ex);
    }

    m_histogram->update(histo, time, ref, false);

    return *m_histogram;
}

QImage
Parameter::getImage(const QSize & size) const
{
    getHistogram();
    return m_histogram->getImage(size);
}

QVector<QPair<QString, QString>>
Parameter::readConfig() const {
    QVector<QPair<QString, QString>> config;

    const dqm_config::dal::DQParameter & dqParameter = getConfig();

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set InputDataSource
    ////////////////////////////////////////////////////////////////////////////////////////////
    const std::vector<std::string>& ids = dqParameter.get_InputDataSource();
    QString s;
    for ( uint i = 0; i < ids.size(); ) {
        s += QString::fromStdString(ids[i]);
        if (++i < ids.size()) {
            s += "<br/>";
        }
    }
    config.push_back(qMakePair(QString("Histogram(s)"), s));

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Reference Name
    ////////////////////////////////////////////////////////////////////////////////////////////
    QString r(getReferenceName());
    if ( !r.isEmpty() ) {
        config.push_back(qMakePair(QString("Reference"), r));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Weight
    ////////////////////////////////////////////////////////////////////////////////////////////
    config.push_back(qMakePair(QString("Weight"), QString("%1")
            .arg(dqParameter.get_Weight())));

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Algorithm Name
    ////////////////////////////////////////////////////////////////////////////////////////////
    const dqm_config::dal::DQAlgorithm *dqAlgorithm = dqParameter.get_DQAlgorithm();
    config.push_back(qMakePair(QString("Algorithm"),
            QString::fromStdString(dqAlgorithm->UID())));

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Algorithm Parameters
    ////////////////////////////////////////////////////////////////////////////////////////////
    QString cfg;
    const std::map<std::string, double> & params = m_algorithm_config->getParameters();
    const std::map<std::string, std::string> & gen_params = m_algorithm_config->getGenericParameters();
    if (!params.empty() || !gen_params.empty()) {
        std::map<std::string, double>::const_iterator it = params.begin();
        while ( it != params.end() ) {
            cfg += QString("%1=%2").arg(it->first.c_str()).arg(it->second);
            if ( ++it != params.end() || !gen_params.empty())
                cfg += ", ";
        }

        std::map<std::string, std::string>::const_iterator git = gen_params.begin();
        while ( git != gen_params.end() ) {
            cfg += QString("%1=%2").arg(git->first.c_str()).arg(git->second.c_str());
            if ( ++git != gen_params.end() )
                cfg += ", ";
        }
    }
    config.push_back(qMakePair(QString("Parameters"), cfg));

    ////////////////////////////////////////////////////////////////////////////////////////////
    // Set Algorithm Thresholds
    ////////////////////////////////////////////////////////////////////////////////////////////
    std::map<std::string, double> greenTh = m_algorithm_config->getGreenThresholds();
    std::map<std::string, double> redTh = m_algorithm_config->getRedThresholds();

    cfg = "";
    std::map<std::string, double>::const_iterator green = greenTh.begin();
    for (; green != greenTh.end(); ++green) {
        std::map<std::string, double>::const_iterator red = redTh.find(green->first);
        if (red != redTh.end()) {
            QString sign = green->second > red->second ? "&gt;" : "&lt;";
            cfg += QString( "<font color=green>%1</font>"
                                      "<font color=black>%2</font>"
                                      "<font color=orange>%3</font>"
                                      "<font color=black>%4</font>"
                                      "<font color=red>%5</font>"
                                      "<font color=black>; </font>"
                                    ).arg(green->second)
                                     .arg(sign)
                                     .arg(QString::fromStdString(green->first))
                                     .arg(sign)
                                     .arg(red->second);
        }
        else {
            cfg += QString( "<font color=green>%1</font>"
                                      "<font color=black>=</font>"
                                      "<font color=green>%2</font>"
                                      "<font color=black>; </font>"
                                    ).arg(QString::fromStdString(green->first))
                                     .arg(green->second);
        }
    }

    std::map<std::string, double>::const_iterator red = redTh.begin();
    for (; red != redTh.end(); ++red) {
        std::map<std::string, double>::const_iterator green = greenTh.find(red->first);
        if (green == greenTh.end()) {
            cfg += QString( "<font color=red>%1</font>"
                                      "<font color=black>=</font>"
                                      "<font color=red>%2</font>"
                                      "<font color=black>; </font>"
                                    ).arg(QString::fromStdString(red->first))
                                     .arg(red->second);
        }
    }

    config.push_back(qMakePair(QString("Thresholds"), cfg));
    return config;
}

QString
Parameter::makeConfigText() const
{
    QString text = QString("<b>%1</b>").arg(name_);

    try {
        auto c = readConfig();
        for (auto it = c.begin(); it != c.end(); ++it) {
            text += QString("<br/><font color=darkslategray>%1:</font> %2")
                    .arg(it->first, it->second);
        }
    }
    catch (const daq::config::Exception &ex) {
	ers::error(ex);
    }

    return text;
}

QVector<QPair<QString, QString>> Parameter::getAllAttributes() const {
    QVector<QPair<QString, QString>> c;

    try {
        c = readConfig();

        c.push_front(qMakePair(QString("Result"), getStatusHtml()));
        c.push_back(qMakePair(QString("Description"), getDescription()));
        c.push_back(qMakePair(QString("Troubleshooting"), getTroubleshooting()));
    }
    catch (const daq::config::Exception &ex) {
        ers::error(ex);
    }
    return c;
}

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/core/Region.h>
#include <dqm_display/core/TreeDataModel.h>
#include <dqm_display/gui/Status.h>

#include <dqm_config/dal/DQNode.h>
#include <dqm_config/dal/DQParameter.h>

#include <dal/ResourceBase.h>

#include <QDateTime>

using namespace dqm_display::core;

QString make_path(TreeNode * node)
{
    QString path;
    const TreeNode * n = node->getParent();
    for ( const TreeNode * p = n->getParent(); n != p; n = p, p = n->getParent() )
    {
	path.insert(0, "/" + n->getName());
    }
    return path;
}

TreeNode::TreeNode(Region & parent, const dqm_config::dal::DQNode & config)
  : parent_(parent), 
    name_(config.UID().c_str()),
    label_(config.get_Label().empty() 
    	? name_ 
        : QString::fromStdString(config.get_Label()).replace("\\n", "\n")),
    path_(make_path(this)),
    description_(config.get_Description().c_str()),
    troubleshooting_(config.get_Troubleshooting().c_str()),
    shape_(config.get_DQShape()),
    result_(new Result())
{
    parent.model().input().addListener(name_, this);
}

TreeNode::TreeNode()
  : parent_(*static_cast<Region*>(this)),
    shape_(0),
    result_(new Result())
{
    ;
}

TreeNode::~TreeNode()
{
    parent_.model().input().removeListener(name_, this);
}

const QString &
TreeNode::getAgentName() const 
{ 
    return parent_.getAgentName();
}

void
TreeNode::handleResult(std::shared_ptr<Result> & new_result)
{
    bool status_changed = false;
    bool status_degraded = false;
    {
        boost::mutex::scoped_lock lock(mutex_);
        const Result::Status s = result_->status();
        const Result::Status new_s = new_result->status();

        status_changed  = s != new_s;
        status_degraded = status_changed &&
        	(new_s == Result::Status::ERROR ||
        	(new_s == Result::Status::WARNING && s != Result::Status::ERROR));
        result_ = new_result;
    }
    parent_.model().resultListener().nodeStatusUpdated(*this, status_changed, status_degraded);
}

QString
TreeNode::makeResultText() const
{
     return resultToHtml(false);
}

QString
TreeNode::getStatusHtml() const
{
    return resultToHtml(true);
}

QString TreeNode::getToolTip() const
{
    QString tt = QString("<b>%1</b>").arg(getName());
    return tt + getStatusHtml();
}

QString TreeNode::resultToHtml(bool addImage) const {
    boost::mutex::scoped_lock lock(mutex_);

    if (!result_) {
        return QString("<i>no result was produced</i>");
    }

    QString text("<pre>");

    if (addImage) {
        text += QString("<img style=\"vertical-align:top\" src=\"%1\" width=\"16\" height=\"16\" /> ")
                        .arg(gui::Status::iconName(result_->status()));
    }

    text += QString("%1").arg(result_->timeString());

    if (result_->tags().empty()) {
        return text;
    }

    for (auto it = result_->tags().begin(); it != result_->tags().end(); ++it) {
	text += QString("<br/><font color=darkslategray>    %1</font>=%2")
	        .arg(it->name.c_str()).arg(it->value);
    }

    return text + "</pre>";
}

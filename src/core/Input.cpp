/*! \file Output.cpp Implements the Output class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/core/Input.h>
#include <dqm_display/core/Result.h>

#include <dqmf/is/Result.h>

#include <ers/ers.h>

using namespace dqm_display::core;

Input::Input(const IPCPartition & partition, const std::string & is_server_name)
	: ISInfoReceiver(partition), ISInfoDictionary(partition), m_server_name(
		is_server_name)
{
    ;
}

Input::~Input()
{
    stop();
}

void Input::start()
{
    try
    {
	subscribe(m_server_name, dqmf::is::Result::type(), &Input::callback,
		this, { ISInfo::Created, ISInfo::Updated, ISInfo::Subscribed });
	ERS_DEBUG(1, "Output listener has been subscribed to IS");
    }
    catch (const daq::is::Exception & ex)
    {
	ers::error(ex);
    }
}

void Input::stop()
{
    try {
	unsubscribe(m_server_name, dqmf::is::Result::type(), true);
    }
    catch (const daq::is::Exception & ex) {
	ers::error(ex);
    }
}

void Input::callback(ISCallbackInfo * const info)
{
    ERS_DEBUG(3,
	    "IS Listener was invoked for the '" << info->name()
		    << "' DQ parameter");

    Listeners::iterator it = m_listeners.find(info->objectName());
    if (it == m_listeners.end())
    {
	ERS_DEBUG(3,
		"No listener was found for the '" << info->name()
			<< "' information of the DQM Result type");
	return;
    }

    std::shared_ptr<Result> result(new Result(*info));

    for (int i = 0; i < it.value().size(); ++i) {
	it.value()[i]->handleResult(result);
    }
}

void Input::addListener(const QString & name,
	InputListener * const listener)
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Listeners::iterator it = m_listeners.find(name);

    if (it == m_listeners.end())
	m_listeners.insert(name, QVector<InputListener*>(1, listener));
    else
	it.value().push_back(listener);
}

void Input::removeListener(const QString & name,
	InputListener * const listener)
{
    std::unique_lock < std::mutex > lock(m_mutex);
    Listeners::iterator it = m_listeners.find(name);

    if (it != m_listeners.end()) {
	for (int i = 0; i < it.value().size(); ++i) {
	    if ( it.value()[i] == listener ) {
		it.value().remove(i);
		break;
	    }
	}
	if (it.value().empty()) {
	    m_listeners.erase(it);
	}
    }
}


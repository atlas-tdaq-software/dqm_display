#include <dqm_display/core/Result.h>

#include <QDateTime>

using namespace dqm_display::core;

std::time_t Result::s_run_started_at = INT_MAX;

Result::Result(const ISCallbackInfo & info)
{
    try {
	info.value( *this );
	m_time = time().c_time();
    }
    catch( daq::is::InfoNotCompatible & ex ) {
	ers::warning( ex );
    }
}

QString Result::timeString() const
{
    return QDateTime::fromTime_t(time_t()).toString("yyyy-MMM-dd hh:mm:ss");
}


/*! \file Region.cpp Implements the dqm_core::Region class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */
#include <sstream>
#include <iomanip>
#include <ctime>

#include <dqm_display/core/Region.h>

#include <ers/ers.h>

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlCommands.h"

#include <dqm_config/dal/DQRegion.h>
#include <dqm_config/dal/DQSummaryMaker.h>
#include <dqm_config/dal/DQAgent.h>

#include <dqm_display/core/Parameter.h>
#include <dqm_display/core/TreeDataModel.h>

#include <dqm_display/gui/Status.h>

namespace {
    QString get_agent_name(Configuration & config,
            const dqm_config::dal::DQRegion & region)
    {
        std::ostringstream out;
        out << "(this (\"DQRegions\" some ( object-id \"" << region.UID()
                << "\" =)))";
        std::vector<const dqm_config::dal::DQAgent *> agents;
        config.get(agents, false, false, out.str());
        return (agents.size() ? QString::fromStdString(agents[0]->UID()) : QString());
    }
}

using namespace dqm_display::core;

Region::Region(TreeDataModel & model, Region & parent, const dqm_config::dal::DQRegion & config)
	: TreeNode(parent, config), model_(model), config_(&config),
	  agent_name_(get_agent_name(model_.config(), *config_))
{
    ;
}

Region::Region(TreeDataModel & model)
	: model_(model), config_(0)
{
    name_ = "Systems";
}

Region::~Region()
{
    for (RegionsList::iterator it = regions_.begin(); it != regions_.end();
	    it++)
	delete *it;

    for (ParametersList::iterator it = parameters_.begin();
	    it != parameters_.end(); it++)
	delete *it;
}

const dqm_config::dal::DQLayout*
Region::getLayout( ) const
{
    return (config_ ? config_->get_DQLayout() : 0);
}

bool
Region::isRoot() const
{
    return &parent_ == this;
}

const QString &
Region::getAgentName() const
{
    return ( agent_name_.isEmpty() && &parent_ != this ? parent_.getAgentName() : agent_name_ );
}

void
Region::changeState(bool enabled, const QString& name) const
{
    QString n(name.isEmpty() ? getName() : name);

    QVector<QString> agents;
    model_.getRootRegion()->findAllAgents(n, agents);

    for (int i = 0; i < agents.size(); ++i)
    {
	ERS_DEBUG(1,
		"'" << n.toStdString() << "' parameter belonging to '" << agents[i].toStdString()
			<< "' agent is requested to be "
			<< (enabled ? "enabled" : "disabled"));

	IPCPartition p(model_.getPartitionName());
	daq::rc::CommandSender commander(p, "DQM Display");
	try
	{
	    daq::rc::UserCmd usrCmd(enabled ? "enable" : "disable",
		    std::vector<std::string>(1, n.toStdString()));
	    commander.userCommand(agents[i].toStdString(), usrCmd);
	}
	catch (ers::Issue &e)
	{
	    ers::error(e);
	    throw;
	}
    }
}

int
Region::getPos(const Region *region) const
{
    RegionsList::const_iterator pos = std::find(regions_.begin(),
	    regions_.end(), region);
    if (pos != regions_.end())
	return distance(regions_.begin(), pos);
    else
	return -1;
}

int
Region::getPos(const Parameter *parameter) const
{
    ParametersList::const_iterator pos = std::find(parameters_.begin(),
	    parameters_.end(), parameter);
    if (pos != parameters_.end())
	return distance(parameters_.begin(), pos);
    else
	return -1;
}

const TreeNode*
Region::findChild(const QString &name) const
{
    const TreeNode * const node = findRegion(name);
    return node ? node : findParameter(name);
}

const Region*
Region::findRegion(const QString &name) const
{
    const RegionsList::const_iterator end = regions_.end();
    for (RegionsList::const_iterator iter = regions_.begin(); iter != end;
	    ++iter)
	if ((*iter)->getName() == name)
	    return *iter;
    return NULL;
}

const Parameter*
Region::findParameter(const QString &name) const
{
    const ParametersList::const_iterator end = parameters_.end();
    for (ParametersList::const_iterator iter = parameters_.begin(); iter != end;
	    ++iter)
	if ((*iter)->getName() == name)
	    return *iter;
    return NULL;
}

void
Region::findAllChildren(const QString &name,
	QVector<TreeNode*> & children) const
{
    const RegionsList::const_iterator end = regions_.end();
    for (RegionsList::const_iterator iter = regions_.begin(); iter != end;
	    ++iter)
    {
	if ((*iter)->getName() == name)
	    children.push_back(*iter);
	(*iter)->findAllChildren(name, children);
    }

    const ParametersList::const_iterator pend = parameters_.end();
    for (ParametersList::const_iterator piter = parameters_.begin();
	    piter != pend; ++piter)
	if ((*piter)->getName() == name)
	    children.push_back(*piter);
}

void
Region::findAllAgents(const QString & name,
	QVector<QString> & agents) const
{
    QVector<TreeNode*> nodes;
    findAllChildren(name, nodes);

    for (int i = 0; i < nodes.size(); ++i)
    {
	agents.push_back(nodes[i]->getAgentName());
    }
}

Region *
Region::addRegion(const dqm_config::dal::DQRegion & rconfig)
{
    Region * region = new Region(model_, *this, rconfig);

    {
	boost::mutex::scoped_lock lock(mutex_);
	regions_.push_back(region);
    }

    ERS_DEBUG(1,
	    "new region '" << region->getName().toStdString()
	    << "' has been added to the '" << name_.toStdString() << "' region");

    return region;
}

Parameter *
Region::addParameter(const dqm_config::dal::DQParameter & config)
{
    Parameter * const parameter = new Parameter(*this, config);

    parameters_.push_back(parameter);

    ERS_DEBUG(1,
	    "new parameter '" << parameter->getName().toStdString()
	    << "' has been added to the '" << name_.toStdString() << "' region");
    return parameter;
}

const TreeNode *
Region::getChild(int index) const
{
    if ((unsigned) index < regions_.size())
	return regions_[index];

    index -= regions_.size();
    if ((unsigned) index < parameters_.size())
	return parameters_[index];

    return 0;
}

uint
Region::totalChildCount() const
{
    uint count = 0;
    for (auto r : regions_)
	count += r->totalChildCount();

    count += regions_.size();
    count += parameters_.size();
    return count;
}

void
Region::countRegionStatus(
	std::map<core::Result::Status, int> & totals) const
{
    for (uint i = 0; i < regions_.size(); ++i)
	regions_[i]->countRegionStatus(totals);

    for (uint i = 0; i < parameters_.size(); ++i)
	++totals[parameters_[i]->getResult()->status()];
}

QString
Region::getStatusHtml() const
{
    QString text = TreeNode::getStatusHtml();
    if (!result_->isValid()) {
        return text;
    }

    std::map<core::Result::Status, int> totals;
    countRegionStatus(totals);

    uint grand_total = 0;
    for (auto x : totals)
	grand_total += x.second;

    text += "<pre>Contains:<br/>";
    for (auto x : totals)
    {
	text +=	QString("   <img style=\"vertical-align:top\" src=\"%1\" width=\"16\" height=\"16\" /> %2 (%3%)<br/>")
			.arg(gui::Status::iconName(x.first))
			.arg(totals[x.first])
			.arg(100. * totals[x.first] / grand_total, 0, 'f', 1);
    }

    return text + "</pre>";
}

QImage
Region::getImage(const QSize & ) const
{
    return gui::Status::regionImage(getResult()->status());
}

QString
Region::makeConfigText() const
{
    if (!config_ || !m_config_text.isEmpty())
	return m_config_text;

    m_config_text = QString("<b>%1</b><br>").arg(name_);

    try {
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Set Weight
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_config_text += QString("<font color=darkslategray>Weight: </font>%1<br>")
	        .arg(config_->get_Weight());

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Set Algorithm Name
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	const dqm_config::dal::DQSummaryMaker *dqAlgorithm = config_->get_DQSummaryMaker();
	m_config_text += QString("<font color=darkslategray>Summary Maker: </font>%1<br>")
	        .arg(dqAlgorithm->UID().c_str());
    }
    catch (const daq::config::Exception &ex) {
	ers::error( ex );
    }

    return m_config_text;
}

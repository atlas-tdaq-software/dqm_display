tdaq_package()

remove_definitions(-DERS_NO_DEBUG)

find_package(Qt5 QUIET REQUIRED COMPONENTS Widgets Gui Core HINTS ${QT5_ROOT})
                          
qt5_add_resources(qrc_srcs src/display/Display.qrc)

file(GLOB ui_inputs src/display/ui/*.ui)
qt5_wrap_ui(ui_srcs ${ui_inputs})

file(GLOB moc_inputs dqm_display/gui/*.h)
qt5_wrap_cpp(moc_srcs ${moc_inputs})

tdaq_add_library(dqm_display_core		
  src/core/*.cpp
  INCLUDE_DIRECTORIES dqmf
  LINK_LIBRARIES Qt5::Core Qt5::Gui Qt5::Widgets ohroot dqm_config config rc_Commander)

tdaq_add_library(dqm_display_graphics		
  src/graphics/*.cpp
  INCLUDE_DIRECTORIES dqmf
  LINK_LIBRARIES dqm_config Qt5::Core Qt5::Gui Qt5::Widgets)

add_custom_target(dqm_display_generated DEPENDS ${ui_srcs} ${moc_srcs})

add_dependencies(dqm_display_graphics dqm_display_generated)
                                                                     
########################################
#	applications 
########################################
tdaq_add_executable(dqm_display
  src/display/*.cpp
  ${moc_srcs}
  ${qrc_srcs}
  ${ui_srcs}
  LINK_LIBRARIES dqm_display_core dqm_display_graphics tdaq-common::HistogramStyles
                 cmdline elisa_client_api ROOT::Gui ROOT::Rint ROOT::Graf ROOT::Gpad Qt5::Svg uuid)
set_target_properties(dqm_display PROPERTIES RUNTIME_OUTPUT_DIRECTORY output)

#ifndef _DQM_DISPLAY_GUI_CANVAS_WIDGET_H_
#define _DQM_DISPLAY_GUI_CANVAS_WIDGET_H_

/* \file CanvasWidget Declares the dqm_display::gui::CanvasWidget class.
 * \author Serguei Kolos
 */

#include <QImage>
#include <QWidget>
#include <TImage.h>

class TCanvas;
class TPad;

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
        
        /*! \class dqm_display::gui::CanvasWidget
         *  Class stands to output histogram on widget
         */
        class CanvasWidget : public QWidget {
	    Q_OBJECT

        public:
            CanvasWidget(QWidget* parent = 0, int width = 600, int height = 400);

            virtual ~CanvasWidget();

            void refresh();

            virtual void paintCanvas() = 0;

        protected:
            void update();

            bool event(QEvent*) override;
            void mouseMoveEvent(QMouseEvent* ) override;
            void mousePressEvent(QMouseEvent* ) override;
            void mouseReleaseEvent(QMouseEvent* ) override;
            void paintEvent(QPaintEvent* ) override;
            void resizeEvent(QResizeEvent* ) override;
            void showEvent(QShowEvent* ) override;
            
        protected:
            TCanvas* m_canvas;
        };
}}

#endif

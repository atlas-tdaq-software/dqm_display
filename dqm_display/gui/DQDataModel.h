#ifndef DQM_DISPLAY_GUI_DQ_DATA_MODEL_H
#define DQM_DISPLAY_GUI_DQ_DATA_MODEL_H

#include <ipc/threadpool.h>

#include <dqm_display/core/TreeDataModel.h>
#include <dqm_display/core/ResultListener.h>

#include <QAbstractItemModel>
#include <QDateTime>
#include <QFont>
#include <QScopedPointer>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
	class LoadDataThread;

	/// makes a QAbstractItemModel out of core::TreeDataModel to be displayed by TreeView
        class DQDataModel : public QAbstractItemModel,
			    public core::ResultListener
        {
            friend class LoadDataThread;

            Q_OBJECT

          public:
            static const core::TreeNode* treeNode(const QModelIndex &index)
            { return static_cast<const core::TreeNode*>(index.data(Qt::UserRole).value<void*>()); }

            DQDataModel(const std::string & partition_name, const std::string & db_connect_string);
            
            void clear();

            bool loadData();

            void runStarted() {
                m_data->runStarted();
            }

            const QScopedPointer<core::TreeDataModel> & model() const { return m_data; }

            QModelIndex rootIndex() const;

            QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
            
            Qt::ItemFlags flags(const QModelIndex &index) const;
            
            QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
            
            QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

            QModelIndex parent(const QModelIndex &index) const;
            
            int rowCount(const QModelIndex &parent = QModelIndex()) const;
            
            int columnCount(const QModelIndex &parent = QModelIndex()) const;
                        
            QModelIndex index(const core::TreeNode * node) const;

            void subscribeForUpdates();

          protected:
            /// multithread
            void nodeStatusUpdated(const core::TreeNode & node, bool status_changed, bool status_degraded);
            
            void timerEvent(QTimerEvent * event);

            void configurationReloaded() { emit configurationUpdated(); }

          signals:
            void resultProduced(const core::TreeNode * node);
            
            void alarmProduced(const core::TreeNode * node);
            
            void statsUpdated(const QString & last_update_time, int total_updates, int update_rate);

            void configurationUpdated();

          private:
            void loadDataThread(bool & done);

            const core::TreeNode* getNode(const QModelIndex &index) const
            { return static_cast<const core::TreeNode*>(index.internalPointer()); }

            QScopedPointer<core::TreeDataModel> m_data;
            std::string m_partition_name;
            std::string m_connect_string;
            const core::TreeNode * m_lastUpdatedNode;
            QFont m_regionFont;
            QFont m_parameterFont;
            int m_updates;
            int m_last_updates;
            int m_timer;
            IPCThreadPool m_thread;
        };
    }
}
#endif

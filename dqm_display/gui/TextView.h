#ifndef _DQM_DISPLAY_GUI_TEXT_VIEW_H
#define _DQM_DISPLAY_GUI_TEXT_VIEW_H

/*! \file TableView Declares the dqm_display::gui::TableView class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/TreeNode.h>

#include <QTextEdit>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::TableView
     *  The Table displayed in the DetailedPanel
     */
    class TextView : public QTextEdit {
        Q_OBJECT
        
      public:
        TextView(QWidget *parent = 0);

        void clear();

        void update(const core::TreeNode & node);
	
      public slots:
        void when_fontSizeChanged(int size);

      private:
        QTextDocument m_document;
        QString m_html;
        int m_font_size;
    };
  }
}

#endif

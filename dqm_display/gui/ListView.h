#ifndef _DQM_DISPLAY_GUI_LIST_VIEW_H
#define _DQM_DISPLAY_GUI_LIST_VIEW_H

/*! \file ListView Declares the dqm_display::gui::ListView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/ViewBase.h>

#include <QListView>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::ListView
     *  The tree displayed in the DetailedPanel
     */
    class ListView : public ViewBase<QListView>
    {
        Q_OBJECT
        
      public:
        ListView(QWidget *parent = 0);

        void setInteractive();

      protected:
        void resizeEvent(QResizeEvent * event) override;

      private:
        bool m_interactive = false;

      protected slots:
  	void when_clicked(const QModelIndex & index)
  	{
  	    processClick(index);
  	}

  	void when_menu_requested(const QPoint & pos)
  	{
  	    showMenu(pos);
  	}

      signals:
    	void nodeActivated(const core::TreeNode * node);

        void viewHeightChanged(int height, const QModelIndex & index);
    };
  }
}

#endif

#ifndef _DQMI_GUI_GRAPHICS_ITEM_H
#define _DQMI_GUI_GRAPHICS_ITEM_H

/*! \file GraphicsItem Declares the dqm_display::gui::GraphicsItem class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/Parameter.h>
#include <dqm_display/core/Region.h>
#include <dqm_display/gui/TreeNodeMenu.h>

#include <QGraphicsPathItem>
#include <QGraphicsLayoutItem>
#include <QFont>
#include <QSizeF>

#include <ers/ers.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {

    class GraphicsItem:	public QObject,
    			public QGraphicsPathItem,
			public QGraphicsLayoutItem
    {
	Q_OBJECT

      public:

        enum struct Shape { Ring, Trapezoid, Filler };

      public:
        GraphicsItem(	const core::TreeNode & node,
           		TreeNodeMenu & menu,
        		const dqm_config::dal::DQShape * default_shape);

        GraphicsItem(const char * text, int column, int row, int cspan, int rspan);

        virtual void setLayoutHint(double angle, double radius, double offset);

        void setGeometry(const QRectF &geom);

        QSizeF sizeHint(Qt::SizeHint which, const QSizeF & constraint = QSizeF()) const;

	void hoverEnterEvent(QGraphicsSceneHoverEvent * event);

	void hoverLeaveEvent(QGraphicsSceneHoverEvent * event);

        void mousePressEvent(QGraphicsSceneMouseEvent * event);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);
        
        void updateStatusColor();

        double aspectRatio() const { return m_aspect_ratio; }

        void setLayoutParameters(int row, int column, int rspan, int cspan) {
            m_row = row, m_column = column, m_rspan = rspan, m_cspan = cspan;
        }

        void getLayoutParameters(int & row, int & column, int & rspan, int & cspan) const {
            row = m_row, column = m_column, rspan = m_rspan, cspan = m_cspan;
        }

        int row() const { return m_row; }

        int column() const { return m_column; }

        int & row() { return m_row; }

        int & column() { return m_column; }

        const core::TreeNode * node() const {
            return m_node;
        }

        void setSelected(bool status) {
            m_selected = status;
            setZValue(m_selected);
            update();
        }

      signals:
	void nodeActivated(const core::TreeNode * node);

      protected:
	QRectF labelRect(QPainter * );

      protected:
        const core::TreeNode *	m_node;
        const QString		m_text;
        const QFont		m_font;

        TreeNodeMenu * 		m_menu;

        mutable QSizeF	m_size;

        QRectF	m_label_rect;
        Shape	m_shape;
        double	m_aspect_ratio;
        double	m_length_ratio;
        double	m_rotation;
        double	m_layout_azimuth;
        double	m_layout_radius;
        double	m_layout_offset;
        bool	m_transparency;
        bool	m_show_label;
        bool	m_circular_layout;
        int	m_row;
        int	m_column;
        int 	m_rspan;
        int	m_cspan;
        bool	m_draw_shape;
        bool    m_mouse_in;
        bool    m_selected;
    };
  }
}

#endif

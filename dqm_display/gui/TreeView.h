#ifndef _DQM_DISPLAY_GUI_TREE_VIEW_H
#define _DQM_DISPLAY_GUI_TREE_VIEW_H

/*! \file TreeView Declares the dqm_display::gui::TreeView class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ViewBase.h>

#include <QAbstractProxyModel>
#include <QModelIndex>
#include <QTreeView>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::TreeView
     *  The tree displayed in the DetailedPanel
     */
    class TreeView : public ViewBase<QTreeView> {
        Q_OBJECT
        
      public:
        TreeView(QWidget *parent = 0);

        virtual ~TreeView() = default;

        void setProxyModel(QAbstractProxyModel * model);

        bool isIndexInTheView(const QModelIndex & i) { return !isIndexHidden(i); }

        void updateNode(const QModelIndex & index);

      protected:
        void resizeEvent(QResizeEvent * event) override;
	
      protected slots:
	virtual void when_clicked(const QModelIndex & index)
	{
	    processClick(index);
	}

	void when_menu_requested(const QPoint & pos)
	{
	    showMenu(pos);
	}

      signals:
  	void nodeActivated(const core::TreeNode * node);

        void viewHeightChanged(int height, const QModelIndex & index);
    };
  }
}

#endif

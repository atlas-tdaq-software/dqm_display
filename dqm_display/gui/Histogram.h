#ifndef _DQM_DISPLAY_GUI_HISTOGRAM_H_
#define _DQM_DISPLAY_GUI_HISTOGRAM_H_

/* \file Histogram Declares the dqm_display::gui::Histogram class.
 * \author Yurij Ilchenko, Kevin Slagle. Serguei Kolos
 */

#include <ctime>

#include <QImage>
#include <QSize>
#include <QString>

#include <TCanvas.h>
#include <TVirtualPadPainter.h>

class TCanvas;
class TImage;
class TGraph;
class TMultiGraph;
class TNamed;
class TH1;
class TLegend;
class TObject;
class TPad;

namespace dqm_display::core {
  class Parameter;
  class TreeNode;
}

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
        
        /*! \class dqm_display::gui::Histogram
         *  Class is used to convert histogram to an image
         */
        class Histogram {
            enum HistoType { TH1_TYPE, TH2_TYPE, TH3_TYPE, TGraph_TYPE, TGraph2D_TYPE, OTHER_TYPE };

        public:
            Histogram(TCanvas * c = 0);

            Histogram(  const QString & name,
                        const std::string & drawOption,
			const std::string & canvasOption,
			bool scaleRef);

            ~Histogram();

            const QString & getName() const {
                return m_name;
            }

            void clear();

            void setNode(const core::Parameter * parameter);

            std::time_t time() const { return m_time; }

            bool isNull() const { return m_histogram == 0; }
	    
            void update(TObject* histo, std::time_t time, TObject* refHisto, bool resetCanvas);

            void copyFrom(const Histogram & h);

            void paint();

            QImage getImage(const QSize & size = QSize());

            void reset();

        private:
	    void plotReference();
	    void plotHistogram(bool noTitle);
            void plotTime();
            bool setHistogram(TObject *histo, TNamed*& m_h, HistoType& m_t);
            void setPadsOptions(TPad * pad1, TPad * pad2);
            
        private:
            QString      m_name;
            TCanvas*     m_canvas;
            TImage*      m_image;
            TNamed*	 m_histogram;
            std::time_t  m_time;
            TNamed*	 m_reference;
            TObject*     m_reference_pointer;
            TH1*         m_clone_reference;
	    std::string  m_draw_option;
	    std::string	 m_canvas_option;
	    bool	 m_scale_ref;
	    TMultiGraph* m_multiGraph;
            TLegend*	 m_legend;
            HistoType	 m_type;
            HistoType	 m_ref_type;
            bool	 m_split_canvas;
            QSize        m_size;
            bool         m_empty;
        };
}}

#endif

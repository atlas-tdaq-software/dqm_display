#ifndef _DQMI_GUI_GRAPHICS_REGION_H
#define _DQMI_GUI_GRAPHICS_REGION_H

/*! \file GraphicsRegion Declares the dqm_display::gui::GraphicsRegion class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqm_config/dal/DQLayout.h>

#include <dqm_display/gui/GraphicsItem.h>
#include <dqm_display/gui/GraphicsGridLayout.h>

#include <QScopedPointer>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {

    class GraphicsRegion: public GraphicsItem
    {        
	Q_OBJECT

      public:
        GraphicsRegion(	const core::Region & region,
			TreeNodeMenu & menu,
        		const dqm_config::dal::DQShape * default_shape = 0,
        		bool top_level = false);

        GraphicsGridLayout * takeLayout() { return m_layout.take(); }

        GraphicsGridLayout * getLayout() { return m_layout.data(); }

        void setGeometry(const QRectF &geom);

        void setLayoutHint(double angle, double radius, double offset);

        QSizeF sizeHint(Qt::SizeHint which, const QSizeF & constraint) const;
        
      protected:
        GraphicsGridLayout * createLayout(const dqm_config::dal::DQLayout * layout, const QString & name);

      protected:
        QScopedPointer<GraphicsGridLayout>	m_layout;
	QSizeF 					m_layout_size;
    };
  }
}

#endif

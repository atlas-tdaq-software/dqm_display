#ifndef _DQMI_GUI_STATUS_H_
#define _DQMI_GUI_STATUS_H_

/*! \file DQSettings Declares the dqm_display::gui DQM Status.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */
 
#include <TColor.h>
#include <QColor>
#include <QIcon>
#include <QMetaType>
#include <QPixmap>
#include <QString>

#include <dqm_display/core/Result.h>

Q_DECLARE_METATYPE(dqm_display::core::Result::Status)

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui
    {
	/*! \headerfile dqm_display::gui::Status
	 *  This is a header for DQM related constants.
	 */
	namespace Status
	{
	    Color_t color_t(dqm_display::core::Result::Status s);

	    const QColor & color(dqm_display::core::Result::Status s);

	    const QString & htmlColor(dqm_display::core::Result::Status s);

	    const char * iconName(dqm_display::core::Result::Status s);

	    QIcon icon(dqm_display::core::Result::Status s);

	    QImage regionImage(dqm_display::core::Result::Status s);
	}
    }
}

#endif

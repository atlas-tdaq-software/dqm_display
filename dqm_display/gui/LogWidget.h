#ifndef _DQMI_GUI_LOGWIDGET_H_
#define _DQMI_GUI_LOGWIDGET_H_

/*! \file LogWidget Declares the dqm_display::gui::LogWidget class.
 * \author Anyes Taffard, Kevin Slagle, Serguei Kolos
 * \version 1.0
 */
#include <ers/InputStream.h>

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/AlarmsMenu.h>
#include <dqm_display/gui/LogsMenu.h>

#include <ui_LogWidget.h>

#include <QIcon>
#include <QMouseEvent>
#include <QWidget>

class QTableWidget;

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::LogWidget
     *  Displays log and alarm messages
     */
    class LogWidget : public QWidget,
		      public Ui::LogWidget,
		      public ers::IssueReceiver
    {
	Q_OBJECT
      
      public:
	LogWidget(QWidget * parent);
	~LogWidget();
        
        void subscribe(const std::string & partition_name);

        QSize sizeHint() const override {
            return QSize(300, 100);
        }

      signals:
	void alarmAcknowledged(const core::TreeNode *);
	void messageReceived(int severity, const QString & time, const QString & application, const QString & message);
	
      public slots:    
	void when_alarmProduced(const core::TreeNode * node);
	void when_messageReceived(int severity, const QString & time, const QString & application, const QString & message);
        
      private slots:
        void showAlarmsMenu(const QPoint & pos);
	void showCurrent();
	void removeCurrent();
	void removeAcknowledged();
	void removeAll();

	void showLogsMenu(const QPoint & pos);
	void removeCurrentMessage();
	void removeAllMessages();

      private:
	void receive(const ers::Issue & issue);

      private:
        static const int s_maxEntries;
        
      private:
        AlarmsMenu m_alarms_menu;
        LogsMenu m_logs_menu;
        std::unique_ptr<ers::IssueCatcherHandler> m_handler;
    };
  }
}

#endif

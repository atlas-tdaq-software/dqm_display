#ifndef _DQM_DISPLAY_GUI_LOGS_MENU_H
#define _DQM_DISPLAY_GUI_LOGS_MENU_H

/*! \file LogsMenu Declares the dqm_display::gui::LogsMenu class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <QMenu>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::LogsMenu
     */
    class LogsMenu : public QMenu {
        Q_OBJECT
        
      public:
        LogsMenu(QWidget * parent);
    };
  }
}

#endif

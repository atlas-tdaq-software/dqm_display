#ifndef _DQM_DISPLAY_GUI_TABLE_VIEW_H
#define _DQM_DISPLAY_GUI_TABLE_VIEW_H

/*! \file TableView Declares the dqm_display::gui::TableView class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ViewBase.h>

#include <QAbstractProxyModel>
#include <QModelIndex>
#include <QTableView>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::TableView
     *  The Table displayed in the DetailedPanel
     */
    class TableView : public ViewBase<QTableView> {
        Q_OBJECT
        
      public:
        TableView(QWidget *parent = 0);

        void setProxyModel(QAbstractProxyModel * model);

        bool isIndexInTheView(const QModelIndex & i) { return !isIndexHidden(i); }

        void updateNode(const QModelIndex & index);
	
      signals:
        void fontSizeChanged(int size);
    };
  }
}

#endif

#ifndef _DQM_DISPLAY_GUI_RESULT_HISTOGRAM_H_
#define _DQM_DISPLAY_GUI_RESULT_HISTOGRAM_H_

/* \file ResultHistogram Declares the dqm_display::gui::ResultHistogram class.
 * \author Serguei Kolos
 */

#include <QMetaType>

#include <TQtWidget.h>

class TObject;

namespace dqm_display::core {
  class Parameter;
  class TreeNode;
}

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
        
        /*! \class dqm_display::gui::ResultHistogram
         *  Class stands to output histogram on widget
         */
        class ResultHistogram : public TQtWidget {
            enum HistoType { TH1_TYPE, TH2_TYPE, TH3_TYPE, TGraph_TYPE, TGraph2D_TYPE, OTHER_TYPE };
        public:
            ResultHistogram(QWidget* parent = 0);

            ResultHistogram(const core::Parameter * parameter);

            ~ResultHistogram();
            
            void setNode(const core::TreeNode * node);
            
            void clear();

            void update();

            QPixmap getPixmap(const QSize & size = QSize());

            bool isNull() const { return m_object == 0; }
	    
        protected:
            void repaint();

	    QSize sizeHint() const;

            void setSize(const QSize & size);

        private:
	    void plotHistogram();
            
        private:            
            TObject*	 m_object;
            bool	 m_need_redraw;
            const core::Parameter * m_parameter;
        };
}}

#endif

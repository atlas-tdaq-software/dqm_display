/*
 * Elog.h
 *
 *  Created on: Dec 11, 2013
 *      Author: kolos
 */

#ifndef _DQM_DISPLAY_ELOG_H_
#define _DQM_DISPLAY_ELOG_H_

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/Histogram.h>

#include <ui_ElogDialog.h>
#include <QApplication>
#include <QCloseEvent>
#include <QDialog>
#include <QButtonGroup>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui {

class Elog: public QDialog, public Ui::ElogDialog
{
    Q_OBJECT

public:
    static Elog & instance() {
	static Elog * i = new Elog;
	return *i;
    }

    void addAttachment(const core::TreeNode * node);

    void addAttachment(Histogram & histogram);

    static bool runningAtP1();

    void closeEvent(QCloseEvent *e) override {
        e->ignore();
        emit reject();
    }

public slots:
    void accept();

    void reject() {
        hide();
    }

    void when_stateChanged();

    void when_attachmentStateChanged(QListWidgetItem* item);

private:
    Elog();

private:
    QButtonGroup m_systems;
    QMap<QListWidgetItem*, QPixmap> m_images;
};

}}

#endif


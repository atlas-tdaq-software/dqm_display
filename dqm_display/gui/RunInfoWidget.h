#ifndef _DQMI_GUI_RUNINFO_WIDGET_H_
#define _DQMI_GUI_RUNINFO_WIDGET_H_

/*! \file RunInfoWidget Declares the dqm_display::gui::RunInfoWidget class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/RunInfoReceiver.h>

#include <QDateTime>
#include <QLabel>
#include <QSharedPointer>
#include <QString>
#include <QTime>
#include <QVector>
#include <QWidget>

class QTableWidget;

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::RunInfoWidget
     *  Displays run information
     */
    class RunInfoWidget : public QLabel
    {
	Q_OBJECT
      
      public:
	RunInfoWidget(const QString & partition_name);

      signals:
	void databaseRestarted();

        void runStarted();

      private:
	void watchRunInfo(const ISInfoDynAny & info);

	void watchRunStart(const ISInfoDynAny & info);

	void watchRunParams(const ISInfoDynAny & info);

	void watchRootController(const ISInfoDynAny & info);

	void watchReady4Physics(const ISInfoDynAny & info);

	void update();

      private:
	QVector<QSharedPointer<RunInfoReceiver>> m_receivers;
	QString m_partition_name;
	QString m_run_state;
	QString	m_run_type;
	int	m_run_number;
	bool	m_stable_beam;
	bool	m_ready_4_physics;
	int	m_beam_energy;
	QTime	m_total_time;
	QDateTime m_start_time;
    };
  }
}

#endif

#ifndef _DQM_DISPLAY_GUI_VIEW_BASE_H
#define _DQM_DISPLAY_GUI_VIEW_BASE_H

/*! \file ViewBase Declares the dqm_display::gui::ViewBase class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/TreeNodeMenu.h>
#include <dqm_display/gui/DQDataModel.h>

#include <QApplication>
#include <QAbstractProxyModel>
#include <QDebug>
#include <QModelIndex>
#include <QItemSelection>
#include <QMouseEvent>
#include <QSortFilterProxyModel>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::ListView
     *  The tree displayed in the DetailedPanel
     */
    template <class View>
    class ViewBase : public View
    {
      public:
        ViewBase(QWidget *parent = 0)
          : View(parent),
            m_menu(this),
            m_proxy_model(0)
	{
	    this->setContextMenuPolicy(Qt::CustomContextMenu);
	}

        virtual void updateNode(const QModelIndex & index)
        {
            if (this->isVisible()) {
        	this->update(m_proxy_model->mapFromSource(index));
            }
        }

        void setModel( QAbstractItemModel * model )
        {
            delete m_proxy_model;
            m_proxy_model = new QSortFilterProxyModel(this);
            m_proxy_model->setSourceModel(model);
            View::setModel(m_proxy_model);
            m_proxy_model->sort(0);
        }

        void setRootIndex(const QModelIndex & index)
        {
            View::setRootIndex(m_proxy_model->mapFromSource(index));
        }

        void setCurrentIndex(const QModelIndex & index)
        {
            View::setCurrentIndex(m_proxy_model->mapFromSource(index));
        }

        QModelIndex currentIndex() const
        {
            return m_proxy_model->mapToSource(View::currentIndex());
        }

      protected:
        virtual void nodeActivated(const core::TreeNode * ) {}

        virtual void processClick(const QModelIndex & index)
        {
            Qt::KeyboardModifiers km = QApplication::keyboardModifiers();
            bool key_pressed = (km & Qt::ControlModifier) || (km & Qt::ShiftModifier);

            if (!key_pressed) {
                const core::TreeNode* node = DQDataModel::treeNode(index);
                emit nodeActivated(node);
            }
        }

        bool viewportEvent(QEvent *e) override {
            if (e->type() == QEvent::Paint) {
                QApplication::setOverrideCursor(Qt::WaitCursor);
            }
            bool r = View::viewportEvent(e);
            if (e->type() == QEvent::Paint) {
                QApplication::restoreOverrideCursor();
            }
            return r;
        }

        void showMenu(const QPoint & pos)
        {
            QModelIndex index = this->indexAt(pos);
            if ( index.isValid() )
            {
        	m_menu.setNode(DQDataModel::treeNode(index));
        	m_menu.move(this->mapToGlobal(pos));
        	m_menu.show();
            }
        }

      protected:
        TreeNodeMenu m_menu;
        QAbstractProxyModel * m_proxy_model;
    };
  }
}

#endif

#ifndef DQM_DISPLAY_RUN_INFO_RECEIVER_H
#define DQM_DISPLAY_RUN_INFO_RECEIVER_H

/*! \file RunInfoReceiver.h Declares the RunInfoReceiver class.
  * \authors Serguei Kolos
  */

#include <string>
#include <memory>

#include <is/infodynany.h>
#include <is/inforeceiver.h>

#include <QObject>
#include <QString>

class IPCPartition;
class ISCallbackInfo;

/*! \namespace dqm_display
  *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
  */
namespace dqm_display {
  /*! \namespace gui
  *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
  */
  namespace gui {
    /*! \class dqm_display::gui::InfoRunReceiver
    *  Helper class for receiving information from IS server.
    *  Used by the SummaryPanel to display info on the top about the current run.
    */
    class RunInfoReceiver : public QObject {
        Q_OBJECT
        
        typedef std::function<void (const ISInfoDynAny & info)> Callback;
        
      public:
        RunInfoReceiver(const QString & partition_name, const QString & name, Callback callback );
        
      private slots:
	void when_runInfoUpdated(const ISInfoDynAny * any);
        
      signals:
	void runInfoUpdated(const ISInfoDynAny * any);
        
      private:
        void callbackReceived(ISCallbackInfo * isc);
        
      private:
         ISInfoReceiver	m_receiver;
         Callback	m_callback;
    };
  }
}

#endif // InfoRunReceiver_H

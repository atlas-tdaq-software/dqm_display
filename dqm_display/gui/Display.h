#ifndef _DQMI_GUI_DISPLAY_H
#define _DQMI_GUI_DISPLAY_H

/*! \file Display Declares the dqm_display::gui::Display class.
 * \author Kevin Slagle, Serguei Kolos
 * \version 1.0
 */

#include <ui_MainWindow.h>

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/About.h>
#include <dqm_display/gui/DQDataModel.h>
#include <dqm_display/gui/ParameterFilterModel.h>
#include <dqm_display/gui/SystemFilterModel.h>

#include <QActionGroup>
#include <QDebug>
#include <QGraphicsDropShadowEffect>
#include <QTimer>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui {
class Find;
class ListViewHistoryModel;
class DQDataModel;

/*! \class dqm_display::gui::Display
 *  MainWindow class for the DQM Display
 */
class Display: public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    Display(DQDataModel * model, About * about);

    ~Display();

    static bool isDivideByReferenceOn() {
        return m_divide_by_reference;
    }

    DQDataModel* getTreeModel() const
    {
	return m_mainModel;
    }

    void saveState();

    void restoreState();

public slots:
    void when_resultProduced(const core::TreeNode * node);

    void when_nodeActivated(const core::TreeNode * node);

    void when_actionViewTriggered(QAction * action);

    void when_configurationUpdated();

    void when_runStarted();

    void when_statsUpdated(const QString & last_update_time, int total_updates, int update_rate);

    void on_actionView_ZoomIn_triggered();

    void on_actionView_ZoomOut_triggered();

    void on_actionView_IncreaseFontSize_triggered();

    void on_actionView_DecreaseFontSize_triggered();

    void on_actionView_FitToView_triggered();

    void on_actionView_DivideByReference_triggered();

    void on_actionNavigation_Find_triggered();

    void on_actionNavigation_Back_triggered();

    void on_actionNavigation_Forward_triggered();

    void on_actionNavigation_LevelUp_triggered();

    void on_actionNavigation_Home_triggered();

    void on_actionHelp_About_triggered();

    void on_action_Elog_triggered();

    void on_action_AddToElog_triggered();

    void on_pushButton_Lock_clicked(bool );

    void handleRootEvents();

protected:
    void addToHistory(const core::TreeNode* node);

    void updateNavigationButtons(const core::TreeNode* node);

    void updateHistogramView(const core::TreeNode * node, bool force = true);

    void updateViews(const QModelIndex & index);

    void activateNode(const core::TreeNode * node);

    void closeEvent(QCloseEvent * ev);

    void signalFontSizeChange();

    void signalImageSizeChange();

signals:
    void fontSizeChanged(int size);

    void viewHeightChanged(int height, const QModelIndex & index);

    void iconsRepaintRequired(const QModelIndex & index);

private:
    void setDropShadowEffect();

private:
    DQDataModel * m_mainModel;
    SystemFilterModel * m_systemModel;
    ParameterFilterModel * m_parameterModel;
    ListViewHistoryModel * m_listViewHistoryModel;
    Find * m_findDialog;
    About * m_aboutDialog;
    QLabel * m_path_label;
    QLabel * m_stat_label;
    QGraphicsDropShadowEffect* m_histogram_Widget_shadow;

    QActionGroup m_systemActionGroup;
    QActionGroup m_viewActionGroup;

    QVector<const core::TreeNode*> m_history;
    int m_history_position;
    QAction * m_current_view;
    int m_histograms_height;
    int m_font_size;
    QString m_rc_state;
    QModelIndex m_current_index;
    QTimer m_timer;

    static bool m_divide_by_reference;
};

}}

#endif

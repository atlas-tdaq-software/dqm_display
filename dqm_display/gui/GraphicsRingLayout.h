#ifndef _DQMI_GUI_GRAPHICS_RING_LAYOUT_H
#define _DQMI_GUI_GRAPHICS_RING_LAYOUT_H

/*! \file GraphicsRingLayout Declares the dqm_display::gui::GraphicsRingLayout class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/GraphicsGridLayout.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    class GraphicsRingLayout : public GraphicsGridLayout
    {        
      public:
        GraphicsRingLayout(GraphicsRegion * parent, const dqm_config::dal::DQLayout * layout, const QString & name);
        
        void setGeometry(const QRectF & rect);

        void setAngle(double angle);

        void updateSize() const;

      private:
        qreal m_sector_angle;
        qreal m_total_v_spacing;
        qreal m_rotation;
    };
  }
}

#endif

#ifndef _DQM_DISPLAY_GUI_TREE_NODE_MENU_H
#define _DQM_DISPLAY_GUI_TREE_NODE_MENU_H

/*! \file TreeNodeMenu Declares the dqm_display::gui::TreeNodeMenu class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <QMenu>

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/UpdateParameters.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::TreeNodeMenu
     *  The tree displayed in the DetailedPanel
     */
    class TreeNodeMenu : public QMenu {
        Q_OBJECT
        
      public:
        TreeNodeMenu(QWidget * parent);
        
        void setNode(const core::TreeNode * node);

      signals:
	void nodeActivated(const core::TreeNode* );

      protected:
	void sendCommand(bool enable);

      protected slots:
	void exploreNode();

	void enableNode() { sendCommand(true); }
	void disableNode() { sendCommand(false); }
	void addImageToElog();
	void updateParameters();
	
      private:
	const core::TreeNode * m_node;
	UpdateParameters m_parameters_dialog;
    };
  }
}

#endif // _DQMI_GUI_TreeNodeMenu_H

#ifndef _DQMI_GUI_DETAILS_VIEW_MODEL_H
#define _DQMI_GUI_DETAILS_VIEW_MODEL_H

/*! \file DetailsViewDelegate.h Declares the gui::DetailsViewDelegate class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <QAbstractItemModel>

#include <dqm_display/gui/IconsViewDelegate.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    class DetailsViewDelegate : public IconsViewDelegate
    {
	Q_OBJECT
	
    public:
	DetailsViewDelegate(int row_height, int font_size, int spacing,
	        QAbstractItemModel *model, QObject *parent);
	
	void paint(QPainter * painter, const QStyleOptionViewItem & option,
	        const QModelIndex & index) const;

    public slots:
        void when_fontSizeChanged(int size);

    private:
        QAbstractItemModel * m_model;
        int m_font_size;
    };
  }
}

#endif

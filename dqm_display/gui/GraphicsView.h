#ifndef _DQMI_GUI_GRAPHICS_VIEW_H_
#define _DQMI_GUI_GRAPHICS_VIEW_H_

/*! \file GraphicsView Declares the dqm_display::gui::GraphicsView class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <QGraphicsView>

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/GraphicsRegion.h>
#include <dqm_display/gui/TreeNodeMenu.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui 
  {
    /*! \class dqm_display::gui::GraphicsView
     *  
     */
    class GraphicsView: public QGraphicsView {
        Q_OBJECT
        
      public:
        GraphicsView(QWidget * parent=0);
        
        void fitSceneToView();

        void setNode(const core::TreeNode * node);

        void updateNode(const core::TreeNode * node);

        void scale(double q);

        void setAutoScale();

        void clear();

      signals:
	void nodeActivated(const core::TreeNode * node);

      protected:
        void resizeEvent(QResizeEvent * event) override;

	void showEvent(QShowEvent * event) override;

	bool viewportEvent(QEvent * event) override;

      private:
        void selectNode();

	void setupScene();

      private:
      	QScopedPointer<GraphicsRegion> m_region;
        bool m_scene_set;
        bool m_scaled;
      	GraphicsGridLayout* m_layout;
      	GraphicsItem* m_selected;
        const core::TreeNode* m_node_to_select;
        TreeNodeMenu m_menu;
    };
  }
}

#endif

#ifndef _DQMI_GUI_LIST_VIEW_HISTORY_MODEL_H
#define _DQMI_GUI_LIST_VIEW_HISTORY_MODEL_H

/*! \file ListViewHistoryModel.h Declares the gui::ListViewHistoryModel class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ResultHistoryGraph.h>
#include <dqm_display/core/TreeNode.h>

#include <QAbstractListModel>
#include <QStyledItemDelegate>
#include <QPair>
#include <QPixmap>
#include <QSharedPointer>
#include <QWidget>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    
    class ListViewHistoryModel : public QAbstractListModel
    {
	Q_OBJECT
        
    public:
	ListViewHistoryModel(QWidget * parent, const QString & partition_name);
        
	void setNode(const core::TreeNode* node);
	
	void update();
	
	QVariant data(const QModelIndex& index, int role) const;
        
        QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
	  	  
	int rowCount(const QModelIndex& parent = QModelIndex()) const;

	void clear();
	  
    private:
     	typedef QVector<QSharedPointer<ResultHistoryGraph>>	HistoryGraphs;
        
        IPCPartition		m_partition;
        const core::TreeNode*	m_node;
        QString			m_name;
        QFont			m_first_font;
        QFont			m_others_font;
        HistoryGraphs		m_graphs;
        QWidget                 m_widget;
    };    
  }
}

#endif

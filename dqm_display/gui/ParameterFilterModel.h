#ifndef DQMI_GUI_PARAMETER_FILTER_MODEL_H
#define DQMI_GUI_PARAMETER_FILTER_MODEL_H

/*! \file SytemFilterModel.h Declares the gui::SytemFilterModel class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ResultHistoryGraph.h>
#include <dqm_display/core/TreeNode.h>

#include <QAbstractItemModel>
#include <QDebug>
#include <QAbstractProxyModel>

class QTreeView;

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    
    class ParameterFilterModel : public QAbstractProxyModel
    {
	Q_OBJECT
        
    public:
	ParameterFilterModel(QObject * parent, QAbstractItemModel * source);
        
	void setVisibleItem(const QModelIndex & index);

	QModelIndex mapFromSource(const QModelIndex & source_index) const override {
            QModelIndex i = index(source_index.row(), source_index.column());
	    return i;
	}

	QModelIndex mapToSource(const QModelIndex & index) const override {
	    QModelIndex i = sourceModel()->index(m_row, index.column(), m_parent);
            return i;
	}

	QModelIndex index(int row, int column,
	        const QModelIndex& parent = QModelIndex()) const override {
            QModelIndex i = createIndex(column, row);
            return i;
	}

        int rowCount(const QModelIndex &) const override {
            return sourceModel()->columnCount();
        }

        int columnCount(const QModelIndex &) const override {
            return 1;
        }

        QModelIndex parent(const QModelIndex &) const override {
            return QModelIndex();
        }

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override {
            QVariant v;
            if (role == Qt::DisplayRole) {
                v = sourceModel()->data(mapToSource(index), role);
            }
            return v;
        }

        QVariant headerData(int section, Qt::Orientation orientation, int role) const override {
            if (orientation == Qt::Vertical) {
                return sourceModel()->headerData(section, Qt::Horizontal, role);
            }
            return QVariant();
        }

    private:
        QModelIndex m_parent;
	int m_row;
    };    
  }
}

#endif

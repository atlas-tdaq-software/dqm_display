#ifndef RESULT_HISTORY_GRAPH_H
#define RESULT_HISTORY_GRAPH_H

/*! \file ResultHistoryGraph Declares the dqm_display::gui::ResultHistoryGraph class.

 * \author Yurij Ilchenko, Kevin Slagle, Serguei Kolos
 */
#include <vector>

#include <dqmf/is/Result.h>
#include <dqm_display/core/Result.h>

#include <TCanvas.h>
#include <TGraph.h>
#include <TImage.h>
#include <TPolyMarker.h>

#include <QObject>
#include <QSize>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {

        /*! \class dqm_display::gui::ResultHistoryGraph
         *  Class stands to plot DQParameter history
         */
        // ======================================================================

        class ResultHistoryGraph : public QObject {
	    Q_OBJECT
            
        public:
            ResultHistoryGraph(	const std::string & name,
                                const std::vector<dqmf::is::Result> & results,
                                const std::string & tag_name = std::string());

            ~ResultHistoryGraph() {
                delete m_image;
            }

            QImage getImage(const QSize & size = QSize(256, 256));
            
            const char * getTitle() const { return m_title.c_str(); }

        private:
            typedef std::vector<TPolyMarker> Markers;

            const bool  m_main;
            QSize       m_size;
            TGraph	m_graph;
            Markers	m_markers;
            std::string	m_title;
            TCanvas*    m_canvas;
            TImage*     m_image;
        };
    }
}

Q_DECLARE_METATYPE(dqm_display::gui::ResultHistoryGraph*)

#endif // RESULT_HISTORY_GRAPH_H

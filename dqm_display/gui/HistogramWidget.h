#ifndef _DQM_DISPLAY_GUI_HISTOGRAM_WIDGET_H_
#define _DQM_DISPLAY_GUI_HISTOGRAM_WIDGET_H_

/* \file Histogram Declares the dqm_display::gui::Histogram class.
 * \author Yurij Ilchenko, Kevin Slagle. Serguei Kolos
 */
#include <QWidget>

#include <dqm_display/gui/CanvasWidget.h>
#include <dqm_display/gui/Histogram.h>

namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
        
        /*! \class dqm_display::gui::HistogramWidget
         *  Class stands to output histogram on widget
         */
        class HistogramWidget : public CanvasWidget {
            Q_OBJECT

            enum HistoType { TH1_TYPE, TH2_TYPE, TH3_TYPE, TGraph_TYPE, TGraph2D_TYPE, OTHER_TYPE };

        public:
            HistogramWidget(QWidget* parent = 0);
            
            void clear();

            void setNode(const core::TreeNode * node);
            
            bool isNull() const { return m_histogram.isNull(); }

            void paintCanvas() override {
                m_histogram.paint();
            }

            Histogram & histogram() {
                return m_histogram;
            }

            QSize sizeHint() const override {
                return QSize(500, 400);
            }

        private:
            Histogram m_histogram;
        };
}}

#endif

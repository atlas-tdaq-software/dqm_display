#ifndef _DQMI_OVERLAY_WIDGET_H
#define _DQMI_OVERLAY_WIDGET_H

/*! \file OverlayWidget Declares the dqm_display::gui::OverlayWidget class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <QWidget>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui {

    class Display;

/*! \class dqm_display::gui::Display
 *  OverlayWidget class for the DQM Display
 */
class OverlayWidget : public QWidget
{
public:
    OverlayWidget(QWidget * parent);

    ~OverlayWidget();

protected:
    //! Catches resize and child events from the parent widget
    bool eventFilter(QObject * obj, QEvent * ev);

    void paintEvent(QPaintEvent *);
};

}}

#endif

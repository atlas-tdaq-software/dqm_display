#ifndef _DQMI_GUI_FIND_H
#define _DQMI_GUI_FIND_H

/*! \file Find Declares the gui::Find class.
 * \author Anyes Taffard, Kevin Slagle, Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/TreeNode.h>
#include <dqm_display/gui/DQDataModel.h>

#include <ui_FindWidget.h>

#include <QCloseEvent>
#include <QIcon>
#include <QRegExp>
#include <QSortFilterProxyModel>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    /*! \namespace gui
     *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
     */
    namespace gui {
        class FindResultsModel: public QAbstractItemModel {
        Q_OBJECT

        public:
            enum Role {
                SortRole = Qt::UserRole
            };

            FindResultsModel(const DQDataModel &model, QObject *parent);

            void reset(const QRegExp &name, const QRegExp &parentName, uint type, uint status);

        public:
            QVariant data(const QModelIndex &index, int role) const;

            Qt::ItemFlags flags(const QModelIndex &index) const;

            QVariant headerData(int section, Qt::Orientation orientation,
                    int role = Qt::DisplayRole) const;

            QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;

            QModelIndex parent(const QModelIndex &index) const;

            int rowCount(const QModelIndex &parent = QModelIndex()) const;

            int columnCount(const QModelIndex &parent = QModelIndex()) const;

            bool hasChildren(const QModelIndex &parent = QModelIndex()) const;

        public slots:
            void when_findFinished();

            void when_mainModelUpdated();

        signals:
            void advanced();

            void finished();

        private:
            void run();

            void watchProgress();

            void find(const core::TreeNode &parent);

        private:
            const DQDataModel &m_data_model;
            const QIcon m_region_icon;
            const QIcon m_parameter_icon;

            double m_percent;
            QRegExp m_name;
            QRegExp m_parent_name;
            uint m_type;
            uint m_status;
            uint m_counter;
            QVector<const core::TreeNode*> m_result;
        };

        /*! \class gui::Find
         *  This class implements a region finder window.
         */
        class Find: public QWidget, public Ui::FindWidget {
        Q_OBJECT

        public:
            Find(const DQDataModel &model, QWidget *parent = 0);

            void closeEvent(QCloseEvent *e) override {
                e->ignore();
                emit reject();
            }

        public slots:
            void on_pushButton_find_clicked();

            void on_treeView_find_doubleClicked(const QModelIndex &index);

            void findAdvanced();

            void findFinished();

            void reject() {
                hide();
            }

        signals:
            void nodeActivated(const core::TreeNode *node);

        private:
            FindResultsModel m_find_result_model;
            QSortFilterProxyModel m_sorting_model;
        };
    }
}

#endif

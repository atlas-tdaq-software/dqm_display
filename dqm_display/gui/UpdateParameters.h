/*
 * UpdateParameters.h
 *
 *  Created on: Dec 11, 2013
 *      Author: kolos
 */

#ifndef _DQM_DISPLAY_UPDATE_PARAMETERS_H_
#define _DQM_DISPLAY_UPDATE_PARAMETERS_H_

#include <dqm_display/core/Parameter.h>

#include <ui_ParametersDialog.h>
#include <QCloseEvent>
#include <QDialog>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui {

class UpdateParameters: public QDialog, public Ui::ParametersDialog
{
    Q_OBJECT

public:
    UpdateParameters();

    void setParameter(const core::Parameter * parameter);

    void closeEvent(QCloseEvent *e) override {
        e->ignore();
        emit reject();
    }

public slots:
    void accept();

    void reject() {
        hide();
    }

private:
    QString m_param_name;
    const core::Parameter * m_parameter;
};

}}

#endif


#ifndef _DQMI_GUI_ICONS_VIEW_DELEGATE_MODEL_H
#define _DQMI_GUI_ICONS_VIEW_DELEGATE_MODEL_H

/*! \file IconsViewDelegate.h Declares the gui::IconsViewDelegate class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ResultHistoryGraph.h>
#include <dqm_display/core/TreeNode.h>

#include <QColor>
#include <QSize>
#include <QStyledItemDelegate>
#include <QStyleOptionViewItem>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    class IconsViewDelegate : public QStyledItemDelegate
    {
	Q_OBJECT
	
      public:
	IconsViewDelegate(const QSize & icon_size, int padding, QObject *parent,
	        bool follow_height_change = true);
	
	void paint(QPainter * painter, const QStyleOptionViewItem & option,
	        const QModelIndex & index) const;
      
	QSize sizeHint(const QStyleOptionViewItem & option,
	        const QModelIndex & index) const;
      
	void repaintIcons();

      public slots:
	void when_viewHeightChanged(int height, const QModelIndex & index);

        void when_iconsRepaintRequired(const QModelIndex & index);

      protected:
	void doPaint(QPainter * painter, const QStyleOptionViewItem & option,
	        const QModelIndex & index, int titleHeight) const;

      protected:
        const int m_original_spacing;
        const int m_margin;
        const int m_radius;
        const int m_shadow;
        int m_spacing;
        QSize m_size_hint;
        QColor m_selection_color;
    };
  }
}

#endif

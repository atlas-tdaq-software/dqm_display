#ifndef _DQMI_GUI_GRAPHICS_GRID_LAYOUT_H
#define _DQMI_GUI_GRAPHICS_GRID_LAYOUT_H

/*! \file GraphicsGridLayout Declares the dqm_display::gui::GraphicsGridLayout class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_config/dal/DQLayout.h>

#include <QGraphicsGridLayout>
#include <QSize>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    class GraphicsItem;
    class GraphicsRegion;

    class GraphicsGridLayout : 	public QGraphicsGridLayout
    {        
      public:
        GraphicsGridLayout(GraphicsRegion * parent, const dqm_config::dal::DQLayout * layout, const QString & name);

        bool insertItem(GraphicsItem * item, int n);

        void insertItem(GraphicsItem * item);

        void setGeometry(const QRectF & rect);

        virtual void setAngle(double ) { ; }
              
	QSizeF sizeHint(Qt::SizeHint which, const QSizeF & constraint = QSizeF()) const;

	int rows() const { return m_rows; }

	int columns() const { return m_columns; }
        
        virtual void updateSize() const;

        QRectF boundingRect() const;

      protected:
        bool calculateItemPosition(uint n, int & row, int & column, int & row_span, int & column_span) const;

      protected:
	typedef std::string Mask;
	typedef std::vector<double> Spacing;
	typedef std::vector<std::pair<unsigned int, unsigned int>> Span;
	typedef std::map<std::pair<unsigned int, unsigned int>, std::string> Headers;

	GraphicsRegion * m_parent;
	const QString m_name;
	mutable QSizeF m_size;
	mutable QSizeF m_cell_size;
	mutable QRectF m_bounding_rect;
	uint m_rows;
	uint m_columns;
	Mask m_children_grid;
	qreal m_h_spacing;
	qreal m_v_spacing;
	qreal m_h_margins;
	qreal m_v_margins;
	Spacing m_row_spacing;
	Spacing m_column_spacing;
	Spacing m_row_shifts;
	Spacing m_column_shifts;
	Span m_children_span;
	Headers m_row_headers;
	Headers m_column_headers;
	bool m_layout_horizontal;
    };
  }
}

#endif

/*
 * About.h
 *
 *  Created on: Dec 11, 2013
 *      Author: kolos
 */

#ifndef _DQM_DISPLAY_ABOUT_H_
#define _DQM_DISPLAY_ABOUT_H_

#include <ui_AboutDialog.h>
#include <QApplication>
#include <QCloseEvent>
#include <QDebug>
#include <QDialog>
#include <QMovie>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui {

class About: public QDialog, public Ui::AboutDialog
{
    Q_OBJECT

public:
    About()
      : m_wait(true)
    {
	setupUi(this);
	m_movie = new QMovie(":/images/dqm-loader.gif");
	labelAnimation->setMovie(m_movie);
	connect(pushButtonClose, SIGNAL(clicked()), this, SLOT(hideDialog()));
    }

    ~About()
    {
	delete m_movie;
    }

    void show(bool show_close_button = true)
    {
	m_movie->start();
	QDialog::show();
	m_wait = true;

	if (show_close_button) {
	    pushButtonClose->show();
	} else {
	    pushButtonClose->hide();
	}
    }

    void closeEvent(QCloseEvent *e) override {
        e->ignore();
        emit reject();
    }

public slots:
    void hideDialog()
    {
	m_movie->stop();
	m_wait = false;
	QDialog::hide();
    }

    void reject() {
        hideDialog();
    }

private:
    QMovie * m_movie;
    bool m_wait;
};

}}

#endif


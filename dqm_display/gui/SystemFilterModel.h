#ifndef DQMI_GUI_SYSTEM_FILTER_MODEL_H
#define DQMI_GUI_SYSTEM_FILTER_MODEL_H

/*! \file SytemFilterModel.h Declares the gui::SytemFilterModel class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/gui/ResultHistoryGraph.h>
#include <dqm_display/core/TreeNode.h>

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  /*! \namespace gui
   *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
   */
  namespace gui
  {
    
    class SystemFilterModel : public QSortFilterProxyModel
    {
	Q_OBJECT
        
    public:
	SystemFilterModel(QObject * parent, QAbstractItemModel * source);
        
	bool filterAcceptsRow(int sourceRow, const QModelIndex & sourceParent) const;
    };    
  }
}

#endif

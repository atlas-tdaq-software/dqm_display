#ifndef _DQM_DISPLAY_GUI_EXTENDED_TREE_VIEW_H
#define _DQM_DISPLAY_GUI_EXTENDED_TREE_VIEW_H

/*! \file TreeView Declares the dqm_display::gui::TreeView class.
 * \author Anyes Taffard, Kevin Slagle
 * \version 1.0
 */

#include <dqm_display/gui/TreeView.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
/*! \namespace gui
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring Display GUI.
 */
namespace gui
{
    /*! \class dqm_display::gui::ExtendedTreeView
     *  The tree displayed in the main panel
     */
    class ExtendedTreeView: public TreeView
    {
	Q_OBJECT

    public:
	ExtendedTreeView(QWidget *parent = 0)
	    : TreeView(parent), m_send_signal(true) { ; }

	void setCurrentIndex(const QModelIndex & index)
	{
	    m_send_signal = false;
	    QTreeView::setCurrentIndex(m_proxy_model->mapFromSource(index));
	    m_send_signal = true;
	}

    protected:
	virtual void processClick(const QModelIndex & )
	{
	    ;
	}

	void currentChanged(const QModelIndex & current, const QModelIndex & previous)
	{
	    QTreeView::currentChanged(current, previous);
	    if (m_send_signal)
		emit nodeActivated(DQDataModel::treeNode(current));
	}

	bool m_send_signal;
    };
}
}

#endif

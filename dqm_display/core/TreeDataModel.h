#ifndef _DQMI_CORE_TREE_DATA_MODEL_H_
#define _DQMI_CORE_TREE_DATA_MODEL_H_

/*! \file TreeDataModel.h Declares the TreeDataModel class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <string>

#include <functional>

#include <config/Configuration.h>

#include <dqm_display/core/Input.h>
#include <dqm_display/core/Region.h>
#include <dqm_display/core/ResultListener.h>

#include <dqm_config/AlgorithmConfig.h>
#include <dqm_config/AlgorithmConfigFactory.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{    
    namespace core
    {
        /*! \brief This class implements the in-memory representation of the DQM configuration.
            \ingroup public
         */
        class TreeDataModel
        {
	    static bool defaultSubsystemSelector( const dqm_config::dal::DQRegion & ) { return true; }
	    static void defaultReloadCallback( ) { return ; }
            
          public:
	    typedef std::function<bool ( const dqm_config::dal::DQRegion & region )>	SubsystemSelector;
	    typedef std::function<void ( void )>					ReloadCallback;
	    
            /*! Constructor creates the DQ tree.
                \param partition defines target partition
                \param connect_string defines the database to take the DQ configuration
             */
            TreeDataModel (	const std::string & partition,
                                const std::string & connect_string,
                                ResultListener & listener,
                                ReloadCallback reload_callback = &TreeDataModel::defaultReloadCallback,
				SubsystemSelector subsystemSelector = &TreeDataModel::defaultSubsystemSelector );

            virtual ~TreeDataModel();

            const std::string & getPartitionName() const { return partition_name_; }
            
            Configuration & config() { return configuration_; }
                        
            Input & input() { return input_; }
            
            Region * getRootRegion() {
                return &root_region_;
            }

            std::shared_ptr<dqm_core::AlgorithmConfig>
            createAlgorithmConfig(const dqm_config::dal::DQParameter & config)
	    {
        	return algo_factory_.createAlgorithmConfig(config);
	    }

            QString getReferenceNameForParameter(const QString & name) {
        	return QString::fromStdString(
        	    algo_factory_.getReferenceManager().getReferenceName(name.toStdString(), true));
            }

            ResultListener & resultListener() { return result_listener_; }

            void runStarted() {
                algo_factory_.updateRunParameters();
            }

          private:
            static void configurationCallback( const std::vector<ConfigurationChange *> & , void * );

          protected:
            std::string				partition_name_;
            dqm_config::AlgorithmConfigFactory	algo_factory_;
            Input				input_;
            Configuration			configuration_;
            Configuration::CallbackId 		subscription_;
            std::function<void (void)>		db_reloaded_callback_;
            ResultListener &			result_listener_;
            Region                              root_region_;
        };
    }
}

#endif

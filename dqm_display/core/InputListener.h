#ifndef _DQM_DISPLAY_CORE_INPUT_LISTENER_H_
#define _DQM_DISPLAY_CORE_INPUT_LISTENER_H_

#include <memory>

/*! \file OutputListener.h Declares the dqm_display::core::InputListener class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  namespace core
  {
    class Result;
    
    /*! \brief This class defines abstract interface for the notification of availability
                of the Data Quality assessment result. User has to declare a
                class which implements this interface and register instances of such class
                with the dqm_display::core::Output class via one of the 
                dqm_display::core::Output::addListener functions.
        \ingroup public
        \sa     dqm_display::core::Input
     */
    struct InputListener
    {
        /*! Virtual destructor is defined to make sure that dustructor of the sub-class will be
                called whenever the delete operator is applied to the
                dqm_display::core::OutputListener pointer.
         */
        virtual ~InputListener () { ; }
        
        /*! This function is called whenever an object for which this listener has been
                registered appears in the corresponding dqm_display::core::Output stream.
            \param result DQ assessment result (called function must delete)
         */
        virtual void handleResult(std::shared_ptr<Result> & result) = 0;

        virtual void removeListener(InputListener * ) { ; }
    };
  }
}

#endif

#ifndef _DQMI_CORE_RESULT_LISTENER_H_
#define _DQMI_CORE_RESULT_LISTENER_H_

/*! \file ResultListener.h Declares the dqm_display::core::ResultListener class.
 * \author Serguei Kolos
 * \version 1.0
 */

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
    namespace core
    {
	class TreeNode;

	/*! \brief This class defines abstract interface for the notification of availability
		    of the Data Quality assessment result. User has to declare a
		    class which implements this interface and register instances of such class
		    with the dqm_display::core::Output class via one of the dqm_display::core::Output::addListener functions.
	    \ingroup public
	 */
	struct ResultListener
	{
	    /*! Virtual destructor is defined to make sure that dustructor of the sub-class will be
		    called whenever the delete operator is applied to the dqm_display::core::ResultListener pointer.
	     */
	    virtual ~ResultListener () { ; }
	    
	    /*! This function is called whenever an object for which this listener has been
		    registered appears in the corresponding dqm_display::core::Output stream.
	     */
	    virtual void nodeStatusUpdated( const TreeNode & node, bool status_changed, bool status_degraded ) = 0;
	};
    }
}

#endif

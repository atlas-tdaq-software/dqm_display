#ifndef _DQM_CORE_PARAMETER_H_
#define _DQM_CORE_PARAMETER_H_

/*! \file Parameter.h Declares the core::Parameter class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */
#include <memory>

#include <dqm_core/AlgorithmConfig.h>

#include <dqm_config/dal/DQParameter.h>

#include <dqm_display/core/TreeNode.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {
    namespace gui {
        class Histogram;
    }
    namespace core {
        /*!
         * This class is used to represent DQ parameters in the tree.
         */
        class Parameter: public TreeNode {
        public:
            /// only to be called by Region
            Parameter(Region &parent, const dqm_config::dal::DQParameter &config);

            ~Parameter();

            int getPosition() const;

            bool histogramExists() const;

            gui::Histogram& getHistogram() const;

            const dqm_config::dal::DQParameter& getConfig() const {
                return m_config;
            }

            const std::vector<std::string>& getInputDataSource() const;

            const std::shared_ptr<const dqm_core::AlgorithmConfig>& getAlgorithmConfig() const {
                return m_algorithm_config;
            }

            void changeState(bool enabled, const QString& = QString()) const override;

            const TreeNode* getChild(int) const override {
                return 0;
            }

            uint childCount() const override {
                return 0;
            }

            uint totalChildCount() const override {
                return 0;
            }

            bool isLeaf() const override {
                return true;
            }

            bool isRoot() const override {
                return false;
            }

            QString getReferenceName() const override;

            QImage getImage(const QSize &size) const override;

            QString makeConfigText() const override;

            QVector<QPair<QString, QString>> getAllAttributes() const override;

        protected:
            QVector<QPair<QString, QString>> readConfig() const;

            const dqm_config::dal::DQParameter &m_config;
            std::shared_ptr<const dqm_core::AlgorithmConfig> m_algorithm_config;
            mutable gui::Histogram *m_histogram;
        };
    }
}

#endif

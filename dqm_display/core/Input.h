#ifndef _DQM_DISPLAY_CORE_INPUT_H_
#define _DQM_DISPLAY_CORE_INPUT_H_

/*! \file Output.h Declares the dqm_display::core::Input class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */

#include <unordered_map>

#include <mutex>

#include <is/inforeceiver.h>
#include <is/infodictionary.h>

#include <dqm_display/core/InputListener.h>

#include <QHash>
#include <QString>
#include <QVector>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
  namespace core
  {    
    /*! \brief This class provides a simple implementation of the abstract output 
    		interface for publishing information
                which is produced by the Data Quality assessment algorithms.
        \ingroup public
        \sa dqm_display::core::Output
     */
    class Input : private ISInfoReceiver,
                   private ISInfoDictionary
    {
      public:
        Input( const IPCPartition & partition, const std::string & is_server_name );
        
        ~Input();

        void start();
        
        void stop();
        
        /*! This function is called to register a listener object with a particular 
        	object in this output stream. The OutputListener::handleResult function 
                will be called by this Output stream whenever a dqm_core::Result object 
                for the dqm_core::Parameter with the given name becomes available in 
                this stream.
            \param name parameters' name
            \param listener listener associated with the given parameter name
         */
        void addListener( const QString & name, InputListener * listener );
        
        void removeListener( const QString & name, InputListener * listener );

        const IPCPartition& partition() const
        { return ISInfoReceiver::partition(); }
        
      private:
        void callback( ISCallbackInfo * info );
        
      private:
        typedef QHash<QString, QVector<InputListener*>> Listeners;
        
        const std::string	m_server_name;
	std::mutex		m_mutex;
        Listeners		m_listeners;
    };
  }
}

#endif

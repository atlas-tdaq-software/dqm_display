#ifndef _DQMI_CORE_REGION_H_
#define _DQMI_CORE_REGION_H_

/*! \file Region.h Declares the Region class.
 * \author Serguei Kolos, Kevin Slagle
 * \version 1.0
 */

#include <QVector>

#include <dqm_config/dal/DQRegion.h>

#include <dqm_display/core/TreeNode.h>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
    namespace core
    {
        class Parameter;
        class TreeDataModel;

	class Region : public TreeNode
	{
            friend class TreeDataModel;

	  public:
	    virtual ~Region();

	    typedef std::vector<Parameter*>	ParametersList;
            typedef std::vector<Region*>	RegionsList;

	    const dqm_config::dal::DQLayout * getLayout( ) const;

	    const QString & getAgentName() const;

	    int getPos(const Region * region) const;

            int getPos(const Parameter * parameter) const;

            int getPosition() const {
	      return parent_.getPos(this);
	    }

            QString getStatusHtml() const;

	    bool isLeaf( ) const { return false; }

	    bool isRoot() const;

	    void changeState(bool enabled, const QString& name) const;

	    const RegionsList & getSubRegions( ) const { return regions_; }

	    const ParametersList & getParameters( ) const { return parameters_; }

	    const TreeNode * getChild( int index ) const;

            const Region * getRegion( int index ) const
            { return regions_[index]; }

            const Parameter * getParameter( int index ) const
            { return parameters_[index]; }

            QString getReferenceName() const { return ""; }

            QImage getImage(const QSize & size) const;

            QString makeConfigText() const;

	    uint childCount() const
            { return ( regions_.size() + parameters_.size() ); }

            uint regionCount() const
            { return regions_.size(); }

            uint parameterCount() const
            { return parameters_.size(); }

            uint totalChildCount() const;

	    const TreeNode* findChild(const QString &name) const;

	    const Region* findRegion(const QString &name) const;

	    const Parameter* findParameter(const QString &name) const;

            void findAllChildren(const QString &name, QVector<TreeNode*> & children) const;

            void findAllAgents(const QString & name, QVector<QString> & agents) const;

            TreeDataModel & model() const { return model_; }

	    Region * addRegion(const dqm_config::dal::DQRegion & config);

	    Parameter * addParameter(const dqm_config::dal::DQParameter & config);

	  protected:
            Region(TreeDataModel & model, Region & parent, const dqm_config::dal::DQRegion & config);
	    Region(TreeDataModel & model);

	    void countRegionStatus(std::map<core::Result::Status, int> & totals) const;

	  protected:
            TreeDataModel & model_;
	    const dqm_config::dal::DQRegion* config_;
	    RegionsList regions_;
	    ParametersList parameters_;
            const QString agent_name_;
	};
    }
}

#endif

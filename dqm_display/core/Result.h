#ifndef _DQMD_CORE_RESULT_H_
#define _DQMD_CORE_RESULT_H_

/*! \file Result.h Declares the Result class.
 * \author Serguei Kolos
 * \version 1.0
 */
#include <dqmf/is/Result.h>
#include <is/callbackinfo.h>

#include <ctime>

#include <QString>

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display
{
    namespace core
    {
	/// DQ result, which is received by core::output and accessed through TreeNode
	class Result : protected dqmf::is::Result
	{
	  public:
            enum struct Status {
            	DISABLED	= dqmf::is::Result::Disabled,  // = -1
		UNDEFINED	= dqmf::is::Result::Undefined, // =  0
		ERROR		= dqmf::is::Result::Red,       // =  1
		WARNING		= dqmf::is::Result::Yellow,    // =  2
		GOOD		= dqmf::is::Result::Green,     // =  3
		LOW_STAT	= dqmf::is::Result::Green + 1, // =  4
            };
            
	  public:
            Result() : m_time(0) { ; }

            Result(const ISCallbackInfo & info);

	    Status status() const {
		return (dqmf::is::Result::status == dqmf::is::Result::Undefined && s_run_started_at < time_t()
			? dqm_display::core::Result::Status::LOW_STAT
			: dqm_display::core::Result::Status(dqmf::is::Result::status));
	    }
	    
	    const std::vector<dqmf::is::Tag> & tags() const { return dqmf::is::Result::tags; }
	    
	    std::time_t time_t() const { return m_time; }
	    
	    QString timeString() const;

	    bool isValid() const { return m_time; }
            
            static void runStartedAt(std::time_t sor) { s_run_started_at = sor; }

	  private:
	    static std::time_t s_run_started_at;

	  private:
	    std::time_t m_time;
	};
    }
}

#endif

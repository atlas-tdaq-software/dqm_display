#ifndef _DQMI_CORE_TREE_NODE_H_
#define _DQMI_CORE_TREE_NODE_H_

/*! \file TreeNode.h Declares the TreeNode class.
 * \author Serguei Kolos
 * \version 1.0
 */

#include <dqm_display/core/InputListener.h>
#include <dqm_display/core/Result.h>

#include <boost/thread/mutex.hpp>

#include <memory>

#include <QPair>
#include <QPixmap>
#include <QSize>
#include <QString>
#include <QVector>

namespace dqm_config {
    namespace dal { class DQNode; class DQParameter; class DQRegion; class DQShape; }
    class AlgorithmConfig;
}

/*! \namespace dqm_display
 *  This is a wrapping namespace for all public classes of the Data Quality Monitoring framework.
 */
namespace dqm_display {    
    namespace core {
        class Region;
        
        /*! \brief This class implements the TreeNode interface for the in-memory DQM data.
        \ingroup public
         */
        class TreeNode : public InputListener
        {
          public:
            virtual ~TreeNode();

	    const QString & getName() const { return name_; }

	    const QString & getLabel() const { return label_; }
            
            const QString & getPath() const { return path_; }

            const QString & getDescription() const { return description_; }

            const QString & getTroubleshooting() const { return troubleshooting_; }

            QString getFullName() const { return (path_ + "/" + name_); }
	    
            const Region * getParent() const { return &parent_; }
            
            std::shared_ptr<Result> getResult() const {
		boost::mutex::scoped_lock lock( mutex_ );
                return result_;
	    }
            
            Result::Status getStatus() const {
		boost::mutex::scoped_lock lock( mutex_ );
                return result_->status();
	    }

            const dqm_config::dal::DQShape * getShape() const { return shape_; }

            virtual QString makeResultText() const;

            virtual QString getStatusHtml() const;

            virtual QString getToolTip() const;

            virtual QVector<QPair<QString, QString>> getAllAttributes() const {
                return QVector<QPair<QString, QString>>();
            }

            virtual const QString & getAgentName() const;

            virtual QString makeConfigText() const = 0;
            
            virtual QImage getImage(const QSize & size) const = 0;

            virtual bool isLeaf( ) const = 0;
            
            virtual bool isRoot() const = 0;

            virtual void changeState(bool enabled, const QString & name = QString()) const = 0;
            	    
            virtual const TreeNode * getChild( int index ) const = 0;
            
            virtual uint childCount() const = 0;

            virtual uint totalChildCount() const = 0;
                        
            virtual int getPosition() const = 0;

            virtual QString getReferenceName() const = 0;
                                                
          protected:
            TreeNode(Region & parent, const dqm_config::dal::DQNode & config);

            TreeNode();

            QString resultToHtml(bool addImage) const;

            void handleResult(std::shared_ptr<Result> & result) override;
                        
          protected:
            const Region &	parent_;
            QString		name_;
            QString		label_;
            const QString	path_;
            const QString	description_;
            const QString	troubleshooting_;
            const dqm_config::dal::DQShape * shape_;
            
            mutable QString	m_config_text;
            mutable boost::mutex mutex_; // mutex_ is used to guard the result_
            mutable std::shared_ptr<Result> result_;
        };
    }
}

#endif
